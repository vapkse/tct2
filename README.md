## Development server

Run `npm i` to install dependencies

Run `npm start` for a dev server. Navigate to `http://localhost:888/`. The app will automatically reload if you change any of the source files.

