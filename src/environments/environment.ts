// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBggcUAEi4PDsGAFfDW850sXi1IT8ciWAE',
        authDomain: 'nodejs-tct.firebaseapp.com',
        // eslint-disable-next-line @typescript-eslint/naming-convention
        databaseURL: 'https://nodejs-tct.firebaseio.com',
        projectId: 'nodejs-tct',
        storageBucket: 'nodejs-tct.appspot.com',
        messagingSenderId: '766706097107',
        appId: 'nodejs-tct'
    }
};
