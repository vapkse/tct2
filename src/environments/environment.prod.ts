export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyBggcUAEi4PDsGAFfDW850sXi1IT8ciWAE',
        authDomain: 'nodejs-tct.firebaseapp.com',
        // eslint-disable-next-line @typescript-eslint/naming-convention
        databaseURL: 'https://nodejs-tct.firebaseio.com',
        projectId: 'nodejs-tct',
        storageBucket: 'nodejs-tct.appspot.com',
        messagingSenderId: '766706097107',
        appId: 'nodejs-tct'
    }
};
