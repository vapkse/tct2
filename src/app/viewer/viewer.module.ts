import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule, Routes } from '@angular/router';

import { MonacoEditorModule } from '../common/monaco-editor/index';
import { MainToolbarModule } from '../components/main-toolbar/main-toolbar.module';
import { UsageViewerModule } from '../components/usage-viewer/usage-viewer.module';
import { LeaveEditionGuardService } from '../services/leave-edition-guard.service';
import { TranslationModule } from '../translation/translation.module';
import { UnitModule } from './unit/unit.module';
import { ViewerComponent } from './viewer.component';

const routes: Routes = [
    { path: ':id', component: ViewerComponent },
    { path: ':id/:userId', component: ViewerComponent },
    { path: ':id/:userId/:view', component: ViewerComponent, canDeactivate: [LeaveEditionGuardService] }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MainToolbarModule,
        MatButtonModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatIconModule,
        MonacoEditorModule,
        TranslationModule,
        UnitModule,
        UsageViewerModule
    ],
    exports: [
        ViewerComponent
    ],
    declarations: [
        ViewerComponent
    ]
})
export class ViewerModule {
}
