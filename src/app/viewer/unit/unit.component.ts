import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { TubeUnit } from '../../models/tube-unit.model';

@Component({
    selector: 'app-unit',
    templateUrl: './unit.component.html',
    styleUrls: ['./unit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnitComponent {
    @Input()
    public unit: TubeUnit;

    @Input()
    public unitIndex: number;
}
