import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslationModule } from '../../translation/translation.module';
import { UnitComponent } from './unit.component';

@NgModule({
    imports: [
        CommonModule,
        TranslationModule
    ],
    exports: [
        UnitComponent
    ],
    declarations: [
        UnitComponent
    ]
})
export class UnitModule {
}
