import { ChangeDetectionStrategy, Component, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { combineLatestWith, debounceTime, distinctUntilChanged, filter, map, mergeWith, Observable, of, skip, startWith, Subject, switchMap, take, tap, throttleTime, throwError, withLatestFrom } from 'rxjs';

import { shareReplayLast, subscribeWith } from '../common/custom-operators';
import { MessageBoxDialogService } from '../common/message-box-dialog/message-box-dialog.service';
import { StatusData } from '../common/status/status.model';
import { StatusService } from '../common/status/status.service';
import { Document } from '../models/document.model';
import { Tube } from '../models/tube.model';
import { TubeBase } from '../models/tube-base.model';
import { TubePinout } from '../models/tube-pinout.model';
import { TubeType } from '../models/tube-type.model';
import { AuthService } from '../services/auth/auth.service';
import { CompareService, CompareView } from '../services/compare.service';
import { FileService } from '../services/file.service';
import { CanLeaveEdition } from '../services/leave-edition-guard.service';
import { DownloadDocumentData, EditTubeData, OpenService, UsageData } from '../services/open.service';
import { SaveService } from '../services/save.service';
import { ThemeService } from '../services/theme.service';
import { TubesService } from '../services/tubes.service';
import { UnitService } from '../services/unit.service';
import { TranslationService } from '../translation/translation.service';

interface RouteParams extends Params {
    id?: string;
    userId?: string;
    view?: CompareView;
}

interface QueryParams extends Params {
    usage: number;
}

@Component({
    selector: 'app-viewer',
    templateUrl: './viewer.component.html',
    styleUrls: ['./viewer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        CompareService,
        SaveService,
        UnitService
    ]
})
export class ViewerComponent implements CanLeaveEdition {
    @ViewChildren('expansion', { read: ElementRef })
    public set expansionPanels(value: QueryList<ElementRef<HTMLElement>>) {
        if (value?.length) {
            this.expansionPanels$.next(value);
        }
    }

    public modifiedGlobalTubeText$ = new Subject<string>();

    public tube$: Observable<Tube>;
    public type$: Observable<TubeType>;

    public userTubeText$: Observable<string>;
    public globalTubeText$: Observable<string>;

    public base$: Observable<TubeBase>;
    public pinout$: Observable<TubePinout>;

    public baseImageFileName$: Observable<string>;
    public pinoutImageFileName$: Observable<string>;

    public hasBaseImageError = false;
    public hasPinoutImageError = false;

    public enableComparator$: Observable<boolean>;
    public comparatorTheme$: Observable<string>;

    public downloadDocument$ = new Subject<DownloadDocumentData>();
    public markAsSeen$ = new Subject<boolean>();
    public markAsMerged$ = new Subject<void>();

    public editTube$ = new Subject<EditTubeData>();
    public editUsage$ = new Subject<UsageData>();

    public usageOpened$ = new Subject<number>();
    public usageClosed$ = new Subject<number>();
    public openedUsageIndex$: Observable<number>;

    private expansionPanels$ = new Subject<QueryList<ElementRef<HTMLElement>>>();

    public constructor(
        public authService: AuthService,
        public fileService: FileService,
        public compareService: CompareService,
        public unitService: UnitService,
        private saveService: SaveService,
        private messageBoxDialogService: MessageBoxDialogService,
        private translationService: TranslationService,
        statusService: StatusService,
        tubeService: TubesService,
        themeService: ThemeService,
        activatedRoute: ActivatedRoute,
        router: Router,
        openService: OpenService
    ) {
        const routeParams$ = activatedRoute.params.pipe(
            map(params => params as RouteParams),
            shareReplayLast()
        );

        const queryParams$ = activatedRoute.queryParams.pipe(
            map(params => params as QueryParams),
            shareReplayLast()
        );

        this.openedUsageIndex$ = queryParams$.pipe(
            map(queryParams => (queryParams.usage && !isNaN(+queryParams.usage) && +queryParams.usage) || 0),
            take(1),
            shareReplayLast()
        );

        const openedUsage$ = this.usageOpened$.pipe(
            debounceTime(10),
            mergeWith(this.usageClosed$),
            debounceTime(100),
            skip(1),
            tap(usage => {
                const queryParams = { usage } as QueryParams;
                void router.navigate([], { relativeTo: activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
            }),
            shareReplayLast()
        );

        const queryParamsUsage$ = queryParams$.pipe(
            map(queryParams => queryParams.usage),
            take(1),
            filter(Boolean),
            map(usage => !isNaN(+usage) ? +usage : undefined),
            shareReplayLast()
        );

        const ensureOpenedUsageVisible$ = openedUsage$.pipe(
            mergeWith(queryParamsUsage$),
            filter(usage => usage !== undefined),
            combineLatestWith(this.expansionPanels$),
            map(([usage, expansionPanels]) => expansionPanels && Array.from(expansionPanels)[usage]?.nativeElement),
            filter(Boolean),
            tap(expansionPanel => expansionPanel.scrollIntoView())
        );

        const ensureUserTubesCollection$ = authService.isAdmin$.pipe(
            combineLatestWith(routeParams$),
            map(([isAdmin, routeParams]) => {
                if (isAdmin && routeParams.userId) {
                    tubeService.showUserTubes$.next(true);
                }
                return routeParams;
            }),
            shareReplayLast()
        );

        let userTube$ = undefined as Observable<Tube>;
        const markedAsMerged$ = this.markAsMerged$.pipe(
            throttleTime(100),
            switchMap(() => userTube$.pipe(
                take(1),
                switchMap(tube => saveService.saveTube$(tube, true).pipe(
                    take(1)
                ))
            ))
        );

        userTube$ = ensureUserTubesCollection$.pipe(
            distinctUntilChanged((r1, r2) => r1.id === r2.id && r1.userId === r2.userId),
            switchMap(routeParams => {
                const t$ = routeParams.id ? tubeService.getTube$(+routeParams.id, routeParams.userId) : of({} as Tube);
                return t$.pipe(
                    switchMap(tube => tube ? of(tube) : throwError(() => new Error('tube not found.'))),
                    tap(tube => {
                        console.log('tube', tube);
                    })
                );
            }),
            mergeWith(markedAsMerged$),
            shareReplayLast()
        );

        const jsonStringify = (tube: Tube, space: number): string => {
            const allKeys = new Array<string>();
            const seen = {} as Record<string, unknown>;
            JSON.stringify(tube, (key: string, value: unknown) => {
                if (!(key in seen)) {
                    allKeys.push(key);
                    seen[key] = null;
                }
                return value;
            });
            allKeys.sort();
            return JSON.stringify(tube, allKeys, space);
        };

        this.userTubeText$ = userTube$.pipe(
            map(tube => jsonStringify(tube, 4)),
            shareReplayLast()
        );

        const globalTube$ = routeParams$.pipe(
            map(routeParams => +routeParams.id),
            distinctUntilChanged(),
            switchMap(tubeId => tubeService.tubeReferentialMap$.pipe(
                map(tubeReferentialMap => tubeReferentialMap.get(tubeId) || { id: tubeId } as Tube)
            )),
            shareReplayLast()
        );

        this.globalTubeText$ = globalTube$.pipe(
            map(tube => jsonStringify(tube, 4)),
            shareReplayLast()
        );

        const isModified$ = this.modifiedGlobalTubeText$.pipe(
            debounceTime(1000),
            map(() => {
                saveService.canSave$.next(true);
                return true;
            }),
            shareReplayLast()
        );

        const downloadDatabase$ = saveService.save$.pipe(
            withLatestFrom(this.modifiedGlobalTubeText$, globalTube$),
            switchMap(([_, modifiedGlobalTubeText, globalTube]) => {
                const modifiedGlobalTube = JSON.parse(modifiedGlobalTubeText) as Tube;
                console.log('Update database with modifiedGlobalTubeText and save into file');
                console.log(modifiedGlobalTube, globalTube);

                // Update global database
                return tubeService.tubeReferential$.pipe(
                    take(1),
                    map(tubeReferential => {
                        const getNewId = (): number => tubeReferential.reduce((id, t) => {
                            if (id <= t.id) {
                                id = t.id + 1;
                            }
                            return id;
                        }, 0);

                        // Check id, and create a new one in case of
                        if (!modifiedGlobalTube.id) {
                            modifiedGlobalTube.id = getNewId();
                            tubeReferential.push(modifiedGlobalTube);
                        } else {
                            const index = tubeReferential.findIndex(t => t.id === modifiedGlobalTube.id);
                            if (index >= 0 && tubeReferential[index].label === modifiedGlobalTube.label) {
                                tubeReferential.splice(index, 1, modifiedGlobalTube);
                            } else {
                                if (index >= 0) {
                                    modifiedGlobalTube.id = getNewId();
                                }
                                tubeReferential.push(modifiedGlobalTube);
                            }
                        }

                        fileService.downloadDatabaseFile(tubeReferential, 'tubes.json');
                        tubeService.updateReferential$.next(tubeReferential);
                        this.markAsMerged$.next();
                        saveService.canSave$.next(false);
                    })
                );
            })
        );

        this.enableComparator$ = routeParams$.pipe(
            combineLatestWith(authService.isAdmin$),
            take(1),
            map(([routeParams, isAdmin]) => routeParams.userId && isAdmin),
            filter(Boolean),
            subscribeWith(downloadDatabase$, isModified$),
            startWith(false),
            shareReplayLast()
        );

        const compareViewChanged$ = this.enableComparator$.pipe(
            filter(Boolean),
            switchMap(() => compareService.compareView$),
            debounceTime(100),
            distinctUntilChanged(),
            withLatestFrom(routeParams$),
            map(([view, routeParams]) => {
                const currentView = routeParams.view || 'user';
                if (view !== currentView) {
                    console.log('compareViewChanged', view);
                    const routeSegments = ['view', `${routeParams.id}`, `${routeParams.userId}`, `${view}`];
                    void router.navigate(routeSegments, { queryParamsHandling: 'preserve' });
                }
            })
        );

        const view$ = routeParams$.pipe(
            map(routeParams => routeParams.view || 'user'),
            distinctUntilChanged(),
            tap(view => {
                compareService.compareView$.next(view);
                // Ensure save button is visible in comparator view
                saveService.canSave$.next(view === 'comparator' ? false : undefined);
            }),
            subscribeWith(compareViewChanged$),
            shareReplayLast()
        );

        const comparisonWarning$ = view$.pipe(
            combineLatestWith(userTube$, globalTube$),
            debounceTime(100),
            filter(([view, userTube, globalTube]) => view === 'comparator' && globalTube.label && (globalTube.label !== userTube.label)),
            take(1),
            switchMap(() => {
                const statusData = {
                    message: translationService.translate('Tube label is different from origin, comparison may be wrong.'),
                    type: 'warning',
                    duration: 10000
                } as StatusData;
                return statusService.showStatus$(statusData);
            }),
            shareReplayLast()
        );

        const downloadDocument$ = this.downloadDocument$.pipe(
            throttleTime(1000),
            switchMap(data => openService.downloadDocument$(data))
        );

        const markedAsSeen$ = this.markAsSeen$.pipe(
            throttleTime(100),
            withLatestFrom(userTube$),
            switchMap(([value, tube]) => {
                tube.seen = value;
                return saveService.saveTube$(tube, tube.merged).pipe(
                    take(1)
                );
            })
        );

        const tube$ = view$.pipe(
            distinctUntilChanged(),
            switchMap(view => view !== 'global' ? userTube$ : globalTube$),
            tap(tube => unitService.tube$.next(tube)),
            shareReplayLast()
        );

        const editTube$ = this.editTube$.pipe(
            throttleTime(1000),
            withLatestFrom(tube$),
            switchMap(([data, tube]) => openService.editTube$(tube, data))
        );

        const editUsage$ = this.editUsage$.pipe(
            throttleTime(1000),
            withLatestFrom(tube$),
            switchMap(([data, tube]) => openService.openUsage$(tube, data))
        );

        this.tube$ = tube$.pipe(
            subscribeWith(comparisonWarning$, downloadDocument$, markedAsSeen$, editTube$, editUsage$, ensureOpenedUsageVisible$),
            shareReplayLast()
        );

        this.type$ = this.tube$.pipe(
            combineLatestWith(tubeService.tubeTypeMap$),
            map(([tube, tubeTypeMap]) => tube.typeId && tubeTypeMap.get(tube.typeId)),
            shareReplayLast()
        );

        this.base$ = this.tube$.pipe(
            combineLatestWith(tubeService.tubeBaseMap$),
            map(([tube, tubeBaseMap]) => tube.baseId && tubeBaseMap.get(tube.baseId)),
            shareReplayLast()
        );

        this.baseImageFileName$ = this.base$.pipe(
            combineLatestWith(themeService.isDark$),
            map(([base, isDark]) => {
                const basePath = isDark ? 'bases-dark' : 'bases-light';
                this.hasBaseImageError = false;
                return base ? `assets/images/${basePath}/${base.label}.svg` : undefined;
            }),
            shareReplayLast()
        );

        this.pinout$ = this.tube$.pipe(
            combineLatestWith(tubeService.tubePinoutMap$),
            map(([tube, tubePinoutMap]) => tube.pinoutId && tubePinoutMap.get(tube.pinoutId)),
            shareReplayLast()
        );

        this.pinoutImageFileName$ = this.pinout$.pipe(
            combineLatestWith(themeService.isDark$),
            map(([pinout, isDark]) => {
                const pinoutPath = isDark ? 'pinouts-dark' : 'pinouts-light';
                this.hasPinoutImageError = false;
                return pinout ? `assets/images/${pinoutPath}/${pinout.label}.svg` : undefined;
            }),
            shareReplayLast()
        );

        this.comparatorTheme$ = themeService.isDark$.pipe(
            map(isDark => isDark ? 'vs-dark' : 'default'),
            shareReplayLast()
        );
    }

    public getDocument(tube: Tube, id: string): Document {
        return tube.documents.find(d => d.id === id);
    }

    public canLeave$(): Observable<boolean> {
        return this.saveService.canSave$.pipe(
            take(1),
            switchMap(canSave => {
                if (!canSave) {
                    return of(true);
                }

                const msg = this.translationService.translate('Are you sure you want to leave without saving your changes?<br/>If you click ok, all your changes will be lost.');
                return this.messageBoxDialogService.openConfirmation$(msg).pipe(
                    map(response => response === 'ok')
                );
            })
        );
    }

    public getDisplayDate(timeStamp: number): string {
        return (new Date(timeStamp)).toLocaleDateString();
    }
}
