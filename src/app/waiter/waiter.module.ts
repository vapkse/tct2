import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { WaiterComponent } from './waiter.component';


@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        WaiterComponent
    ],
    declarations: [
        WaiterComponent
    ]
})
export class WaiterModule {
}
