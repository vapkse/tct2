import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
    selector: 'app-waiter',
    template: '<div></div>',
    styleUrls: ['./waiter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WaiterComponent {
}
