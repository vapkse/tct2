import { NgModule } from '@angular/core';

import { ResizeListenerDirective } from './resize-listener.directive';

@NgModule({
    declarations: [
        ResizeListenerDirective
    ],
    exports: [
        ResizeListenerDirective
    ]
})
export class ResizeListenerModule { }

export * from './resize-listener.directive';
