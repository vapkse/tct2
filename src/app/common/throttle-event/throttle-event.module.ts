import { NgModule } from '@angular/core';

import { ThrottleEventDirective } from './throttle-event-directive';

@NgModule({
    declarations: [
        ThrottleEventDirective
    ],
    exports: [
        ThrottleEventDirective
    ]
})
export class ThrottleEventModule { }
