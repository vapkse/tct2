import { ChangeDetectorRef, Directive, ElementRef, Input, OnInit } from '@angular/core';
import { catchError, finalize, fromEvent, Observable, of, switchMap, take, takeUntil, throttleTime, timeout } from 'rxjs';

import { DestroyDirective } from '../destroy/destroy.directive';
import { StatusService } from '../status/status.service';

export interface ThrottleEventParams {
    action$: Observable<unknown>;
    name?: string;
    time?: number;
    waiter?: boolean;
    timeout?: number;
}

@Directive({
    selector: '[throttle-event]'
})
export class ThrottleEventDirective extends DestroyDirective implements OnInit {

    @Input('throttle-event')
    public action$: ThrottleEventParams | Observable<unknown>;

    public constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private elementRef: ElementRef<HTMLButtonElement>,
        private statusService: StatusService
    ) {
        super();
    }

    public ngOnInit(): void {
        const element = this.elementRef.nativeElement;
        const params = !(this.action$ instanceof Observable) && this.action$;
        const eventName = params?.name || 'click';
        const time = params?.time || 250;

        fromEvent(element, eventName).pipe(
            throttleTime(time),
            switchMap(event => {
                event.stopPropagation();
                event.preventDefault();

                const action$ = params?.action$ || this.action$ as Observable<unknown>;
                if (!action$) {
                    return of(undefined);
                }

                if (params?.waiter) {
                    element.disabled = true;
                    element.classList.add('mat-disabled');
                    element.setAttribute('waiter', 'true');
                }

                const timeoutDelay = params?.timeout || (params?.waiter ? 30000 : 86400000);

                return action$.pipe(
                    take(1),
                    catchError(error => this.statusService.showStatus$(error as Error)),
                    timeout(timeoutDelay),
                    finalize(() => {
                        if (params?.waiter) {
                            element.disabled = false;
                            element.classList.remove('mat-disabled');
                            element.removeAttribute('waiter');
                        }
                    })
                );
            }),
            takeUntil(this.destroyed$)
        ).subscribe(() => {
            this.changeDetectorRef.markForCheck();
        });
    }
}
