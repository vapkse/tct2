import { isEqual } from 'lodash-es';
import { distinctUntilChanged, map, mergeWith, MonoTypeOperatorFunction, Observable, of, pairwise, startWith } from 'rxjs';

import { catchErrorLog } from '../custom-operators';

export class LoadingContext<T> {
    public constructor(public loading: boolean, public data?: T, public error?: Error) { }

    public static fromLoadingContext<R>(loadingContext: LoadingContext<unknown>, data?: R): LoadingContext<R> {
        return new LoadingContext<R>(loadingContext.loading, data, loadingContext.error);
    }
}

export const loading$ = <T>(obs$: Observable<T>): Observable<LoadingContext<T>> => {
    const obsLoader$ = obs$.pipe(
        map(data => new LoadingContext<T>(false, data)),
        catchErrorLog(err => of(new LoadingContext<T>(false, undefined, err as Error)))
    );

    return of(new LoadingContext<T>(true)).pipe(
        mergeWith(obsLoader$)
    );
};

// Operator to be used in loading observables but outside loading$ call
export const keepPreviousDataWhileLoading = <T>(): MonoTypeOperatorFunction<LoadingContext<T>> => (source$: Observable<LoadingContext<T>>): Observable<LoadingContext<T>> => source$.pipe(
    startWith(undefined),
    distinctUntilChanged((a, b) => isEqual(a, b) && a.loading),
    pairwise(),
    map(([previousLoader, loader]) => {
        if (loader.loading) {
            return new LoadingContext<T>(true, previousLoader?.data);
        }
        return loader;
    })
);
