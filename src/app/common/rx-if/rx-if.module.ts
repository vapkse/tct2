import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RxIfDirective } from './rx-if.directive';

@NgModule({
    declarations: [
        RxIfDirective
    ],
    exports: [
        RxIfDirective
    ],
    imports: [
        CommonModule
    ]
})
export class RxIfModule { }
