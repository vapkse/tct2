import { Directive, EmbeddedViewRef, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { NextObserver, Observable, of, Subscription, tap } from 'rxjs';

import { catchErrorLog } from '../custom-operators';
import { LoadingContext } from './loading-context';


export type IfDirectiveStatus = 'else' | 'loading' | 'error' | 'complete' | 'empty';

type IfDirectiveTemplate = IfDirectiveStatus | 'content';

@Directive({
    selector: '[rxIf]'
})
export class RxIfDirective<T = unknown> implements OnDestroy {
    /** @internal */
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public static rxIfUseIfTypeGuard: void;

    /**
     * Assert the correct type of the expression bound to the `rxIf` input within the template.
     *
     * The presence of this static field is a signal to the Ivy template type check compiler that
     * when the `rxIf` structural directive renders its template, the type of the expression bound
     * to `rxIf` should be narrowed in some way. For `rxIf`, the binding expression itself is used to
     * narrow its type, which allows the strictNullChecks feature of TypeScript to work with `rxIf`.
     */
    // eslint-disable-next-line @typescript-eslint/naming-convention, camelcase
    public static ngTemplateGuard_rxIf: 'binding';

    private context: IfContext<T> = new IfContext<T>();
    private viewRef = new Map<IfDirectiveTemplate, EmbeddedViewRef<IfContext<T>>>();
    private templateRef = new Map<IfDirectiveTemplate, TemplateRef<IfContext<T>>>();
    private subscription = new Subscription();

    public constructor(
        private viewContainer: ViewContainerRef,
        templateRef: TemplateRef<IfContext<T>>
    ) {
        this.templateRef.set('content', templateRef);
        this.context.$status = 'else';
    }

    /**
     * The Boolean expression to evaluate as the condition for showing a template.
     */
    @Input()
    // eslint-disable-next-line rxjs/finnish
    public set rxIf(sourceObs$: Observable<T | LoadingContext<T>>) {
        this.subscription.unsubscribe();
        this.context.$status = 'else';
        this.context.$implicit = null;

        const obs$: Observable<T | LoadingContext<T>> = sourceObs$.pipe(
            catchErrorLog(err => {
                this.context.$error = err as Error;
                this.context.$status = 'error';
                this.updateView();
                return of(undefined) as Observable<T | LoadingContext<T>>;
            }),
            tap({
                next: value => {
                    const isEmpty = (data: T): boolean => data === null || data === undefined || (data instanceof Array && data.length === 0);
                    if (value instanceof LoadingContext) {
                        const data = value.data;
                        this.context.$implicit = data;

                        if (value.error) {
                            this.context.$error = value.error;
                            this.context.$status = 'error';
                        } else if (value.loading) {
                            this.context.$status = 'loading';
                        } else {
                            this.context.$status = isEmpty(data) ? 'empty' : null;
                        }
                    } else {
                        this.context.$implicit = value;
                        this.context.$status = isEmpty(value) ? 'empty' : null;
                    }
                    this.updateView();
                },
                complete: () => {
                    if (this.context.$status !== 'error' && !(this.context.$status === 'empty' && this.viewRef.get('empty'))) {
                        this.context.$status = 'complete';
                        this.updateView();
                    }
                }
            } as NextObserver<T | LoadingContext<T>>)
        );
        this.subscription = new Subscription();
        this.subscription.add(obs$.subscribe());
        this.updateView();
    }

    /**
     * A template to show if the condition expression evaluates without errors.
     */
    @Input()
    public set rxIfComplete(templateRef: TemplateRef<IfContext<T>> | null) {
        RxIfDirective.assertTemplate('rxIfComplete', templateRef);
        this.templateRef.set('complete', templateRef);
        if (templateRef) {
            this.removeViewRef('complete');
            this.updateView();
        }
    }

    /**
     * A template to show if the condition expression not yet evaluate.
     */
    @Input()
    public set rxIfLoading(templateRef: TemplateRef<IfContext<T>> | null) {
        RxIfDirective.assertTemplate('rxIfLoading', templateRef);
        this.templateRef.set('loading', templateRef);
        if (templateRef) {
            this.removeViewRef('loading');
            this.updateView();
        }
    }

    /**
     * A template to show if the condition expression evaluates with error.
     */
    @Input()
    public set rxIfError(templateRef: TemplateRef<IfContext<T>> | null) {
        RxIfDirective.assertTemplate('rxIfError', templateRef);
        this.templateRef.set('error', templateRef);
        if (templateRef) {
            this.removeViewRef('error');
            this.updateView();
        }

    }

    /**
     * A template to show if the condition expression evaluates is empty.
     */
    @Input()
    public set rxIfEmpty(templateRef: TemplateRef<IfContext<T>> | null) {
        RxIfDirective.assertTemplate('rxIfEmpty', templateRef);
        this.templateRef.set('empty', templateRef);
        if (templateRef) {
            this.removeViewRef('empty');
            this.updateView();
        }

    }

    /**
     * A template to show if the condition expression evaluates is false.
     */
    @Input()
    public set rxIfElse(templateRef: TemplateRef<IfContext<T>> | null) {
        RxIfDirective.assertTemplate('rxIfElse', templateRef);
        this.templateRef.set('else', templateRef);
        if (templateRef) {
            this.removeViewRef('else');
            this.updateView();
        }

    }

    /**
     * Asserts the correct type of the context for the template that `rxIf` will render.
     *
     * The presence of this method is a signal to the Ivy template type-check compiler that the
     * `rxIf` structural directive renders its template with a specific context type.
     */
    public static ngTemplateContextGuard<U>(_dir: RxIfDirective<U>, _ctx: unknown):
        _ctx is IfContext<Exclude<U, false | 0 | '' | null | undefined>> {
        return true;
    }

    private static assertTemplate(property: string, templateRef: TemplateRef<unknown> | null): void {
        const isTemplateRefOrNull = !!(!templateRef || templateRef.createEmbeddedView);
        if (!isTemplateRefOrNull) {
            throw new Error(`${property} must be a TemplateRef, but received '${JSON.stringify(templateRef)}'.`);
        }
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    private removeViewRef(...names: ReadonlyArray<IfDirectiveTemplate>): void {
        names.forEach(name => {
            const viewRef = this.viewRef.get(name);
            if (viewRef) {
                viewRef.detach();
                viewRef.destroy();
                this.viewRef.delete(name);
            }
        });
    }

    private createView(name: IfDirectiveTemplate): EmbeddedViewRef<IfContext<T>> {
        let viewRef = this.viewRef.get(name);
        const templateRef = this.templateRef.get(name);
        if (!viewRef && templateRef) {
            this.viewRef.set(name, viewRef = this.viewContainer.createEmbeddedView(templateRef, this.context));
        }
        return viewRef;
    }

    private updateView(): void {
        if (this.context.$status === 'else') {
            this.removeViewRef('content', 'complete', 'loading', 'empty', 'error');
            this.createView('else');
        } else {
            this.removeViewRef('else');

            const viewRef = this.createView('content');
            viewRef.markForCheck();

            if (this.context.$status === 'error') {
                if (!this.viewRef.get('error')) {
                    this.removeViewRef('complete', 'loading', 'empty');
                    const errorViewRef = this.createView('error');
                    if (!errorViewRef) {
                        throw this.context.$error;
                    }
                }
            } else if (this.context.$status === 'loading') {
                if (!this.viewRef.get('loading')) {
                    this.removeViewRef('complete', 'error', 'empty');
                    this.createView('loading');
                }
            } else if (this.context.$status === 'empty') {
                if (!this.viewRef.get('empty')) {
                    this.removeViewRef('complete', 'error', 'loading');
                    this.createView('empty');
                }
            } else if (!this.viewRef.get('complete')) {
                this.removeViewRef('error', 'loading', 'empty');
                this.createView('complete');
            }
        }
    }
}

/**
 * @publicApi
 */
export class IfContext<T = unknown | ReadonlyArray<unknown>> {
    public $implicit: T = null;
    public $error: Error = null;
    public $status: IfDirectiveStatus = null;
}
