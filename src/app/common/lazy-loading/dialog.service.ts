import { Type } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { merge } from 'lodash-es';
import { map, Observable, Subject, switchMap, tap, withLatestFrom } from 'rxjs';

import { shareReplayLast, subscribeWith } from '../custom-operators';
import { AbstractLazyModule, LazyLoaderService } from './lazy-loader.service';

export class DialogConfig<T> extends MatDialogConfig<T> {
    public constructor(width?: string, height?: string) {
        super();

        this.width = width;
        this.height = height;
        this.maxWidth = '95vw';
        this.maxHeight = '95vh';
        this.minWidth = '400px';
        this.minHeight = '180px';
        this.panelClass ||= 'no-padding-dialog';
    }
}

export abstract class DialogService<R, D, C = unknown> {
    protected close$ = new Subject<R>();

    public constructor(
        private lazyLoaderService: LazyLoaderService,
        private dialog: MatDialog,
        private dialogConfig?: DialogConfig<D>
    ) {
        if (!this.dialogConfig) {
            this.dialogConfig = new DialogConfig();
        }
    }

    public open$(dialogData: D, dialogConfig?: Partial<DialogConfig<D>>): Observable<R> {
        const dialogRef$ = this.openDialogRef$(dialogData, dialogConfig).pipe(
            shareReplayLast()
        );

        const externalClose$ = this.close$.pipe(
            withLatestFrom(dialogRef$),
            tap(([response, dialogRef]) => dialogRef.close(response))
        );

        return dialogRef$.pipe(
            switchMap(dialogRef => dialogRef.afterClosed()),
            subscribeWith(externalClose$),
            shareReplayLast()
        );
    }

    public closeDialog(response?: R): void {
        this.close$.next(response);
    }

    protected openDialogRef$(dialogData: D, dialogConfig?: Partial<DialogConfig<D>>): Observable<MatDialogRef<C, R>> {
        return this.lazyLoaderService.loadModule$(this.getModule()).pipe(
            switchMap(moduleInfos => {
                const config = merge({}, this.dialogConfig, dialogConfig || {} as Partial<DialogConfig<D>>);
                config.data = dialogData || {} as D;
                config.minWidth = config.minWidth || '400px';

                // injector is private in MatDialog
                // eslint-disable-next-line @typescript-eslint/dot-notation
                this.dialog['_injector'] = moduleInfos.injector;
                const dialogRef = this.dialog.open<C, D, R>(moduleInfos.module.componentType, config);
                return dialogRef.afterOpened().pipe(
                    map(() => dialogRef)
                );
            })
        );
    }

    protected abstract getModule(): Promise<Type<AbstractLazyModule<C>>>;
}
