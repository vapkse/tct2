import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { map, Observable, take } from 'rxjs';

import { LazyLoaderService } from '../lazy-loading/lazy-loader.service';
import { StatusData } from './status.model';

@Injectable({
    providedIn: 'root'
})
export class StatusService {
    public constructor(
        private snackBar: MatSnackBar,
        private lazyLoaderService: LazyLoaderService
    ) { }

    public showStatus$(message: string | Error | StatusData | HttpErrorResponse): Observable<void> {

        return this.lazyLoaderService.loadModule$(
            import('./status.module').then(m => m.StatusModule)
        ).pipe(
            take(1),
            map(moduleInfos => {
                let data = {} as StatusData;
                let duration: number;
                if (message instanceof HttpErrorResponse) {
                    data.error = {
                        name: 'HttpErrorResponse',
                        message: message.message
                    };
                    duration = 20000;
                } else if (message instanceof Error) {
                    data.error = message;
                    duration = 20000;
                } else if (typeof message === 'string') {
                    data.message = message;
                    duration = 5000;
                } else {
                    data = message;
                    duration = message.duration || 5000;
                }

                this.snackBar.openFromComponent(moduleInfos.module.componentType, {
                    horizontalPosition: 'end',
                    verticalPosition: 'bottom',
                    duration,
                    data,
                    panelClass: 'status-container'
                } as MatSnackBarConfig<StatusData>);
            })
        );
    }
}
