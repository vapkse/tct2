
export type StatusType = 'info' | 'warning' | 'error' | 'accent';

export interface StatusData {
    error?: Error;
    message?: string;
    infos?: string;
    title?: string;
    icon?: string;
    type?: StatusType;
    duration?: number;
}

