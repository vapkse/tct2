import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { TranslationModule } from '../../translation/translation.module';
import { AbstractLazyModule } from '../lazy-loading/lazy-loader.service';
import { ThrottleEventModule } from '../throttle-event/throttle-event.module';
import { StatusComponent } from './status.component';

@NgModule({
    declarations: [StatusComponent],
    exports: [StatusComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        ThrottleEventModule,
        TranslationModule
    ]
})
export class StatusModule extends AbstractLazyModule<StatusComponent> {
    public constructor() {
        super(StatusComponent);
    }
}
