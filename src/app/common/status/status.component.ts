import { ChangeDetectionStrategy, Component, HostBinding, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

import { asyncSwitchMap } from '../custom-operators';
import { MessageBoxDialogService } from '../message-box-dialog/message-box-dialog.service';
import { StatusData, StatusType } from './status.model';


@Component({
    selector: 'app-status',
    templateUrl: './status.component.html',
    styleUrls: ['./status.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class StatusComponent {
    @HostBinding('attr.type')
    public type: StatusType;

    public icon: string;
    public title: string;
    public message: string;
    public info: string;

    public constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data: StatusData,
        public snackBar: MatSnackBar,
        private messageBoxDialogService: MessageBoxDialogService
    ) {
        if (data.error) {
            this.message = data.error.message;
            this.title = 'Error';
            this.info = data.error.stack;
            this.type = 'error';
            this.icon = 'error_outline';
        } else {
            this.type = data.type || 'info';
            this.message = data.message;
            this.title = data.title;
            this.icon = data.icon;

            switch (this.type) {
                case 'error':
                    this.title ||= 'Error';
                    this.icon ||= 'error_outline';
                    break;
                case 'warning':
                    this.title ||= 'Warning';
                    this.icon ||= 'warning_amber';
                    break;
                case 'accent':
                    this.title ||= 'Great';
                    this.icon ||= 'thumb_up_off_alt';
                    break;
                default:
                    this.title ||= 'Information';
                    this.icon ||= 'info';
            }
        }
    }

    public showInfo$(): Observable<unknown> {
        return asyncSwitchMap(() => this.messageBoxDialogService.open$(this.info));
    }
}
