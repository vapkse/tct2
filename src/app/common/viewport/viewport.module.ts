import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

import { ViewPortComponent } from './viewport.component';

@NgModule({
    declarations: [
        ViewPortComponent
    ],
    exports: [
        ViewPortComponent
    ],
    imports: [
        CommonModule,
        MatIconModule
    ]
})
export class ViewPortModule { }
