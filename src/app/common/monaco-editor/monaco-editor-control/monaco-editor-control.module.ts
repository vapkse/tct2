import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ResizeListenerModule } from '../../resize-listener';
import { MonacoEditorControlComponent } from './monaco-editor-control.component';

@NgModule({
    declarations: [MonacoEditorControlComponent],
    exports: [MonacoEditorControlComponent],
    imports: [
        CommonModule,
        FormsModule,
        ResizeListenerModule
    ]
})
export class MonacoEditorControlModule {}
