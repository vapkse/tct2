import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MonacoEditorComponent } from './monaco-editor.component';
import { MonacoEditorControlModule } from './monaco-editor-control/monaco-editor-control.module';

@NgModule({
    declarations: [MonacoEditorComponent],
    exports: [MonacoEditorComponent],
    imports: [
        CommonModule,
        FormsModule,
        MonacoEditorControlModule
    ]
})
export class MonacoEditorModule {}

export * from './options/editor-options.model';
export * from './options/editor-scrollbar-options.model';
export * from './monaco-editor.component';
