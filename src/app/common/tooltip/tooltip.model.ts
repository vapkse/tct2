import { DialogConfig } from '../lazy-loading/dialog.service';

export class TooltipConfig<D> extends DialogConfig<D> {
    public hideDelay: number;
}
