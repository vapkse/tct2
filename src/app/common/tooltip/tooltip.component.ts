import { Directive, ElementRef } from '@angular/core';


@Directive()
export class TooltipComponent {
    public constructor(
        public elementRef: ElementRef<HTMLElement>
    ) { }
}
