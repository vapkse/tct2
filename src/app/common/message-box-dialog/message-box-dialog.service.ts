import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, take } from 'rxjs';

import { TranslationService } from '../../translation/translation.service';
import { DialogConfig, DialogService } from '../lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from '../lazy-loading/lazy-loader.service';
import { MessageBoxDialogButtons, MessageBoxDialogData, MessageBoxDialogResponse } from './message-box-dialog.model';


@Injectable({
    providedIn: 'root'
})
export class MessageBoxDialogService extends DialogService<MessageBoxDialogResponse, MessageBoxDialogData> {
    public constructor(
        private translationService: TranslationService,
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        super(lazyLoaderService, dialog);
    }

    public open$(dialogData: string | MessageBoxDialogData, dialogConfig?: Partial<DialogConfig<MessageBoxDialogData>>): Observable<MessageBoxDialogResponse> {
        let messageBoxDialogData: MessageBoxDialogData;
        if (typeof dialogData === 'string') {
            messageBoxDialogData = {
                text: dialogData,
                buttons: MessageBoxDialogButtons.OK
            } as MessageBoxDialogData;
        } else {
            messageBoxDialogData = dialogData;
        }
        messageBoxDialogData.title = messageBoxDialogData.title || this.translationService.translate('Confirmation');

        return super.open$(messageBoxDialogData, dialogConfig);
    }

    public openConfirmation$(message: string): Observable<MessageBoxDialogResponse> {
        const dialogData = {
            text: message,
            buttons: MessageBoxDialogButtons.OK + MessageBoxDialogButtons.CANCEL
        } as MessageBoxDialogData;

        return this.open$(dialogData).pipe(
            take(1)
        );
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./message-box-dialog.module').then(m => m.MessageBoxDialogModule);
    }
}
