import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MessageBoxDialogButtons, MessageBoxDialogData } from './message-box-dialog.model';

@Component({
    selector: 'app-message-box-dialog',
    templateUrl: './message-box-dialog.component.html',
    styleUrls: ['./message-box-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageBoxDialogComponent {
    public constructor(
        @Inject(MAT_DIALOG_DATA) public dialogParams: MessageBoxDialogData
    ) {}

    public hasControl(key: 'OK' | 'CANCEL' | 'IGNORE' | 'RETRY'): boolean {
        // eslint-disable-next-line no-bitwise
        return (this.dialogParams.buttons & MessageBoxDialogButtons[key]) !== 0;
    }
}
