export type MessageBoxDialogResponse = 'ok' | 'cancel' | 'ignore' | 'retry';

export enum MessageBoxDialogButtons {
    OK = 0x1,
    CANCEL = 0x2,
    IGNORE = 0x4,
    RETRY = 0x8,
}

export interface MessageBoxDialogData {
    title: string;
    text: string;
    buttons?: MessageBoxDialogButtons;
}
