import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { TranslationModule } from '../../translation/translation.module';
import { AbstractLazyModule } from '../lazy-loading/lazy-loader.service';
import { MessageBoxDialogComponent } from './message-box-dialog.component';

@NgModule({
    declarations: [MessageBoxDialogComponent],
    exports: [MessageBoxDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        TranslationModule
    ]
})
export class MessageBoxDialogModule extends AbstractLazyModule<MessageBoxDialogComponent> {
    public constructor() {
        super(MessageBoxDialogComponent);
    }
}
