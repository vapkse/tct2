import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { NumericStepperComponent } from './numeric-stepper.component';

@NgModule({
    declarations: [NumericStepperComponent],
    exports: [NumericStepperComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule
    ]
})
export class NumericStepperModule { }

export * from './numeric-stepper.component';
