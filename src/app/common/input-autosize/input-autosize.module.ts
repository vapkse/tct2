import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { InputAutoSizeDirective, InputAutoSizeService } from './input-autosize.service';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [InputAutoSizeDirective],
    declarations: [InputAutoSizeDirective],
    providers: [InputAutoSizeService]
})
export class InputAutosizeModule { }
