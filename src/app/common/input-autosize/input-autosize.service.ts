import { Directive, ElementRef, Host, Injectable, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { AbstractControl, ControlContainer } from '@angular/forms';

type AutoSizeValidator = (control: AbstractControl) => null;

@Injectable()
export class InputAutoSizeService {
    private validators = new Map<string, AutoSizeValidator>();
    private nbCharMap = new Map<string, number>();
    private elementMap = new Map<string, HTMLInputElement>();

    public getValidator(name: string): AutoSizeValidator {
        if (!this.validators.has(name)) {
            this.validators.set(name, (control: AbstractControl): null => {
                const element = this.elementMap.get(name);
                const nbChar = String(control.value)?.length;
                this.nbCharMap.set(name, nbChar);
                this.setElementWidth(name, element);
                return null;
            });
        }

        return this.validators.get(name);
    }

    public addElement(name: string, element: HTMLInputElement): void {
        this.elementMap.set(name, element);
        element.style.maxWidth = '100%';
        this.setElementWidth(name, element);
    }

    private setElementWidth(name: string, element: HTMLInputElement): void {
        if (element && this.nbCharMap.has(name)) {
            const nbChar = this.nbCharMap.get(name);
            element.style.width = nbChar ? `${1 + nbChar}ch` : '2ch';
        }
    }
}

@Directive({
    selector: '[matInput][formControlName][type="number"]'
})
export class InputAutoSizeDirective implements OnInit {
    @Input()
    public formControlName: string;

    public constructor(
        private element: ElementRef<HTMLInputElement>,
        @Optional() @Host() @SkipSelf() private controlContainer: ControlContainer,
        private autoSizeService: InputAutoSizeService
    ) { }

    public ngOnInit(): void {
        if (this.controlContainer) {
            if (this.formControlName) {
                const formControl = this.controlContainer.control.get(this.formControlName);
                if (formControl) {
                    this.autoSizeService.addElement(this.formControlName, this.element.nativeElement);
                }
            } else {
                console.warn('Missing FormControlName directive from host element of the component');
            }
        } else {
            console.warn('Can\'t find parent FormGroup directive');
        }
    }
}
