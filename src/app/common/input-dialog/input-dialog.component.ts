import { ChangeDetectionStrategy, Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { debounceTime, distinctUntilChanged, filter, fromEvent, Subject, switchMap, takeUntil, tap } from 'rxjs';

import { shareReplayLast } from 'src/app/common/custom-operators';

import { filterMap, subscribeWith } from '../custom-operators';
import { DestroyDirective } from '../destroy/destroy.directive';
import { InputDialogData } from './input-dialog.model';


@Component({
    selector: 'app-input-dialog',
    templateUrl: './input-dialog.component.html',
    styleUrls: ['./input-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputDialogComponent extends DestroyDirective {
    public value: unknown;

    private inputElement$ = new Subject<ElementRef<HTMLTextAreaElement | HTMLInputElement>>();

    @ViewChild('inputref') protected set dialogInputRef(element: ElementRef<HTMLTextAreaElement | HTMLInputElement>) {
        this.inputElement$.next(element);
    }

    public constructor(
        @Inject(MAT_DIALOG_DATA) public dialogParams: InputDialogData<unknown>,
        dialogRef: MatDialogRef<InputDialogComponent, unknown>
    ) {
        super();

        this.value = dialogParams.value;

        const inputElement$ = this.inputElement$.pipe(
            debounceTime(100),
            filterMap(element => element?.nativeElement || undefined),
            distinctUntilChanged(),
            shareReplayLast()
        );

        const enterPressed$ = inputElement$.pipe(
            filter(inputElement => inputElement.tagName === 'INPUT'),
            switchMap(inputElement => fromEvent<KeyboardEvent>(inputElement, 'keypress')),
            tap(keyEvent => {
                if (keyEvent.code === 'Enter') {
                    dialogRef.close(this.value);
                }
            })
        );

        inputElement$.pipe(
            subscribeWith(enterPressed$),
            takeUntil(this.destroyed$)
        ).subscribe(inputElement => {
            if (inputElement.type !== 'number') {
                inputElement.selectionStart = 0;
                inputElement.selectionEnd = 9999;
            }
            inputElement.focus();
            inputElement.select();
        });
    }
}
