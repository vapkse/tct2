import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';

import { TranslationModule } from '../../translation/translation.module';
import { AbstractLazyModule } from '../lazy-loading/lazy-loader.service';
import { NumericStepperModule } from '../numeric-stepper/numeric-stepper.module';
import { InputDialogComponent } from './input-dialog.component';

@NgModule({
    declarations: [InputDialogComponent],
    exports: [InputDialogComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatToolbarModule,
        NumericStepperModule,
        TranslationModule
    ]
})
export class InputDialogModule extends AbstractLazyModule<InputDialogComponent> {
    public constructor() {
        super(InputDialogComponent);
    }
}
