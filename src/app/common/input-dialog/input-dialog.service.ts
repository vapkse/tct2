import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from '../lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from '../lazy-loading/lazy-loader.service';
import { InputDialogData } from './input-dialog.model';

@Injectable({
    providedIn: 'root'
})
export class InputDialogService<T> extends DialogService<T, InputDialogData<T>> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        super(lazyLoaderService, dialog);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./input-dialog.module').then(m => m.InputDialogModule);
    }
}
