export enum InputDialogButtons {
    OK = 0x1,
    CANCEL = 0x2,
}

export type InputDialogType = 'input' | 'textarea' | 'number';

export interface InputDialogData<T> {
    title: string;
    dialogType: InputDialogType;
    text?: string;
    fieldLabel?: string;
    value?: T;
    className?: string;
    buttons?: InputDialogButtons;
}
