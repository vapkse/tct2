/* eslint-disable guard-for-in */
import { ChangeDetectorRef, Directive } from '@angular/core';
import { AbstractControl, ControlValueAccessor, ValidationErrors, Validator } from '@angular/forms';
import { IFormGroup } from '@rxweb/types';
import { map, Observable, ReplaySubject, switchMap, take, tap, withLatestFrom } from 'rxjs';

import { shareReplayLast, subscribeWith } from '../custom-operators';

@Directive()
export abstract class AbstractSubformDirective<T extends object> implements ControlValueAccessor, Validator {

    public form$: Observable<IFormGroup<T>>;

    protected value$ = new ReplaySubject<T>(1);
    protected disabled$ = new ReplaySubject<boolean>(1);

    private _value: T;

    public constructor(
        protected changeDetectorRef: ChangeDetectorRef
    ) {
        const form$ = this.value$.pipe(
            map(value => this.createFormGroup(value)),
            shareReplayLast()
        );

        const valueChanged$ = form$.pipe(
            switchMap(form => form.valueChanges.pipe(
                tap(() => {
                    this._value = form.getRawValue();
                    this.onChangeCallback(this._value);
                    this.onTouchedCallback();
                })
            ))
        );

        const disableForm$ = this.disabled$.pipe(
            withLatestFrom(form$),
            tap(([disable, form]) => {
                if (disable) {
                    form.disable();
                } else {
                    form.enable();
                }
            })
        );

        this.form$ = form$.pipe(
            subscribeWith(valueChanged$, disableForm$),
            shareReplayLast()
        );
    }

    // ************* ControlValueAccessor Implementation **************
    public set value(value: T) {
        if (value !== this.value) {
            this.writeValue(value);
            this.onChangeCallback(value);
        }
    }

    public get value(): T {
        return this._value;
    }

    public writeValue(value: T): void {
        if (value !== this.value) {
            this._value = value;
            this.value$.next(value);
        }

    }

    public registerOnChange(fn: (value: T) => void): void {
        this.onChangeCallback = fn;
    }

    public registerOnTouched(fn: () => void): void {
        this.onTouchedCallback = fn;
    }

    public setDisabledState?(isDisabled: boolean): void {
        this.disabled$.next(isDisabled);
    }
    // ************* End of ControlValueAccessor Implementation **************


    // ************* AsyncValidator Implementation **************
    public validate(_control: AbstractControl): Observable<ValidationErrors | null> {
        return this.form$.pipe(
            take(1),
            map(form => form.valid ? null : { error: 'error' })
        );
    }
    // ************* End of AsyncValidator Implementation **************

    /**
     * Act as a trigger function called by parent when markForCheck
     * FIXME: There should be some more efficient ways to do it.
     *        There should be a way to call the onTouchedCallback
     *        method somewhere in the lifecycle of the component.
     */
    public markAllAsTouched$(): Observable<void> {
        return this.form$.pipe(
            take(1),
            map(form => {
                form.markAllAsTouched();
                this.changeDetectorRef.markForCheck();
            })
        );
    }

    // ngModel
    public onTouchedCallback = (): void => undefined;
    public onChangeCallback = (_cycle: T): void => undefined;

    protected abstract createFormGroup(value: T): IFormGroup<T>;
}
