import { ChangeDetectionStrategy, Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IFormArray, IFormBuilder, IFormGroup } from '@rxweb/types';
import { cloneDeep, isEqual } from 'lodash-es';
import { catchError, combineLatestWith, debounceTime, distinctUntilChanged, filter, map, mergeWith, Observable, of, startWith, Subject, switchMap, take, takeUntil, tap, throttleTime, throwError, withLatestFrom } from 'rxjs';

import { rowAnimation } from '../common/animations';
import { asyncMap, shareReplayLast, subscribeWith } from '../common/custom-operators';
import { DestroyDirective } from '../common/destroy/destroy.directive';
import { IdService } from '../common/id.service';
import { InputAutoSizeService } from '../common/input-autosize/input-autosize.service';
import { MessageBoxDialogService } from '../common/message-box-dialog/message-box-dialog.service';
import { StatusService } from '../common/status/status.service';
import { ChartWizardDialogService } from '../components/chart-wizard-dialog/chart-wizard-dialog.service';
import { UploadDialogService } from '../components/upload-dialog/upload-dialog.service';
import { Document } from '../models/document.model';
import { Printable } from '../models/printable.model';
import { Tube } from '../models/tube.model';
import { TubeUnit } from '../models/tube-unit.model';
import { TubeUsage } from '../models/tube-usage.model';
import { AuthService } from '../services/auth/auth.service';
import { allAcceptedFileType, isUndefined } from '../services/constants';
import { ErrorService } from '../services/error.service';
import { FileService } from '../services/file.service';
import { CanLeaveEdition } from '../services/leave-edition-guard.service';
import { DownloadDocumentData, OpenService, UsageData } from '../services/open.service';
import { SaveService } from '../services/save.service';
import { ThemeService } from '../services/theme.service';
import { TubesService } from '../services/tubes.service';
import { UndoRedoService } from '../services/undo-redo.service';
import { UnitService } from '../services/unit.service';
import { TranslationService } from '../translation/translation.service';

interface DocumentForm {
    document: Document;
}

interface MainForm {
    id: number;
    name: string;
    typeId: number;
    doubleFilament: boolean;
    vh1: number;
    ih1: number;
    vh2: number;
    ih2: number;
    baseId: number;
    pinoutId: number;
    notes: string;
    documents: Array<DocumentForm>;
    userId: string;
    date: number;
}

interface RouteParams extends Params {
    id?: string;
    userId?: string;
    duplicate?: 'duplicate' | undefined;
}

interface QueryParams extends Params {
    unit?: string;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    animations: [rowAnimation],
    providers: [
        SaveService,
        UndoRedoService,
        UnitService
    ]
})
export class EditorComponent extends DestroyDirective implements CanLeaveEdition {
    @ViewChild('fileUpload')
    public fileUpload: ElementRef<HTMLInputElement>;

    public tube$: Observable<Tube>;

    public mainForm$: Observable<IFormGroup<MainForm>>;
    public documentFormArrayControls$: Observable<ReadonlyArray<IFormGroup<DocumentForm>>>;
    public documentMap$: Observable<Map<string, Document>>;

    public addGraph$ = new Subject<void>();
    public uploadDocument$ = new Subject<Event>();
    public deleteDocument$ = new Subject<{ index: number; document: Document }>();
    public downloadDocument$ = new Subject<DownloadDocumentData>();

    public usages$: Observable<ReadonlyArray<TubeUsage>>;
    public deleteUsage$ = new Subject<[Event, TubeUsage]>();
    public openUsage$ = new Subject<UsageData>();
    public createUsage$ = new Subject<MouseEvent>();

    public updateUnit$ = new Subject<TubeUnit>();

    public selectedUnitHasG1$: Observable<boolean>;
    public selectedUnitHasG2$: Observable<boolean>;
    public selectedUnitHasG3$: Observable<boolean>;
    public hasGraphAttachment$: Observable<boolean>;

    public baseImageFileName$: Observable<string>;
    public pinoutImageFileName$: Observable<string>;

    public hasBaseImageError = false;
    public hasPinoutImageError = false;

    public allAcceptedFileType = allAcceptedFileType;

    public constructor(
        public authService: AuthService,
        public errorService: ErrorService,
        public tubeService: TubesService,
        public fileService: FileService,
        public unitService: UnitService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private saveService: SaveService,
        private messageBoxDialogService: MessageBoxDialogService,
        private chartWizardDialogService: ChartWizardDialogService,
        private translationService: TranslationService,
        idService: IdService,
        formBuilder: FormBuilder,
        themeService: ThemeService,
        uploadDialogService: UploadDialogService,
        undoRedoService: UndoRedoService<Tube>,
        statusService: StatusService,
        openService: OpenService,
        inputAutoSizeService: InputAutoSizeService
    ) {

        super();

        const fb = formBuilder as IFormBuilder;
        let formTube$ = undefined as Observable<Tube>;
        let units$ = undefined as Observable<Array<TubeUnit>>;

        // Ensure save button is visible
        saveService.canSave$.next(false);

        const normalizeTube = (tube: Tube): Tube => {
            const deleteProperties = (object: Record<string, unknown>): void => {
                Object.keys(object).forEach(k => {
                    const key = k as keyof Tube;
                    if (isUndefined(object[key])) {
                        delete object[key];
                    } else if (object[key] instanceof Object) {
                        deleteProperties(object[key] as Record<string, unknown>);
                    }
                });
            };

            deleteProperties(tube as unknown as Record<string, unknown>);

            delete tube.text;

            tube.documents ||= [] as Document[];
            tube.units ||= [] as TubeUnit[];
            tube.usages ||= [] as TubeUsage[];

            return tube;
        };

        const routeParams$ = activatedRoute.params.pipe(
            map(params => params as RouteParams),
            shareReplayLast()
        );

        const queryParams$ = activatedRoute.queryParams.pipe(
            map(params => params as QueryParams),
            shareReplayLast()
        );

        const saveTube$ = this.saveService.save$.pipe(
            throttleTime(1000),
            switchMap(() => formTube$.pipe(
                take(1),
                switchMap(tube => {
                    const isNew = !tube.id;
                    return this.saveService.saveTube$(tube).pipe(
                        take(1),
                        catchError((err: unknown) => statusService.showStatus$(err).pipe(
                            map(() => undefined as Tube)
                        )),
                        filter(Boolean),
                        switchMap(tubeSaved => {
                            tubeService.updateTube$.next(tubeSaved);
                            if (isNew) {
                                const queryParams = { search: tube.label } as QueryParams;
                                const urlTree = router.createUrlTree([], { relativeTo: activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
                                void router.navigateByUrl(router.serializeUrl(urlTree));
                            }
                            return undoRedoService.replace$(tubeSaved);
                        })
                    );
                })
            )),
            shareReplayLast()
        );

        const ensureUserTubesCollection$ = authService.isAdmin$.pipe(
            combineLatestWith(routeParams$),
            map(([isAdmin, routeParams]) => {
                if (isAdmin && routeParams.userId) {
                    tubeService.showUserTubes$.next(true);
                }
                return routeParams;
            }),
            shareReplayLast()
        );

        const tube$ = ensureUserTubesCollection$.pipe(
            switchMap(routeParams => {
                const t$ = routeParams.id ? tubeService.getTube$(+routeParams.id, routeParams.userId) : of({} as Tube);
                return t$.pipe(
                    switchMap(tube => tube ? of(tube) : throwError(() => new Error('tube not found.'))),
                    map(tube => {
                        if (routeParams.duplicate) {
                            // Clone tube before modifications to avoid a modification of the original instance
                            tube = cloneDeep(tube);
                            delete tube.userId;
                        }
                        return tube;
                    })
                );
            }),
            mergeWith(saveTube$),
            map(tube => normalizeTube(tube)),
            shareReplayLast()
        );

        const downloadDocument$ = this.downloadDocument$.pipe(
            throttleTime(1000),
            withLatestFrom(tube$),
            switchMap(([data, tube]) => {
                data.userId = tube.userId;
                return openService.downloadDocument$(data);
            })
        );

        this.tube$ = tube$.pipe(
            mergeWith(undoRedoService.current$),
            shareReplayLast()
        );

        this.documentMap$ = this.tube$.pipe(
            map(tube => tube.documents),
            map(documents => documents.reduce((m, doc) => m.set(doc.id, doc), new Map<string, Document>()))
        );

        const createDocumentFormGroup = (document: Document): IFormGroup<DocumentForm> => fb.group<DocumentForm>({
            document
        });

        this.mainForm$ = this.tube$.pipe(
            combineLatestWith(tubeService.tubeMapByNameLowerCase$),
            map(([tube, tubeMapByName]) => {
                const documentFormArray = tube.documents.map(document => createDocumentFormGroup(document));

                const nameValidator = (control: AbstractControl): ValidationErrors => {
                    const name = (control.value as string)?.toLocaleLowerCase().trim();
                    if (!name) {
                        return { required: '' } as ValidationErrors;
                    }
                    const existingTube = tubeMapByName.get(name);
                    if (existingTube && existingTube.id !== tube.id) {
                        return { duplicate: '' } as ValidationErrors;
                    } else {
                        return null as ValidationErrors;
                    }
                };

                const documentsDuplicationValidator = (form: IFormGroup<MainForm>): ValidationErrors => {
                    const documents = form.value.documents.map(documentForm => documentForm.document);
                    const names = documents.reduce((m, document) => m.add(document.label.toLowerCase().trim()), new Set<string>());
                    if (names.size < documents.length) {
                        return { documentDuplicated: true } as ValidationErrors;
                    } else {
                        return null as ValidationErrors;
                    }
                };

                const mainForm = fb.group<MainForm>({
                    id: tube.id,
                    name: [tube.label, nameValidator],
                    typeId: [tube.typeId, Validators.required],
                    doubleFilament: tube.dh,
                    vh1: [tube.vh1, [Validators.min(0), inputAutoSizeService.getValidator('vh1')]],
                    ih1: [tube.ih1, [Validators.min(0), inputAutoSizeService.getValidator('ih1')]],
                    vh2: [tube.vh2, [Validators.min(0), inputAutoSizeService.getValidator('vh2')]],
                    ih2: [tube.ih2, [Validators.min(0), inputAutoSizeService.getValidator('ih2')]],
                    baseId: tube.baseId,
                    pinoutId: tube.pinoutId,
                    notes: tube.notes,
                    documents: fb.array<DocumentForm>(documentFormArray),
                    userId: tube.userId,
                    date: tube.date
                }, { validators: documentsDuplicationValidator });

                if (tube.id) {
                    mainForm.controls.name.disable({ emitEvent: false });
                } else {
                    mainForm.controls.name.enable({ emitEvent: false });
                }

                console.log('EditorComponent, main form created');
                return mainForm;
            }),
            shareReplayLast()
        );

        const documentFormArray$ = this.mainForm$.pipe(
            map(form => form.controls.documents as IFormArray<DocumentForm>),
            shareReplayLast()
        );

        const documentAdded$ = this.uploadDocument$.pipe(
            throttleTime(1000),
            switchMap(event => {
                const target = event.target as HTMLInputElement;
                const file = target.files[0];
                if (!allAcceptedFileType.includes(file.type)) {
                    return statusService.showStatus$(new Error('Invalid file type.')).pipe(
                        map(() => undefined as IFormArray<DocumentForm>),
                        filter(Boolean)
                    );
                }

                return uploadDialogService.open$({ filename: `docs/${file.name}`, data: file }).pipe(
                    map(resultFileName => {
                        this.fileUpload.nativeElement.value = '';

                        if (resultFileName) {
                            return {
                                id: idService.generate(),
                                filename: file.name,
                                label: file.name
                            } as Document;
                        }

                        return undefined as Document;
                    }),
                    filter(Boolean),
                    withLatestFrom(documentFormArray$),
                    map(([document, documentFormArray]) => {
                        documentFormArray.push(createDocumentFormGroup(document));
                        return documentFormArray;
                    })
                );
            }),
            shareReplayLast()
        );

        const graphAdded$ = this.addGraph$.pipe(
            throttleTime(1000),
            switchMap(() => formTube$.pipe(
                take(1),
                switchMap(tube => this.chartWizardDialogService.open$({ tube }).pipe(
                    filter(Boolean),
                    withLatestFrom(documentFormArray$),
                    switchMap(([tubeGraph, documentFormArray]) =>
                        // Save json into firebase as a document
                        this.saveService.saveChart$(tube.id, tubeGraph).pipe(
                            map(graphFile => {
                                const document = {
                                    id: idService.generate(),
                                    label: graphFile.tubeGraph.name,
                                    filename: graphFile.filename
                                } as Document;

                                const existingIndex = documentFormArray.value.findIndex(d => d.document.filename === document.filename);
                                if (existingIndex >= 0) {
                                    documentFormArray.controls[existingIndex].setValue({
                                        document
                                    }, { emitEvent: false });
                                } else {
                                    documentFormArray.push(createDocumentFormGroup(document));
                                }

                                return documentFormArray;
                            })
                        )
                    )
                ))
            )),
            shareReplayLast()
        );

        const documentDeleted$ = this.deleteDocument$.pipe(
            switchMap(event => {
                const msg = translationService.translateWithArgs('Are you sure you want to delete the document $1 ?', `<br/><br/><span color="accent">${event.document.label}</span>`);
                return messageBoxDialogService.openConfirmation$(msg).pipe(
                    filter(response => response === 'ok'),
                    withLatestFrom(documentFormArray$),
                    map(([_, documentFormArray]) => {
                        documentFormArray.removeAt(event.index);
                        return documentFormArray;
                    })
                );
            }),
            shareReplayLast()
        );

        // Just map to have the correct type in html
        this.documentFormArrayControls$ = documentFormArray$.pipe(
            mergeWith(documentDeleted$, documentAdded$, graphAdded$),
            map(formArray => formArray.controls as Array<IFormGroup<DocumentForm>>),
            shareReplayLast()
        );

        const usageDeleted$ = this.deleteUsage$.pipe(
            map(([event, usage]) => {
                event.preventDefault();
                event.stopImmediatePropagation();
                return usage;
            }),
            // translate
            switchMap(usage => {
                const msg = translationService.translateWithArgs('Are you sure you want to delete the usage example $1 ?', `<br/><br/><span color="accent">${usage.label}</span>`);
                return messageBoxDialogService.openConfirmation$(msg).pipe(
                    filter(response => response === 'ok'),
                    switchMap(() => this.usages$.pipe(
                        take(1),
                        map(usages => usages.filter(u => u.id !== usage.id))
                    ))
                );
            })
        );

        this.usages$ = this.tube$.pipe(
            map(tube => tube.usages),
            mergeWith(usageDeleted$),
            startWith(new Array<TubeUsage>()),
            shareReplayLast()
        );

        const selectUnitIndex$ = queryParams$.pipe(
            tap(queryParams => {
                const selectedIndex = queryParams.unit && !isNaN(+queryParams.unit) && +queryParams.unit || 0;
                unitService.selectUnitIndex$.next(selectedIndex);
            })
        );

        const updatedUnits$ = this.updateUnit$.pipe(
            filter(Boolean),
            withLatestFrom(this.unitService.selectedUnitIndex$),
            switchMap(([unit, selectedUnitIndex]) => units$.pipe(
                take(1),
                map(units => {
                    // eslint-disable-next-line no-loops/no-loops
                    while (selectedUnitIndex >= units.length) {
                        units.push({} as TubeUnit);
                    }

                    units[selectedUnitIndex] = unit;
                    return [...units];
                })
            )),
            shareReplayLast()
        );

        units$ = this.tube$.pipe(
            map(tube => [...tube.units]),
            mergeWith(updatedUnits$),
            shareReplayLast()
        );

        this.selectedUnitHasG1$ = unitService.selectedTubeConfig$.pipe(
            map(config => config?.pins?.includes('g1')),
            shareReplayLast()
        );

        this.selectedUnitHasG2$ = unitService.selectedTubeConfig$.pipe(
            map(config => config?.pins?.includes('g2')),
            shareReplayLast()
        );

        this.selectedUnitHasG3$ = unitService.selectedTubeConfig$.pipe(
            map(config => config?.pins?.includes('g3')),
            shareReplayLast()
        );

        // output documents
        const formDocuments$ = documentFormArray$.pipe(
            switchMap(documentFormArray => of(documentFormArray.value).pipe(
                mergeWith(documentFormArray.valueChanges.pipe(
                    map(value => {
                        console.log('document form value changed emit', value);
                        return documentFormArray.getRawValue();
                    })
                ))
            )),
            debounceTime(10),
            map(values => values.map(value => value.document)),
            shareReplayLast()
        );

        // output tube observable
        formTube$ = this.mainForm$.pipe(
            switchMap(mainForm => of(mainForm.getRawValue()).pipe(
                mergeWith(mainForm.valueChanges.pipe(
                    map(value => {
                        console.log('EditorComponent, main form value changed emit', value);
                        return mainForm.getRawValue();
                    })
                ))
            )),
            combineLatestWith(formDocuments$, this.usages$, units$),
            debounceTime(20),
            map(([values, documents, usages, units]) => {
                const formTube = {
                    id: values.id,
                    label: values.name,
                    baseId: values.baseId,
                    pinoutId: values.pinoutId,
                    typeId: values.typeId,
                    documents,
                    units: [...units],
                    usages,
                    notes: values.notes,
                    dh: values.doubleFilament,
                    vh1: values.vh1,
                    ih1: values.ih1,
                    vh2: values.vh2,
                    ih2: values.ih2,
                    userId: values.userId,
                    date: values.date
                } as Tube;

                const normalizedFormTube = normalizeTube(formTube);

                unitService.tube$.next(normalizedFormTube);

                return normalizedFormTube;
            }),
            distinctUntilChanged((t1, t2) => {
                const eq = isEqual(t1, t2);
                if (!eq) {
                    console.log('new tube calculated', t1, t2);
                }
                return eq;
            }),
            switchMap(formTube => undoRedoService.add$(formTube).pipe(
                take(1)
            )),
            shareReplayLast()
        );

        this.baseImageFileName$ = formTube$.pipe(
            map(tube => tube.baseId),
            combineLatestWith(tubeService.tubeBaseMap$, themeService.isDark$),
            map(([baseId, tubeBaseMap, isDark]) => {
                const basePath = isDark ? 'bases-dark' : 'bases-light';
                const base = baseId && tubeBaseMap.get(baseId);
                this.hasBaseImageError = false;
                return base ? `assets/images/${basePath}/${base.label}.svg` : undefined;
            }),
            shareReplayLast()
        );

        this.pinoutImageFileName$ = formTube$.pipe(
            map(tube => tube.pinoutId),
            combineLatestWith(tubeService.tubePinoutMap$, themeService.isDark$),
            map(([pinoutId, tubePinoutMap, isDark]) => {
                const pinoutPath = isDark ? 'pinouts-dark' : 'pinouts-light';
                const pinout = pinoutId && tubePinoutMap.get(pinoutId);
                this.hasPinoutImageError = false;
                return pinout ? `assets/images/${pinoutPath}/${pinout.label}.svg` : undefined;
            }),
            shareReplayLast()
        );

        this.hasGraphAttachment$ = formTube$.pipe(
            map(tube => tubeService.hasGraphAttachment(tube.documents))
        );

        // Is modified observable
        const isModified$ = tube$.pipe(
            switchMap(tube => formTube$.pipe(
                withLatestFrom(this.mainForm$),
                map(([formTube, mainForm]) => {
                    if (mainForm.invalid) {
                        saveService.canSave$.next(false);
                        console.log('tube invalid', tube, formTube);
                    } else {
                        const isModified = !isEqual(formTube, tube);
                        if (isModified) {
                            console.log('tube modified', tube, formTube);
                        }
                        saveService.canSave$.next(isModified);
                    }
                })
            )),
            shareReplayLast()
        );

        const openUsage$ = this.openUsage$.pipe(
            throttleTime(1000),
            withLatestFrom(this.tube$),
            switchMap(([data, tube]) => openService.openUsage$(tube, data))
        );

        const createUsage$ = this.createUsage$.pipe(
            throttleTime(1000),
            withLatestFrom(this.tube$),
            map(([event, tube]) => openService.navigateTo(['creator', `${tube.id}`], event.ctrlKey))
        );

        formTube$.pipe(
            subscribeWith(isModified$, selectUnitIndex$, openUsage$, createUsage$, downloadDocument$),
            takeUntil(this.destroyed$)
        ).subscribe();
    }

    public filterOptions<T extends Printable>(list: ReadonlyArray<T>, text: string): ReadonlyArray<T> {
        if (!text?.trim()) {
            return list;
        }

        const ltext = text.trim().toLowerCase();
        return list.filter(item => item.label.toLowerCase().includes(ltext));
    }

    public selectUnitIndex$(index: number): Observable<unknown> {
        return asyncMap(() => {
            const queryParams = { unit: `${index}` } as QueryParams;
            return this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
        });
    }

    public clearControl$(formControl: AbstractControl): Observable<unknown> {
        return asyncMap(() => {
            formControl.reset();
        });
    }

    public canLeave$(): Observable<boolean> {
        return this.saveService.canSave$.pipe(
            take(1),
            switchMap(canSave => {
                if (!canSave) {
                    return of(true);
                }

                const msg = this.translationService.translate('Are you sure you want to leave without saving your changes?<br/>If you click ok, all your changes will be lost.');
                return this.messageBoxDialogService.openConfirmation$(msg).pipe(
                    map(response => response === 'ok')
                );
            })
        );
    }
}
