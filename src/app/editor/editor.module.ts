import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule, Routes } from '@angular/router';

import { InputAutosizeModule } from '../common/input-autosize/input-autosize.module';
import { NumericStepperModule } from '../common/numeric-stepper/numeric-stepper.module';
import { ThrottleEventModule } from '../common/throttle-event/throttle-event.module';
import { MainToolbarModule } from '../components/main-toolbar/main-toolbar.module';
import { UsageViewerModule } from '../components/usage-viewer/usage-viewer.module';
import { LeaveEditionGuardService } from '../services/leave-edition-guard.service';
import { TranslationModule } from '../translation/translation.module';
import { DocumentModule } from './document/document.module';
import { EditorComponent } from './editor.component';
import { UnitModule } from './unit/unit.module';

const routes: Routes = [
    { path: '', component: EditorComponent, canDeactivate: [LeaveEditionGuardService] },
    { path: ':id', component: EditorComponent, canDeactivate: [LeaveEditionGuardService] },
    { path: ':id/:userId', component: EditorComponent, canDeactivate: [LeaveEditionGuardService] },
    { path: ':id/:userId/:duplicate', component: EditorComponent, canDeactivate: [LeaveEditionGuardService] }
];

@NgModule({
    imports: [
        CommonModule,
        DocumentModule,
        RouterModule.forChild(routes),
        FormsModule,
        InputAutosizeModule,
        MainToolbarModule,
        MatButtonModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        NumericStepperModule,
        ReactiveFormsModule,
        ThrottleEventModule,
        TranslationModule,
        UnitModule,
        UsageViewerModule
    ],
    exports: [
        EditorComponent
    ],
    declarations: [
        EditorComponent
    ]
})
export class EditorModule {
}
