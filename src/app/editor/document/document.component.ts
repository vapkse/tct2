import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { FormBuilder, NG_ASYNC_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { Observable } from 'rxjs';

import { Document } from 'src/app/models/document.model';

import { AbstractSubformDirective } from '../../common/abstract-subform/abstract-subform.directive';
import { rowAnimation } from '../../common/animations';
import { ErrorService } from '../../services/error.service';
import { FileService } from '../../services/file.service';

export interface DocumentForm {
    id: string;
    filename: string;
    label: string;
}

export interface OpenDocumentEvent {
    filename: string;
    label: string;
    originalEvent: MouseEvent;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-document',
    templateUrl: './document.component.html',
    styleUrls: ['./document.component.scss'],
    animations: [rowAnimation],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DocumentComponent),
            multi: true
        },
        {
            provide: NG_ASYNC_VALIDATORS,
            useExisting: forwardRef(() => DocumentComponent),
            multi: true
        }
    ]
})
export class DocumentComponent extends AbstractSubformDirective<Document> {
    @Output()
    public readonly deleteDocument = new EventEmitter<Document>();

    @Output()
    public readonly openDocument = new EventEmitter<OpenDocumentEvent>();

    @Input()
    public hasG1: boolean;

    @Input()
    public hasG2: boolean;

    public form$: Observable<IFormGroup<DocumentForm>>;

    public constructor(
        public errorService: ErrorService,
        public fileService: FileService,
        private formBuilder: FormBuilder,
        changeDetectorRef: ChangeDetectorRef
    ) {
        super(changeDetectorRef);
    }

    protected createFormGroup(document: Document): IFormGroup<Document> {
        const fb = this.formBuilder as IFormBuilder;
        return fb.group<DocumentForm>({
            id: document.id,
            label: document.label,
            filename: document.filename
        });
    }
}
