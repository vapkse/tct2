import { ChangeDetectionStrategy, Component, EventEmitter, Input, Optional, Output, Self } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NgControl } from '@angular/forms';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { debounceTime, map, Observable, ReplaySubject, switchMap, tap, withLatestFrom } from 'rxjs';

import { TubeUnit } from 'src/app/models/tube-unit.model';

import { rowAnimation } from '../../common/animations';
import { shareReplayLast, subscribeWith } from '../../common/custom-operators';
import { InputAutoSizeService } from '../../common/input-autosize/input-autosize.service';
import { ErrorService } from '../../services/error.service';

export interface UnitForm {
    vamax: number;
    vamaxp: number;
    pamax: number;
    vg2max: number;
    vg2maxp: number;
    pg2max: number;
    ig2max: number;
    ig2maxp: number;
    ikmax: number;
    ikmaxp: number;
    vg1nmax: number;
    vg1nmaxp: number;
    vhknmax: number;
    vhkpmax: number;
    ig1max: number;
    ig1maxp: number;
    cgk: number;
    cga: number;
    cak: number;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-unit',
    templateUrl: './unit.component.html',
    styleUrls: ['./unit.component.scss'],
    animations: [rowAnimation]
})
export class UnitComponent implements ControlValueAccessor {
    @Output()
    public readonly valueChange = new EventEmitter<TubeUnit>();

    @Input()
    public hasG1: boolean;

    @Input()
    public hasG2: boolean;

    public form$: Observable<IFormGroup<UnitForm>>;

    private unit$ = new ReplaySubject<TubeUnit>(1);
    private disable$ = new ReplaySubject<boolean>(1);

    private unit: TubeUnit;

    public constructor(
        @Self() @Optional() public control: NgControl,
        public errorService: ErrorService,
        formBuilder: FormBuilder,
        inputAutoSizeService: InputAutoSizeService
    ) {
        if (this.control) {
            this.control.valueAccessor = this;
        }

        const form$ = this.unit$.pipe(
            map(unit => {
                const fb = formBuilder as IFormBuilder;
                return fb.group<UnitForm>({
                    vamax: [unit.vamax, inputAutoSizeService.getValidator('vamax')],
                    vamaxp: [unit.vamaxp, inputAutoSizeService.getValidator('vamaxp')],
                    pamax: [unit.pamax, inputAutoSizeService.getValidator('pamax')],
                    vg2max: [unit.vg2max, inputAutoSizeService.getValidator('vg2max')],
                    vg2maxp: [unit.vg2maxp, inputAutoSizeService.getValidator('vg2maxp')],
                    pg2max: [unit.pg2max, inputAutoSizeService.getValidator('pg2max')],
                    ig2max: [unit.ig2max, inputAutoSizeService.getValidator('ig2max')],
                    ig2maxp: [unit.ig2maxp, inputAutoSizeService.getValidator('ig2maxp')],
                    ikmax: [unit.ikmax, inputAutoSizeService.getValidator('ikmax')],
                    ikmaxp: [unit.ikmaxp, inputAutoSizeService.getValidator('ikmaxp')],
                    vg1nmax: [unit.vg1nmax, inputAutoSizeService.getValidator('vg1nmax')],
                    vg1nmaxp: [unit.vg1nmaxp, inputAutoSizeService.getValidator('vg1nmaxp')],
                    vhknmax: [unit.vhknmax, inputAutoSizeService.getValidator('vhknmax')],
                    vhkpmax: [unit.vhkpmax, inputAutoSizeService.getValidator('vhkpmax')],
                    ig1max: [unit.ig1max, inputAutoSizeService.getValidator('ig1max')],
                    ig1maxp: [unit.ig1maxp, inputAutoSizeService.getValidator('ig1maxp')],
                    cgk: [unit.cgk, inputAutoSizeService.getValidator('cgk')],
                    cga: [unit.cga, inputAutoSizeService.getValidator('cga')],
                    cak: [unit.cak, inputAutoSizeService.getValidator('cak')]
                });
            }),
            shareReplayLast()
        );

        const disableForm$ = this.disable$.pipe(
            withLatestFrom(form$),
            tap(([disable, form]) => {
                if (disable) {
                    form.disable();
                } else {
                    form.enable();
                }
            })
        );

        const valueChanged$ = form$.pipe(
            switchMap(form => form.valueChanges.pipe(
                debounceTime(100),
                tap(() => {
                    const values = form.getRawValue();
                    this.unit = {
                        vamax: values.vamax,
                        vamaxp: values.vamaxp,
                        pamax: values.pamax,
                        vg2max: values.vg2max,
                        vg2maxp: values.vg2maxp,
                        pg2max: values.pg2max,
                        ig2max: values.ig2max,
                        ig2maxp: values.ig2maxp,
                        ikmax: values.ikmax,
                        ikmaxp: values.ikmaxp,
                        vg1nmax: values.vg1nmax,
                        vg1nmaxp: values.vg1nmaxp,
                        vhknmax: values.vhknmax,
                        vhkpmax: values.vhkpmax,
                        ig1max: values.ig1max,
                        ig1maxp: values.ig1maxp,
                        cgk: values.cgk,
                        cga: values.cga,
                        cak: values.cak
                    } as TubeUnit;

                    this.onChangeCallback(this.unit);
                    this.valueChange.emit(this.unit);
                })
            ))
        );

        this.form$ = form$.pipe(
            subscribeWith(disableForm$, valueChanged$),
            shareReplayLast()
        );
    }

    // ************* ControlValueAccessor Implementation **************
    // set accessor including call the onchange callback
    @Input()
    public set value(value: TubeUnit) {
        if (value !== this.unit) {
            this.writeValue(value);
            this.onChangeCallback(value);
        }
    }

    // get accessor
    public get value(): TubeUnit {
        return this.unit;
    }

    // From ControlValueAccessor interface
    public writeValue(value: TubeUnit): void {
        if (value !== this.unit) {
            this.unit = value;
            this.unit$.next(value);
        }
    }

    // From ControlValueAccessor interface
    public registerOnChange(fn: (_a: TubeUnit) => void): void {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    public registerOnTouched(fn: () => void): void {
        this.onTouchedCallback = fn;
    }

    public setDisabledState(value: boolean): void {
        this.disable$.next(value);
    }
    // ************* End of ControlValueAccessor Implementation **************

    // ngModel
    public onTouchedCallback = (): void => undefined;
    public onChangeCallback = (_a: TubeUnit): void => undefined;
}
