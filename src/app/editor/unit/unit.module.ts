import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { InputAutosizeModule } from '../../common/input-autosize/input-autosize.module';
import { NumericStepperModule } from '../../common/numeric-stepper/numeric-stepper.module';
import { TranslationModule } from '../../translation/translation.module';
import { UnitComponent } from './unit.component';

@NgModule({
    imports: [
        CommonModule,
        InputAutosizeModule,
        MatFormFieldModule,
        MatInputModule,
        NumericStepperModule,
        ReactiveFormsModule,
        TranslationModule
    ],
    exports: [
        UnitComponent
    ],
    declarations: [
        UnitComponent
    ]
})
export class UnitModule {
}
