import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';

import { InputAutosizeModule } from '../common/input-autosize/input-autosize.module';
import { NumericStepperModule } from '../common/numeric-stepper/numeric-stepper.module';
import { RxIfModule } from '../common/rx-if/rx-if.module';
import { MainToolbarModule } from '../components/main-toolbar/main-toolbar.module';
import { TransferGraphModule } from '../components/transfer-graph/transfer-graph.module';
import { LeaveEditionGuardService } from '../services/leave-edition-guard.service';
import { TranslationModule } from '../translation/translation.module';
import { CreatorComponent } from './creator.component';

const routes: Routes = [
    { path: ':tubeid', component: CreatorComponent, canDeactivate: [LeaveEditionGuardService] },
    { path: ':tubeid/:usageid', component: CreatorComponent, canDeactivate: [LeaveEditionGuardService] },
    { path: ':tubeid/:usageid/:userId', component: CreatorComponent, canDeactivate: [LeaveEditionGuardService] }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        InputAutosizeModule,
        MainToolbarModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        NumericStepperModule,
        ReactiveFormsModule,
        RxIfModule,
        TransferGraphModule,
        TranslationModule
    ],
    exports: [
        CreatorComponent
    ],
    declarations: [
        CreatorComponent
    ]
})
export class CreatorModule {
}
