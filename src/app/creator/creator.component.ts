import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { Series } from 'highcharts';
import { cloneDeep, isEqual, round } from 'lodash-es';
import { combineLatestWith, debounceTime, distinctUntilChanged, filter, map, mergeWith, Observable, of, startWith, Subject, switchMap, take, tap, throttleTime, throwError, withLatestFrom } from 'rxjs';

import { loading$ } from 'src/app/common/rx-if/loading-context';

import { filterMap, shareReplayLast, subscribeWith } from '../common/custom-operators';
import { IdService } from '../common/id.service';
import { InputAutoSizeService } from '../common/input-autosize/input-autosize.service';
import { MessageBoxDialogService } from '../common/message-box-dialog/message-box-dialog.service';
import { LoadingContext } from '../common/rx-if/loading-context';
import { AttachmentsDialogService } from '../components/attachments-dialog/attachments-dialog.service';
import { Tube } from '../models/tube.model';
import { TubeGraph } from '../models/tube-graph.model';
import { TubeGraphFile } from '../models/tube-graph-file.model';
import { TubeMode } from '../models/tube-load.model';
import { TubeUsage, tubeUsageModes } from '../models/tube-usage.model';
import { TubeUsageZoom, TubeUsageZoomX, TubeUsageZoomY } from '../models/tube-usage-zoom.model';
import { AuthService } from '../services/auth/auth.service';
import { chartPattern, databaseChartPattern, isUndefined } from '../services/constants';
import { DatabaseService } from '../services/database.service';
import { ErrorService } from '../services/error.service';
import { FileService } from '../services/file.service';
import { CanLeaveEdition } from '../services/leave-edition-guard.service';
import { SaveService } from '../services/save.service';
import { TubesService } from '../services/tubes.service';
import { UndoRedoService } from '../services/undo-redo.service';
import { UnitService } from '../services/unit.service';
import { TranslationService } from '../translation/translation.service';

interface MainForm {
    id: string;
    name: string;
    note: string;
    mode: TubeMode;
    va: number;
    iazero: number;
    vinpp: number;
    load: number;
    unit: number;
    zoom: TubeUsageZoom;
    series: { [key: string]: false };
}

interface RouteParams extends Params {
    tubeid: string;
    usageid?: string;
    userId?: string;
}

@Component({
    selector: 'app-creator',
    templateUrl: './creator.component.html',
    styleUrls: ['./creator.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        SaveService,
        UndoRedoService,
        UnitService
    ]
})
export class CreatorComponent implements CanLeaveEdition {
    public selectGraph$ = new Subject<void>();
    public zoomChanged$ = new Subject<TubeUsageZoomX | TubeUsageZoomY>();
    public visibilityChanged$ = new Subject<Series>();
    public usageDragDrop$ = new Subject<Partial<TubeUsage>>();
    public usageDragEnd$ = new Subject<TubeUsage>();

    public tube$: Observable<Tube>;
    public mainForm$: Observable<IFormGroup<MainForm>>;
    public formUsage$: Observable<TubeUsage>;
    public graphLoader$: Observable<LoadingContext<TubeGraph>>;

    public modes = tubeUsageModes;

    public constructor(
        public authService: AuthService,
        public errorService: ErrorService,
        public unitService: UnitService,
        private saveService: SaveService,
        private messageBoxDialogService: MessageBoxDialogService,
        private tubeService: TubesService,
        private translationService: TranslationService,
        idService: IdService,
        fileService: FileService,
        databaseService: DatabaseService,
        attachmentsDialogService: AttachmentsDialogService,
        activatedRoute: ActivatedRoute,
        formBuilder: FormBuilder,
        undoRedoService: UndoRedoService<TubeUsage>,
        router: Router,
        inputAutoSizeService: InputAutoSizeService
    ) {

        // Ensure save button is visible
        saveService.canSave$.next(false);

        const fb = formBuilder as IFormBuilder;

        const normalizeUsage = (usage: TubeUsage): TubeUsage => {
            if (!usage.series) {
                usage.series = {};
            }

            if (usage.zoom && !usage.zoom.minY && !usage.zoom.minX && !usage.zoom.maxY && !usage.zoom.maxX) {
                delete usage.zoom;
            }

            Object.keys(usage).forEach(k => {
                const key = k as keyof TubeUsage;
                if (isUndefined(usage[key])) {
                    delete usage[key];
                }
            });

            return usage;
        };

        const routeParams$ = activatedRoute.params.pipe(
            map(params => params as RouteParams),
            shareReplayLast()
        );

        const saveTube$ = this.saveService.save$.pipe(
            throttleTime(1000),
            switchMap(() => this.formUsage$.pipe(
                take(1),
                withLatestFrom(this.tube$),
                switchMap(([formUsage, tube]) => {
                    // Clone tube before modifications to avoid a modification of the original instance
                    const updatedTube = cloneDeep(tube);
                    const usages = [...updatedTube.usages];
                    const isNew = !formUsage.id;
                    if (isNew) {
                        formUsage.id = idService.generate();
                        usages.push(formUsage);
                    } else {
                        const usageIndex = usages.findIndex(u => u.id === formUsage.id);
                        if (usageIndex >= 0) {
                            usages[usageIndex] = formUsage;
                        } else {
                            usages.push(formUsage);
                        }
                    }
                    updatedTube.usages = usages;
                    return this.saveService.saveTube$(updatedTube).pipe(
                        take(1),
                        tap(tubeSaved => {
                            this.tubeService.updateTube$.next(tubeSaved);
                            if (isNew) {
                                void router.navigate(['creator', tube.id, formUsage.id], { queryParamsHandling: 'preserve' });
                            }
                            return undoRedoService.replace$(formUsage);
                        })
                    );
                })
            )),
            shareReplayLast()
        );

        const ensureUserTubesCollection$ = authService.isAdmin$.pipe(
            combineLatestWith(routeParams$),
            map(([isAdmin, routeParams]) => {
                if (isAdmin && routeParams.userId) {
                    tubeService.showUserTubes$.next(true);
                }
                return routeParams;
            }),
            shareReplayLast()
        );

        this.tube$ = ensureUserTubesCollection$.pipe(
            switchMap(routeParams => tubeService.getTube$(+routeParams.tubeid, routeParams.userId)),
            switchMap(tube => tube ? of(tube) : throwError(() => new Error('tube not found.'))),
            mergeWith(saveTube$),
            tap(tube => unitService.tube$.next(tube)),
            shareReplayLast()
        );

        const usage$ = routeParams$.pipe(
            combineLatestWith(this.tube$),
            map(([routeParams, tube]) => routeParams.usageid ? tube.usages.find(u => u.id === routeParams.usageid) : {} as TubeUsage),
            switchMap(usage => usage ? of(usage) : throwError(() => new Error('tube usage not found.'))),
            map(usage => normalizeUsage(usage)),
            shareReplayLast()
        );

        this.mainForm$ = usage$.pipe(
            mergeWith(undoRedoService.current$, this.usageDragEnd$),
            combineLatestWith(this.unitService.hasMultiUnits$),
            withLatestFrom(this.tube$),
            map(([[usage, hasMultiUnits], tube]) => {
                console.log('creating form group');
                const usageMapByName = tube.usages.reduce((m, u) => m.set(u.label.toLocaleLowerCase().trim(), u), new Map<string, TubeUsage>());

                const nameValidator = (control: AbstractControl): ValidationErrors => {
                    const name = (control.value as string)?.toLocaleLowerCase().trim();
                    if (!name) {
                        return { required: '' } as ValidationErrors;
                    }
                    const existing = usageMapByName.get(name);
                    if (existing && existing.id !== usage.id) {
                        return { duplicate: '' } as ValidationErrors;
                    } else {
                        return null as ValidationErrors;
                    }
                };

                return fb.group<MainForm>({
                    id: usage.id,
                    name: [usage.label, nameValidator],
                    mode: [usage.mode, Validators.required],
                    unit: hasMultiUnits ? [usage.unit, Validators.required] : usage.unit,
                    note: usage.note,
                    va: [round(usage.va, 0), [Validators.required, inputAutoSizeService.getValidator('va')]],
                    iazero: [round(usage.iazero, 2), [Validators.required, inputAutoSizeService.getValidator('iazero')]],
                    vinpp: [round(usage.vinpp, 2), [Validators.required, inputAutoSizeService.getValidator('vinpp')]],
                    load: [round(usage.load, 0), [Validators.required, inputAutoSizeService.getValidator('load')]],
                    zoom: usage.zoom,
                    series: usage.series
                });
            }),
            shareReplayLast()
        );

        let selectedGraphId$ = of(undefined as string);

        const selectedGraph$ = this.selectGraph$.pipe(
            throttleTime(1000),
            switchMap(() => selectedGraphId$.pipe(
                take(1)
            )),
            withLatestFrom(this.tube$),
            switchMap(([selectedId, tube]) => attachmentsDialogService.open$({ tube, selectedId, documentFilter: chartPattern })),
            shareReplayLast()
        );

        selectedGraphId$ = usage$.pipe(
            mergeWith(undoRedoService.current$),
            map(usage => usage?.traces),
            mergeWith(selectedGraph$),
            startWith(undefined as string),
            distinctUntilChanged(),
            shareReplayLast()
        );

        this.graphLoader$ = selectedGraphId$.pipe(
            combineLatestWith(this.tube$),
            filterMap(([graphId, tube]) => {
                const document = graphId && tube.documents.find(d => d.id === graphId);
                if (document) {
                    return [document, tube] as const;
                }

                return undefined;
            }),
            switchMap(([document, tube]) => {
                if (!document) {
                    return of(undefined);
                }

                let graph$: Observable<TubeGraphFile>;
                if (databaseChartPattern.test(document.filename)) {
                    graph$ = databaseService.getChart$(tube.userId, document.filename);
                } else {
                    graph$ = fileService.getGraphContent$(`assets/usages/${document.filename}`);
                }

                return loading$(graph$);
            }),
            map(loader => {
                if (loader?.data) {
                    return new LoadingContext(false, loader.data.tubeGraph);
                }
                return new LoadingContext(loader?.loading || false, undefined as TubeGraph, loader?.error);
            }),
            shareReplayLast()
        );

        const zoomChanged$ = this.zoomChanged$.pipe(
            withLatestFrom(this.mainForm$),
            map(([zoom, form]) => {
                const currentZoom = form.value.zoom;
                const newZoom = { ...currentZoom, ...zoom };
                if (isEqual(newZoom, currentZoom)) {
                    return undefined;
                }
                form.controls.zoom.setValue(newZoom, { emitEvent: false });
                return form.value;
            }),
            filter(Boolean),
            debounceTime(300),
            tap(zoomChanged => {
                console.log('zoom changed emitted', zoomChanged);
            })
        );

        const visibilityChanged$ = this.visibilityChanged$.pipe(
            withLatestFrom(this.mainForm$),
            map(([serie, form]) => {
                const currentSeries = form.value.series;
                let newSeries = cloneDeep(currentSeries);
                if (serie.visible) {
                    delete newSeries?.[serie.options.id];
                } else {
                    if (!newSeries) {
                        newSeries = {} as { [key: string]: false };
                    }
                    newSeries[serie.options.id] = false;
                }
                if (isEqual(newSeries, currentSeries)) {
                    return undefined;
                }
                form.controls.series.setValue(newSeries, { emitEvent: false });
                return form.value;
            }),
            filter(Boolean),
            debounceTime(300),
            tap(visibilityChanged => {
                console.log('visibility changed emitted', visibilityChanged);
            })
        );

        // Is modified observable
        const isModified$ = usage$.pipe(
            switchMap(usage => this.formUsage$.pipe(
                withLatestFrom(this.mainForm$),
                map(([formUsage, mainForm]) => {
                    if (mainForm.invalid) {
                        saveService.canSave$.next(false);
                        console.log('usage invalid', usage, formUsage);
                    } else {
                        const isModified = !isEqual(formUsage, usage);
                        if (isModified) {
                            console.log('usage modified', usage, formUsage);
                        }
                        saveService.canSave$.next(isModified);
                    }
                })
            )),
            shareReplayLast()
        );

        const usageChangeByDragAndDrop$ = this.usageDragDrop$.pipe(
            switchMap(usage => this.mainForm$.pipe(
                tap(form => {
                    const partial = {} as Partial<TubeUsage>;
                    if (usage.va) {
                        partial.va = round(usage.va, 0);
                    }
                    if (usage.iazero) {
                        partial.iazero = round(usage.iazero, 2);
                    }
                    if (usage.vinpp) {
                        partial.vinpp = round(usage.vinpp, 2);
                    }
                    if (usage.load) {
                        partial.load = round(usage.load, 0);
                    }
                    form.patchValue(partial, { emitEvent: false });
                })
            ))
        );

        // output usage observable
        this.formUsage$ = this.mainForm$.pipe(
            switchMap(mainForm => of(mainForm.value).pipe(
                mergeWith(mainForm.valueChanges.pipe(
                    map(value => {
                        console.log('value changed emit', value);
                        return mainForm.getRawValue();
                    })
                ), zoomChanged$, visibilityChanged$)
            )),
            combineLatestWith(selectedGraphId$),
            map(([values, selectedGraphId]) => {
                const formUsage = {
                    id: values.id,
                    label: values.name,
                    va: values.va,
                    iazero: values.iazero,
                    vinpp: values.vinpp,
                    mode: values.mode,
                    note: values.note,
                    load: values.load,
                    unit: values.unit,
                    traces: selectedGraphId,
                    zoom: {
                        minX: values.zoom?.minX,
                        maxX: values.zoom?.maxX,
                        minY: values.zoom?.minY,
                        maxY: values.zoom?.maxY
                    },
                    series: values.series
                } as TubeUsage;

                unitService.selectUnitIndex$.next(values.unit);

                return normalizeUsage(formUsage);
            }),
            distinctUntilChanged((u1, u2) => isEqual(u1, u2)),
            switchMap(formUsage => {
                console.log('new usage calculated', formUsage);
                return undoRedoService.add$(formUsage).pipe(
                    take(1)
                );
            }),
            subscribeWith(isModified$, usageChangeByDragAndDrop$),
            shareReplayLast()
        );
    }

    public canLeave$(): Observable<boolean> {
        return this.saveService.canSave$.pipe(
            take(1),
            switchMap(canSave => {
                if (!canSave) {
                    return of(true);
                }

                const msg = this.translationService.translate('Are you sure you want to leave without saving your changes?<br/>If you click ok, all your changes will be lost.');
                return this.messageBoxDialogService.openConfirmation$(msg).pipe(
                    map(response => response === 'ok')
                );
            })
        );
    }
}
