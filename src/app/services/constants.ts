export const imageFileTypes = ['image/apng', 'image/bmp', 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/webp', 'image/x-icon'];
export const jsonFileType = ['application/JSON'];
export const documentFileType = ['application/pdf'];
export const allAcceptedFileType = [...imageFileTypes, ...jsonFileType, ...documentFileType];

export const imageExtensions = 'tif|pjp|xbm|jxl|svgz|jpg|jpeg|ico|tiff|gif|svg|jfif|webp|png|bmp|pjpeg|avif';
export const jsonExtensions = 'json';
export const documentExtensions = 'pdf';
export const chartWizardAcceptedExtension = `${imageExtensions}|${documentExtensions}`;
export const allAcceptedExtension = `${imageExtensions}|${jsonExtensions}|${documentExtensions}`;

export const imageExtensionPattern = new RegExp(`[^\\s]+(.*?)\\.(${imageExtensions})$`, 'i');
export const jsonExtensionPattern = new RegExp(`[^\\s]+(.*?)\\.(${jsonExtensions})$`, 'i');
export const documentExtensionPattern = new RegExp(`[^\\s]+(.*?)\\.(${documentExtensions})$`, 'i');
export const chartWizardAcceptedExtensionPattern = new RegExp(`[^\\s]+(.*?)\\.(${chartWizardAcceptedExtension})$`, 'i');
export const allAcceptedExtensionPattern = new RegExp(`[^\\s]+(.*?)\\.(${allAcceptedExtension})$`, 'i');

export const databaseChartPattern = new RegExp('^fire://');

export const chartPattern = new RegExp(`[^\\s]+(.*?)\\.(${jsonExtensions})$|^fire://`);

export const isUndefined = (v: unknown): boolean => v === undefined || v === null;
export const isDefined = (v: unknown): boolean => v !== undefined && v !== null;
