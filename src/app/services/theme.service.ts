import { Injectable } from '@angular/core';
import { combineLatestWith, map, Observable, of, startWith, Subject, tap } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { materialPallets } from '../models/chart-colors';

export type Theme = 'darkBlue' | 'lightBlue' | 'darkGreen' | 'lightGreen' | 'darkPink' | 'lightPink' | 'darkOrange' | 'lightOrange' | 'darkBlueGray' | 'lightBlueGray' | 'darkCyan' | 'lightTeal';

export interface ThemeItem {
    theme: Theme;
    label: string;
    color: string;
}

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    public themeItems: ReadonlyArray<ThemeItem>;
    public theme$ = new Subject<Theme>();
    public currentTheme$: Observable<Theme>;
    public isDark$: Observable<boolean>;
    public materialPallets$: Observable<Array<Record<string, string>>>;

    public constructor() {
        this.themeItems = [{
            theme: 'darkBlue',
            label: 'Dark Blue',
            color: '#3097f4'
        }, {
            theme: 'lightBlue',
            label: 'Light Blue',
            color: '#1f5f9a'
        }, {
            theme: 'darkGreen',
            label: 'Dark Green',
            color: '#8bc34a'
        }, {
            theme: 'lightGreen',
            label: 'Light Green',
            color: '#5c8130'
        }, {
            theme: 'darkPink',
            label: 'Dark Pink',
            color: '#e91e63'
        }, {
            theme: 'lightPink',
            label: 'Light Pink',
            color: '#a91e4d'
        }, {
            theme: 'darkOrange',
            label: 'Dark Orange',
            color: '#ff9800'
        }, {
            theme: 'lightOrange',
            label: 'Light Orange',
            color: '#bb7103'
        }, {
            theme: 'darkBlueGray',
            label: 'Dark BlueGray',
            color: '#53728e'
        }, {
            theme: 'lightBlueGray',
            label: 'Light BlueGray',
            color: '#0a2034'
        }, {
            theme: 'darkCyan',
            label: 'Dark Cyan',
            color: '#00bcd4'
        }, {
            theme: 'lightTeal',
            label: 'Light Teal',
            color: '#009688'
        }] as ReadonlyArray<ThemeItem>;

        const restoreTheme = (): Theme => {
            let theme: Theme;
            try {
                theme = localStorage.getItem('tct-theme') as Theme;
            } catch (_e) {
                console.log('Fail to get your prefered color from the local storage.');
            }

            if (!theme) {
                theme = 'lightBlue';
            }

            return theme;
        };

        this.currentTheme$ = this.theme$.pipe(
            tap(theme => {
                try {
                    localStorage.setItem('tct-theme', theme);
                } catch (_e) {
                    console.log('Fail to set your prefered color to the local storage.');
                }
            }),
            startWith(restoreTheme()),
            tap(theme => {
                document.body.setAttribute('theme', theme);
                document.body.setAttribute('dark-theme', (theme as string).startsWith('dark') ? 'true' : 'false');
            }),
            shareReplayLast()
        );

        this.isDark$ = this.currentTheme$.pipe(
            map((theme: string) => theme.startsWith('dark')),
            shareReplayLast()
        );

        this.materialPallets$ = of(materialPallets).pipe(
            combineLatestWith(this.currentTheme$),
            map(([pallets, currentTheme]) => {
                const themeItem = this.themeItems.find(t => t.theme === currentTheme) || this.themeItems[1];
                const themeItemR = parseInt(themeItem.color[1] + themeItem.color[2], 16);
                const themeItemG = parseInt(themeItem.color[3] + themeItem.color[4], 16);
                const themeItemB = parseInt(themeItem.color[5] + themeItem.color[6], 16);

                const diff = (color: string): number => {
                    const colorR = parseInt(color[1] + color[2], 16);
                    const colorG = parseInt(color[3] + color[4], 16);
                    const colorB = parseInt(color[5] + color[6], 16);
                    return Math.abs(themeItemR - colorR) + Math.abs(themeItemG - colorG) + Math.abs(themeItemB - colorB);
                };

                pallets.sort((p1, p2) => diff(p1[500]) - diff(p2[500]));
                return pallets;
            }),
            shareReplayLast()
        );
    }
}
