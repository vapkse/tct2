import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { from, Observable, of, switchMap, take } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class UnauthGuardService implements CanActivate {
    public constructor(private authService: AuthService, private router: Router) { }

    public canActivate(): Observable<boolean> {
        return from(this.authService.isAuthenticated$).pipe(
            take(1),
            switchMap(isAuthenticated => {
                if (isAuthenticated) {
                    void this.router.navigate(['/home']);
                    return of(true);
                }

                return of(false);
            })
        );
    }
}
