import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, NavigationExtras, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of, switchMap } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    public constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    public canActivate(_route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.isAuthenticated$.pipe(
            switchMap(isAuthenticated => {
                if (!isAuthenticated) {
                    const extras = {
                        queryParams: { navto: state.url },
                        skipLocationChange: true
                    } as NavigationExtras;
                    void this.router.navigate(['/sign-in'], extras);
                    return of(false);
                }

                return of(true);
            })
        );
    }
}
