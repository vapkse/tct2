import { Injectable } from '@angular/core';
import { Auth, AuthProvider as FirebaseAuthProvider, authState, getAuth, GithubAuthProvider, GoogleAuthProvider, signInWithEmailAndPassword, signInWithPopup, User as FirebaseUser, UserCredential } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { FirebaseError } from 'firebase/app';
import { createUserWithEmailAndPassword, sendPasswordResetEmail } from 'firebase/auth';
import { catchError, from, map, Observable, of, take, tap, throwError } from 'rxjs';

import { shareReplayLast } from '../../common/custom-operators';
import { DestroyDirective } from '../../common/destroy/destroy.directive';

export type AuthProvider = 'google' | 'github';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends DestroyDirective {
    public authenticatedUser$: Observable<FirebaseUser>;
    public isAuthenticated$: Observable<boolean>;
    public isAdmin$: Observable<boolean>;

    private auth: Auth;

    public constructor(public firestore: Firestore) {
        super();

        this.auth = getAuth();

        this.authenticatedUser$ = authState(this.auth).pipe(
            tap(user => {
                console.log('Authenticated User', user);
            }),
            shareReplayLast()
        );

        this.isAuthenticated$ = this.authenticatedUser$.pipe(
            map(Boolean),
            shareReplayLast()
        );

        this.isAdmin$ = this.authenticatedUser$.pipe(
            map(user => {
                const adminIds = ['m089IxfDIXfzS7fq9kfIv9ZGDKg2'];
                return user && adminIds.includes(user.uid);
            }),
            shareReplayLast()
        );
    }

    public signInWithEmail$(eMail: string, password: string): Observable<UserCredential> {
        return from(signInWithEmailAndPassword(this.auth, eMail, password));
    }

    public sendPasswordResetEmail$(eMail: string): Observable<void> {
        return from(sendPasswordResetEmail(this.auth, eMail));
    }

    public signInWithProvider$(provider: AuthProvider): Observable<UserCredential> {
        let firebaseprovider: FirebaseAuthProvider;
        switch (provider) {
            case 'google':
                firebaseprovider = new GoogleAuthProvider();
                break;
            case 'github':
                firebaseprovider = new GithubAuthProvider();
                break;
            default:
                return throwError(() => new Error('Invalid provider'));
        }

        return from(signInWithPopup(this.auth, firebaseprovider)).pipe(
            catchError((err: unknown) => {
                if (err instanceof FirebaseError && err.code === 'auth/popup-closed-by-user') {
                    return of(null as UserCredential);
                }
                return throwError(() => err);
            }),
            take(1)
        );
    }

    public createUserWithEmailAndPassword$(eMail: string, password: string): Observable<UserCredential> {
        return from(createUserWithEmailAndPassword(this.auth, eMail, password));
    }

    public signOut$(): Observable<void> {
        return from(this.auth.signOut());
    }
}
