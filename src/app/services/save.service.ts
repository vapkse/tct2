import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { doc, setDoc } from 'firebase/firestore';
import { BehaviorSubject, combineLatestWith, from, map, Observable, startWith, Subject, switchMap, take, throwError, withLatestFrom } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { Tube } from '../models/tube.model';
import { TubeGraph } from '../models/tube-graph.model';
import { TubeGraphFile } from '../models/tube-graph-file.model';
import { AuthService } from './auth/auth.service';
import { TubesService } from './tubes.service';

@Injectable()
export class SaveService {
    public canSave$ = new BehaviorSubject<boolean>(undefined);
    public save$ = new Subject<void>();

    private isAdminSafe$: Observable<boolean>;

    public constructor(
        private fireStore: Firestore,
        private authService: AuthService,
        private tubesService: TubesService
    ) {
        this.isAdminSafe$ = authService.isAdmin$.pipe(
            startWith(false),
            shareReplayLast()
        );
    }

    public saveTube$(tube: Tube, mergedFlag = false): Observable<Tube> {
        if (!tube.label) {
            return throwError(() => new Error('Missing required value: tube.label.'));
        }

        if (!tube.typeId) {
            return throwError(() => new Error('Missing required value: tube.type.'));
        }

        return this.authService.authenticatedUser$.pipe(
            combineLatestWith(this.tubesService.tubes$, this.tubesService.tubeMapById$),
            take(1),
            withLatestFrom(this.isAdminSafe$),
            switchMap(([[user, tubes, tubeMapById], isAdmin]) => {
                if (!user) {
                    return throwError(() => new Error('Not authenticated.'));
                }

                const getNewId = (): number => tubes.reduce((id, t) => {
                    if (id <= t.id) {
                        id = t.id + 1;
                    }
                    return id;
                }, 0);

                // Check id, and create a new one in case of
                if (!tube.id) {
                    tube.id = getNewId();
                } else {
                    const currentTube = tubeMapById.get(tube.id);
                    if (currentTube.label !== tube.label) {
                        tube.id = getNewId();
                    }
                }

                const loggedUserId = user.email || user.uid;
                const userId = (isAdmin && tube.userId) || loggedUserId;

                const docRef = doc(this.fireStore, `tubes/${userId}#${tube.id}`);
                tube.userId = userId;
                tube.date = Date.now();
                tube.merged = mergedFlag;
                return from(setDoc(docRef, tube)).pipe(
                    map(() => tube)
                );
            })
        );
    }

    public saveChart$(tubeId: number, tubeGraph: TubeGraph): Observable<TubeGraphFile> {
        return this.authService.authenticatedUser$.pipe(
            take(1),
            switchMap(user => {
                if (!user) {
                    return throwError(() => new Error('Not authenticated.'));
                }

                const userId = user.email || user.uid;
                const filename = tubeGraph.name.replace(/[ ]/g, '_');

                const docRef = doc(this.fireStore, `charts/${userId}#${tubeId}_${filename}`);

                const graphFile = {
                    tubeGraph,
                    userId,
                    date: Date.now(),
                    filename: `fire://${tubeId}_${filename}`
                } as TubeGraphFile;

                return from(setDoc(docRef, graphFile)).pipe(
                    map(() => graphFile)
                );
            })
        );
    }
}
