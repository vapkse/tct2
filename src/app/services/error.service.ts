import { ErrorHandler, Injectable, NgZone } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { FirebaseError } from 'firebase/app';
import { takeUntil } from 'rxjs';

import { DestroyDirective } from '../common/destroy/destroy.directive';
import { StatusService } from '../common/status/status.service';
import { TranslationService } from '../translation/translation.service';

const validationErrorMap = new Map<string, string>([
    ['min', 'Value too small.'],
    ['max', 'Value too big.'],
    ['required', 'This value is required.'],
    ['email', 'Wrong email format.'],
    ['duplicate', 'This value already exists.'],
    ['passwordnotmatch', 'The passwords don\'t match.'],
    ['auth/email-already-in-use', 'Email already in use.'],
    ['auth/wrong-password', 'Wrong email or password.']
]);

@Injectable({
    providedIn: 'root'
})
export class ErrorService extends DestroyDirective implements ErrorHandler {
    public constructor(
        private statusService: StatusService,
        private translationService: TranslationService,
        private zone: NgZone
    ) {
        super();
    }

    public handleError(error: unknown): void {
        this.zone.run(() => {
            console.error('Error from global error handler', error);
            this.statusService.showStatus$(error).pipe(
                takeUntil(this.destroyed$)
            ).subscribe();
        });

    }

    public toValidationError(err: unknown): ValidationErrors {
        const validationErrors = {} as ValidationErrors;
        if (err instanceof FirebaseError) {
            validationErrors[err.code] = err.message;
        } else if (err instanceof Error) {
            validationErrors[err.name] = err.message;
        } else {
            validationErrors.unknown = err;
        }
        return validationErrors;
    }

    public getError(errors: ValidationErrors): string {
        if (!errors) {
            return undefined;
        }
        let error: string;
        const errorKey = Object.keys(errors).find(key => validationErrorMap.has(key));
        if (errorKey) {
            error = validationErrorMap.get(errorKey);
        } else {
            const firstKey = Object.keys(errors)[0];
            error = (firstKey && errors[firstKey] as string) || firstKey || undefined;
        }

        return this.translationService.translate(error);
    }
}
