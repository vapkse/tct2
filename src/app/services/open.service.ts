import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, Observable, take } from 'rxjs';

import { StatusService } from '../common/status/status.service';
import { TransferGraphData, TransferGraphDialogService } from '../components/transfer-graph-dialog/transfer-graph-dialog.service';
import { Tube } from '../models/tube.model';
import { chartPattern } from './constants';
import { FileService } from './file.service';
import { TubesService } from './tubes.service';

export interface DownloadDocumentData {
    filename: string;
    userId: string;
    label: string;
    originalEvent: MouseEvent;
}

export interface UsageData {
    usageId: string;
    originalEvent: MouseEvent;
}

export interface EditTubeData {
    duplicate: boolean;
    originalEvent: MouseEvent;
}

@Injectable({
    providedIn: 'root'
})
export class OpenService {
    public downloadDocument$: (data: DownloadDocumentData) => Observable<unknown>;
    public openUsage$: (tube: Tube, data: UsageData) => Observable<unknown>;
    public viewTube$: (tube: Tube, openInNewTab: boolean) => Observable<unknown>;
    public editTube$: (tube: Tube, data: EditTubeData) => Observable<unknown>;

    public constructor(
        private router: Router,
        transferGraphDialogService: TransferGraphDialogService,
        statusService: StatusService,
        tubeService: TubesService,
        fileService: FileService
    ) {

        this.downloadDocument$ = (data: DownloadDocumentData): Observable<unknown> => {
            if (chartPattern.test(data.filename)) {
                return transferGraphDialogService.open$({
                    userId: data.userId,
                    filename: data.filename
                } as TransferGraphData);
            } else {
                return fileService.downloadDocument$(data.filename, data.label, data.originalEvent.ctrlKey).pipe(
                    catchError(err => statusService.showStatus$(err as Error))
                );
            }
        };

        this.openUsage$ = (tube: Tube, data: UsageData): Observable<unknown> => tubeService.showUserTubes$.pipe(
            take(1),
            map(showUserTubes => {
                const routeSegments = showUserTubes ? ['creator', `${tube.id}`, `${data.usageId}`, `${tube.userId}`] : ['creator', `${tube.id}`, `${data.usageId}`];
                this.navigateTo(routeSegments, data.originalEvent.ctrlKey);
            })
        );

        this.viewTube$ = (tube: Tube, openInNewTab: boolean): Observable<unknown> => tubeService.showUserTubes$.pipe(
            take(1),
            map(showUserTubes => {
                const routeSegments = showUserTubes ? ['view', `${tube.id}`, `${tube.userId}`] : ['view', `${tube.id}`];
                this.navigateTo(routeSegments, openInNewTab);
            })
        );

        this.editTube$ = (tube: Tube, data: EditTubeData): Observable<unknown> => tubeService.showUserTubes$.pipe(
            take(1),
            map(showUserTubes => {
                const routeSegments = ['edit', `${tube.id}`];
                if (showUserTubes) {
                    routeSegments.push(`${tube.userId}`);
                }
                if (data.duplicate) {
                    routeSegments.push('duplicate');
                }
                this.navigateTo(routeSegments, data.originalEvent.ctrlKey);
            })
        );
    }

    public navigateTo(routeSegments: string[], openInNewTab: boolean): void {
        const urlTree = this.router.createUrlTree(routeSegments, { queryParamsHandling: 'preserve' });
        delete urlTree.queryParams.usage;
        if (openInNewTab) {
            const url = this.router.serializeUrl(urlTree);
            window.open(url, '_blank');
        } else {
            void this.router.navigateByUrl(urlTree);
        }
    }
}
