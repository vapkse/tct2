import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, startWith, take, tap } from 'rxjs';

import { filterMap, shareReplayLast } from '../common/custom-operators';

@Injectable()
export class UndoRedoService<T> {
    public pointer$ = new BehaviorSubject<number>(undefined);

    public current$: Observable<T>;
    public canUndo$: Observable<boolean>;
    public canRedo$: Observable<boolean>;

    private stack = new Array<T>();
    private lastSatckedTime = 0;

    public constructor() {
        this.canUndo$ = this.pointer$.pipe(
            map(pointer => (pointer > 0 || pointer === undefined) && this.stack.length > 1),
            startWith(false),
            shareReplayLast()
        );

        this.canRedo$ = this.pointer$.pipe(
            map(pointer => pointer < this.stack.length - 1 && pointer >= 0),
            startWith(false),
            shareReplayLast()
        );

        this.current$ = this.pointer$.pipe(
            filterMap(pointer => {
                if (pointer !== undefined) {
                    const stack = this.stack[pointer];
                    if (stack) {
                        this.lastSatckedTime = Date.now();
                        console.log('UndoRedoService, restore stack', pointer);
                        return stack;
                    }
                }

                return undefined;
            }),
            shareReplayLast()
        );
    }

    public add$(value: T): Observable<T> {
        return this.pointer$.pipe(
            take(1),
            map(currentPointer => {
                const now = Date.now();
                if (now - this.lastSatckedTime > 200) {
                    console.log('UndoRedoService, add undo stack');
                    // Delete all entries after currentPointer
                    if (currentPointer === undefined) {
                        currentPointer = this.stack.length - 1;
                    }
                    this.stack.splice(currentPointer + 1);
                    this.stack.push(value);
                    this.lastSatckedTime = now;
                    this.pointer$.next(undefined);
                }
                return value;
            })
        );
    }

    public replace$(value: T): Observable<T> {
        return this.pointer$.pipe(
            take(1),
            map(pointer => {
                console.log('UndoRedoService, replace undo stack');
                if (pointer === undefined) {
                    pointer = this.stack.length - 1;
                }
                this.stack[pointer] = value;
                return value;
            })
        );
    }

    public undo$(): Observable<unknown> {
        return this.pointer$.pipe(
            take(1),
            tap(pointer => {
                if (pointer === undefined) {
                    pointer = this.stack.length - 1;
                }
                this.pointer$.next(pointer - 1);
            })
        );
    }

    public redo$(): Observable<unknown> {
        return this.pointer$.pipe(
            take(1),
            tap(pointer => {
                this.pointer$.next(pointer + 1);
            })
        );
    }
}
