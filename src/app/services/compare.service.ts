import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, startWith } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { AuthService } from './auth/auth.service';

export type CompareView = 'user' | 'global' | 'comparator';

@Injectable()
export class CompareService {
    public isAdminSafe$: Observable<boolean>;
    public compareView$ = new ReplaySubject<CompareView>(1);

    public constructor(
        authService: AuthService
    ) {
        this.isAdminSafe$ = authService.isAdmin$.pipe(
            startWith(false),
            shareReplayLast()
        );
    }
}
