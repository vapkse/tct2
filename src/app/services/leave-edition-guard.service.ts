import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

export interface CanLeaveEdition {
    canLeave$: () => Observable<boolean>;
}

@Injectable({
    providedIn: 'root'
})
export class LeaveEditionGuardService implements CanDeactivate<CanLeaveEdition> {
    public canDeactivate(component: CanLeaveEdition, _route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        console.log('LeaveEditionGuardService: canDeactivate asked for url', state.url);
        return component.canLeave$ ? component.canLeave$() : true;
    }
}
