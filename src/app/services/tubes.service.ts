import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatestWith, map, mergeWith, Observable, Subject, switchMap, take, throwError } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { Document } from '../models/document.model';
import { Tube } from '../models/tube.model';
import { TubeBase } from '../models/tube-base.model';
import { TubePinout } from '../models/tube-pinout.model';
import { TubeType } from '../models/tube-type.model';
import { AuthService } from './auth/auth.service';
import { chartPattern } from './constants';
import { DatabaseService } from './database.service';


@Injectable({
    providedIn: 'root'
})
export class TubesService {
    public removeTube$ = new Subject<Tube>();
    public updateTube$ = new Subject<Tube>();
    public showUserTubes$ = new BehaviorSubject<boolean>(false);
    public updateReferential$ = new Subject<Array<Tube>>();

    public tubeReferential$: Observable<Array<Tube>>;
    public tubeReferentialMap$: Observable<Map<number, Tube>>;

    public tubes$: Observable<ReadonlyArray<Tube>>;
    public tubeMapById$: Observable<Map<number, Tube>>;
    public tubeMapByName$: Observable<Map<string, Tube>>;
    public tubeMapByNameLowerCase$: Observable<Map<string, Tube>>;

    public tubeTypes$: Observable<ReadonlyArray<TubeType>>;
    public tubeTypeMap$: Observable<Map<number, TubeType>>;

    public tubeBases$: Observable<ReadonlyArray<TubeBase>>;
    public tubeBaseMap$: Observable<Map<number, TubeBase>>;

    public tubePinouts$: Observable<ReadonlyArray<TubePinout>>;
    public tubePinoutMap$: Observable<Map<number, TubePinout>>;

    // Access for admin only
    public globalTubes$: Observable<ReadonlyArray<Tube>>;
    public userTubes$: Observable<ReadonlyArray<Tube>>;

    public constructor(
        databaseService: DatabaseService,
        httpClient: HttpClient,
        authService: AuthService
    ) {
        this.tubeReferential$ = httpClient.get<Array<Tube>>('assets/tubes.json').pipe(
            mergeWith(this.updateReferential$),
            shareReplayLast()
        );

        this.tubeReferentialMap$ = this.tubeReferential$.pipe(
            map(tubes => tubes.reduce((m, tube) => m.set(tube.id, tube), new Map<number, Tube>())),
            shareReplayLast()
        );

        const globalTubesRemoved$ = this.removeTube$.pipe(
            switchMap(tubeRemoved => this.globalTubes$.pipe(
                take(1),
                switchMap(tubes =>
                    // Replace user tube in the main database
                    this.tubeReferentialMap$.pipe(
                        take(1),
                        map(tubeReferentialMap => {
                            const originalTube = tubeReferentialMap.get(tubeRemoved.id);
                            if (originalTube) {
                                tubes = tubes.map(t => t.id === tubeRemoved.id ? originalTube : t);
                            } else {
                                tubes = tubes.filter(t => t.id !== tubeRemoved.id || t.userId !== tubeRemoved.userId);
                            }
                            return tubes;
                        })
                    )
                )
            )),
            shareReplayLast()
        );

        const globalTubesUpdated$ = this.updateTube$.pipe(
            switchMap(tubeUpdated => this.globalTubes$.pipe(
                take(1),
                map(tubes => {
                    const tubesUpdated = [...tubes];
                    const tubeIndex = tubes.findIndex(t => t.id === tubeUpdated.id);
                    if (tubeIndex >= 0) {
                        tubesUpdated.splice(tubeIndex, 1, tubeUpdated);
                    } else {
                        tubesUpdated.push(tubeUpdated);
                    }
                    return tubesUpdated;
                })
            )),
            shareReplayLast()
        );

        this.globalTubes$ = this.tubeReferential$.pipe(
            combineLatestWith(databaseService.userTubes$),
            map(([tubes, userTubes]) => {
                if (userTubes?.length) {
                    // Merge user base in main base
                    const userTubeMap = userTubes.reduce((m, tube) => m.set(tube.id, tube), new Map<number, Tube>());
                    tubes = tubes.map(tube => {
                        const userTube = userTubeMap.get(tube.id);
                        if (userTube) {
                            userTubeMap.delete(tube.id);
                            return userTube;
                        } else {
                            return tube;
                        }
                    });

                    if (userTubeMap.size) {
                        userTubeMap.forEach(tube => tubes.push(tube));
                    }
                }

                tubes.sort((t1, t2) => t1.label.localeCompare(t2.label));
                return tubes;
            }),
            mergeWith(globalTubesRemoved$, globalTubesUpdated$),
            shareReplayLast()
        );

        const userTubesRemoved$ = this.removeTube$.pipe(
            switchMap(tubeRemoved => this.userTubes$.pipe(
                take(1),
                map(tubes => tubes.filter(t => t.id !== tubeRemoved.id || t.userId !== tubeRemoved.userId)) // Remove tube from the list
            )),
            shareReplayLast()
        );

        const userTubesUpdated$ = this.updateTube$.pipe(
            switchMap(tubeUpdated => this.userTubes$.pipe(
                take(1),
                map(tubes => {
                    const tubesUpdated = [...tubes];
                    const tubeIndex = tubes.findIndex(t => t.id === tubeUpdated.id && t.userId === tubeUpdated.userId);
                    if (tubeIndex >= 0) {
                        tubesUpdated.splice(tubeIndex, 1, tubeUpdated);
                    } else {
                        tubesUpdated.push(tubeUpdated);
                    }
                    return tubesUpdated;
                })
            )),
            shareReplayLast()
        );

        this.userTubes$ = authService.isAdmin$.pipe(
            switchMap(isAdmin => {
                if (!isAdmin) {
                    return throwError(() => new Error('This feature is available only for administrator.'));
                }

                return databaseService.allUserTube$;
            }),
            mergeWith(userTubesRemoved$, userTubesUpdated$),
            shareReplayLast()
        );

        this.tubes$ = this.showUserTubes$.pipe(
            switchMap(showUserTubes => showUserTubes ? this.userTubes$ : this.globalTubes$),
            shareReplayLast()
        );

        this.tubeMapById$ = this.tubes$.pipe(
            map(tubes => tubes.reduce((m, tube) => m.set(tube.id, tube), new Map<number, Tube>())),
            shareReplayLast()
        );

        this.tubeMapByName$ = this.tubes$.pipe(
            map(tubes => tubes.reduce((m, tube) => m.set(tube.label, tube), new Map<string, Tube>())),
            shareReplayLast()
        );

        this.tubeMapByNameLowerCase$ = this.tubes$.pipe(
            map(tubes => tubes.reduce((m, tube) => m.set(tube.label.trim().toLowerCase(), tube), new Map<string, Tube>())),
            shareReplayLast()
        );

        this.tubeTypes$ = httpClient.get<Array<TubeType>>('assets/tubes-types.json').pipe(
            map(types => {
                types.sort((t1, t2) => t1.label.localeCompare(t2.label));
                return types;
            }),
            shareReplayLast()
        );

        this.tubeTypeMap$ = this.tubeTypes$.pipe(
            map(types => types.reduce((m, type) => m.set(type.id, type), new Map<number, TubeType>())),
            shareReplayLast()
        );

        this.tubeBases$ = httpClient.get<Array<TubeBase>>('assets/tubes-bases.json').pipe(
            map(bases => {
                bases.sort((b1, b2) => b1.label.localeCompare(b2.label));
                return bases;
            }),
            shareReplayLast()
        );

        this.tubeBaseMap$ = this.tubeBases$.pipe(
            map(bases => bases.reduce((m, base) => m.set(base.id, base), new Map<number, TubeBase>())),
            shareReplayLast()
        );

        this.tubePinouts$ = httpClient.get<Array<TubePinout>>('assets/tubes-pinouts.json').pipe(
            map(pinouts => {
                pinouts.sort((p1, p2) => p1.label.localeCompare(p2.label));
                return pinouts;
            }),
            shareReplayLast()
        );

        this.tubePinoutMap$ = this.tubePinouts$.pipe(
            map(pinouts => pinouts.reduce((m, pinout) => m.set(pinout.id, pinout), new Map<number, TubePinout>())),
            shareReplayLast()
        );
    }

    public getTubeImageDocuments(tube: Tube): ReadonlyArray<Document> {
        return tube.documents.filter(doc => doc.filename.match(/.bmp$|.png$|.jpg$|jpeg$|.gif$|.tif$|tiff$/));
    }

    public hasTubeAttachments(tube: Tube): boolean {
        return tube.documents?.length > 0 || tube.usages?.length > 0;
    }

    public hasGraphAttachment(documents: ReadonlyArray<Document>): boolean {
        return documents?.some(doc => chartPattern.test(doc.filename));
    }

    public getTube$(tubeId: number, userId?: string): Observable<Tube> {
        return this.tubes$.pipe(
            take(1),
            map(tubes => tubes.find(tube => tube.id === tubeId && (!userId || userId === tube.userId)))
        );
    }
}
