import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatestWith, debounceTime, distinctUntilChanged, map, Observable, ReplaySubject } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { Tube } from '../models/tube.model';
import { TubePins } from '../models/tube-pins.model';
import { TubeUnit } from '../models/tube-unit.model';
import { TubesService } from './tubes.service';

@Injectable()
export class UnitService {
    public tube$ = new ReplaySubject<Tube>(1);
    public selectUnitIndex$ = new BehaviorSubject<number>(0);

    public unitCount$: Observable<number>;
    public selectedUnitIndex$: Observable<number>;
    public hasMultiUnits$: Observable<boolean>;
    public selectedUnit$: Observable<TubeUnit>;
    public symetricalUnits$: Observable<boolean>;
    public selectedTubeConfig$: Observable<TubePins>;

    public paMax$: Observable<number>;

    public constructor(
        tubeService: TubesService
    ) {

        const type$ = this.tube$.pipe(
            distinctUntilChanged(),
            combineLatestWith(tubeService.tubeTypeMap$),
            map(([tube, tubeTypeMap]) => tube.typeId && tubeTypeMap.get(tube.typeId)),
            shareReplayLast()
        );

        this.unitCount$ = type$.pipe(
            distinctUntilChanged(),
            map(type => type?.sym ? 1 : type?.config?.length || 0),
            shareReplayLast()
        );

        this.selectedUnitIndex$ = this.selectUnitIndex$.pipe(
            distinctUntilChanged(),
            combineLatestWith(this.unitCount$),
            map(([selectUnitIndex, unitCount]) => (selectUnitIndex && selectUnitIndex < unitCount && selectUnitIndex) || 0),
            shareReplayLast()
        );

        this.hasMultiUnits$ = this.unitCount$.pipe(
            distinctUntilChanged(),
            map(unitCount => unitCount > 1),
            shareReplayLast()
        );

        this.symetricalUnits$ = type$.pipe(
            distinctUntilChanged(),
            map(type => type?.sym),
            shareReplayLast()
        );

        this.selectedUnit$ = this.selectedUnitIndex$.pipe(
            combineLatestWith(this.tube$),
            debounceTime(10),
            distinctUntilChanged(),
            map(([selectedUnitIndex, tube]) => tube.units?.[selectedUnitIndex] || {} as TubeUnit),
            shareReplayLast()
        );

        this.selectedTubeConfig$ = type$.pipe(
            distinctUntilChanged(),
            combineLatestWith(this.selectedUnitIndex$),
            map(([type, selectedUnitIndex]) => type?.config?.[selectedUnitIndex]),
            shareReplayLast()
        );

        this.paMax$ = this.selectedUnit$.pipe(
            debounceTime(10),
            map(selectedUnit => selectedUnit?.pamax || undefined),
            shareReplayLast()
        );
    }
}
