import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { getDownloadURL, getMetadata, ref, Storage, uploadBytesResumable, UploadTask } from '@angular/fire/storage';
import { listAll, StorageReference } from 'firebase/storage';
import { combineLatestWith, finalize, from, map, Observable, of, switchMap, take, tap } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { IdService } from '../common/id.service';
import { ImageDialogService } from '../components/image-modal/image-dialog.service';
import { ImageModalData } from '../components/image-modal/image-modal.model';
import { PdfViewerDialogService } from '../components/pdf-viewer-dialog/pdf-viewer-dialog.service';
import { Tube } from '../models/tube.model';
import { TubeGraphFile } from '../models/tube-graph-file.model';
import { chartPattern, documentExtensionPattern, imageExtensionPattern } from './constants';
import { TubesService } from './tubes.service';

export interface UploadDocumentResult {
    progress: number;
    filename?: string;
    downloadUrl?: string;
}

interface PackageJson {
    name: string;
    version: string;
    author: string;
    license: string;
}

@Injectable({
    providedIn: 'root'
})
export class FileService {
    public appVersion$: Observable<string>;

    private fileCache = new Map<string, Observable<string>>();
    private graphCache = new Map<string, Observable<TubeGraphFile>>();

    public constructor(
        private storage: Storage,
        private httpClient: HttpClient,
        private imageDialogService: ImageDialogService,
        private pdfViewerDialogService: PdfViewerDialogService,
        private ngZone: NgZone,
        private tubesService: TubesService,
        private idService: IdService
    ) {

        this.appVersion$ = this.httpClient.get<PackageJson>('assets/package.json').pipe(
            map(json => json.version),
            shareReplayLast()
        );
    }

    public getDocumentUrl$(filename: string): Observable<string> {
        if (!this.fileCache.get(filename)) {
            const r = ref(this.storage, filename);
            this.fileCache.set(filename, from(getDownloadURL(r)).pipe(
                shareReplayLast()
            ));
        }
        return this.fileCache.get(filename);
    }

    public getGraphContent$(filename: string): Observable<TubeGraphFile> {
        if (!this.graphCache.get(filename)) {
            this.graphCache.set(filename, this.httpClient.get<TubeGraphFile>(filename).pipe(
                tap(json => {
                    console.log('getGraphContent', json);
                }),
                shareReplayLast()
            ));
        }
        return this.graphCache.get(filename);
    }

    public getDocumentIcon(filename: string): string {
        if (chartPattern.test(filename)) {
            return null;
        } else if (imageExtensionPattern.test(filename)) {
            return 'image';
        } else if (documentExtensionPattern.test(filename)) {
            return 'picture_as_pdf';
        } else {
            console.log('no icon found for document', filename);
            return 'insert_drive_file';
        }
    }

    public downloadDocument$(filename: string, title?: string, openInNewWindow = false): Observable<unknown> {
        // firebase storage
        return this.getDocumentUrl$(`docs/${filename}`).pipe(
            switchMap(documentUrl => {
                if (openInNewWindow) {
                    // open with browser
                    return of(this.openDocument(documentUrl));
                } else if (documentExtensionPattern.test(filename)) {
                    // Open with pdf viewer
                    return this.pdfViewerDialogService.open$({
                        filename: documentUrl
                    });
                } else {
                    return this.imageDialogService.open$({
                        filename: documentUrl,
                        title
                    } as ImageModalData);
                }
            })
        );

    }

    public openDocument(fileUrl: string): void {
        window.open(fileUrl, '_blank');
    }

    public uploadDocument$(filename: string, data: Blob): Observable<UploadDocumentResult> {
        let task = undefined as UploadTask;

        return this.ngZone.runOutsideAngular<Observable<UploadDocumentResult>>(() => new Observable<UploadDocumentResult>(subscriber => {
            const uploadFile = (validFileName: string, storageRef: StorageReference): void => {
                task = uploadBytesResumable(storageRef, data);

                this.ngZone.run(() => {
                    subscriber.next({
                        progress: 1
                    });
                });

                task.on('state_changed', snapshot => {
                    const progress = Math.round(
                        (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    );
                    console.log('uploadDocument progress', progress);
                    this.ngZone.run(() => {
                        subscriber.next({
                            progress
                        });
                    });

                }, (error: unknown) => {
                    console.log('uploadDocument error', error);
                    subscriber.error(error);

                }, () => {
                    // complete function
                    void getDownloadURL(storageRef).then(downloadUrl => {
                        console.log('uploadDocument complete', downloadUrl);
                        this.ngZone.run(() => {
                            subscriber.next({
                                progress: 100,
                                filename: validFileName,
                                downloadUrl
                            });

                            task = undefined;
                            subscriber.complete();
                        });
                    });
                });
            };

            let fileIndex = 0;
            const checkFileName = (fileNameToCheck: string): void => {
                const storageRef = ref(this.storage, fileNameToCheck);
                void getMetadata(storageRef).then(() => {
                    // File already existsSync, change name
                    checkFileName(`${name}_${++fileIndex}.${ext}`);
                }).catch(() => {
                    // File don't exists
                    uploadFile(fileNameToCheck, storageRef);
                });
            };

            const fileSegments = filename.split('.');
            let name = filename;
            let ext = '';
            if (fileSegments.length) {
                ext = fileSegments[fileSegments.length - 1];
                name = fileSegments.slice(0, fileSegments.length - 1).join('.');
            }

            this.ngZone.run(() => {
                subscriber.next({
                    progress: 0
                });
            });

            return checkFileName(`${name}.${ext}`);
        }).pipe(
            finalize(() => {
                // eslint-disable-next-line @typescript-eslint/no-misused-promises
                if (task) {
                    console.log('uploadDocument canceled');
                    task.cancel();
                }
            })
        ));
    }

    public downloadDatabaseFile(obj: unknown, name: string): void {
        const downloadTextFile = (text: string): void => {
            const a = document.createElement('a');
            const type = name.split('.').pop();
            a.href = URL.createObjectURL(new Blob([text], { type: `text/${type === 'txt' ? 'plain' : type}` }));
            a.download = name;
            a.click();
        };

        downloadTextFile(JSON.stringify(obj));
    }

    public printUnusedDocuments$(): Observable<unknown> {
        console.log('Print orphan document');

        return this.tubesService.userTubes$.pipe(
            combineLatestWith(this.tubesService.globalTubes$),
            take(1),
            switchMap(([userTubes, globalTubes]) => {
                console.log(`${userTubes.length + globalTubes.length} tubes found`);

                const documentMap = globalTubes.reduce((mmap, tube) => {
                    tube.documents?.reduce((m, d) => {
                        m.set(d.filename, tube);
                        m.set(d.filename.toLowerCase(), tube);
                        return m;
                    }, mmap);
                    return mmap;
                }, new Map<string, Tube>());

                userTubes.reduce((mmap, tube) => {
                    tube.documents?.reduce((m, d) => {
                        m.set(d.filename, tube);
                        m.set(d.filename.toLowerCase(), tube);
                        return m;
                    }, mmap);
                    return mmap;
                }, documentMap);

                console.log(`${documentMap.size} used documents found`);

                const storageRef = ref(this.storage, 'docs');
                return from(listAll(storageRef)).pipe(
                    tap(list => {
                        const result = (list.items || []).reduce((res, fileRef) => {
                            if (documentMap.has(fileRef.name)) {
                                // continue
                                return res;
                            } else if (documentMap.has(fileRef.name.toLowerCase())) {
                                console.warn(`Warning: ${fileRef.name} as a wrong case in tube with id ${documentMap.get(fileRef.name.toLowerCase()).id}`);
                                return res;
                            } else {
                                // Orphelin document
                                res.push(fileRef.name);
                            }
                            return res;
                        }, new Array<string>());

                        if (result.length === 0) {
                            console.log('Operation finished no orphan documents');
                        } else {
                            console.log(`Operation finished ${result.length} document found`);
                            console.log('******************* Begin List *******************');
                            result.forEach(fileName => {
                                console.log(`"${fileName}";`);
                            });
                            console.log('******************* End of List *******************');
                        }
                    })
                );
            })
        );
    }

    public fixUids$(): Observable<void> {
        console.log('Fix duplicate uids');

        return this.tubesService.globalTubes$.pipe(
            take(1),
            map(globalTubes => {
                console.log(`${globalTubes.length} tubes found`);
                let updated = false;

                globalTubes.forEach(tube => {
                    tube.documents?.reduce((s, d) => {
                        if (s.has(d.id)) {
                            console.log(`Duplicate uid found in document for tube ${tube.label}`);
                            d.id = this.idService.generate();
                            updated = true;
                        }
                        s.add(d.id);
                        return s;
                    }, new Set<string>());

                    tube.usages?.reduce((s, u) => {
                        if (s.has(u.id)) {
                            console.log(`Duplicate uid found in usage for tube ${tube.label}`);
                            u.id = this.idService.generate();
                            updated = true;
                        }
                        s.add(u.id);
                        return s;
                    }, new Set<string>());
                });

                if (updated) {
                    // Download database
                    this.downloadDatabaseFile(globalTubes, 'tubes.json');
                } else {
                    console.log('Operation finished no duplicate ids');
                }
            })
        );
    }
}
