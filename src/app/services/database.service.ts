import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { collection, deleteDoc, doc, DocumentData, getDocs, Query, query, where } from 'firebase/firestore';
import { catchError, combineLatestWith, from, map, Observable, of, startWith, switchMap, take, throwError, withLatestFrom } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { Tube } from '../models/tube.model';
import { TubeGraphFile } from '../models/tube-graph-file.model';
import { AuthService } from './auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {
    public userTubes$: Observable<ReadonlyArray<Tube>>;
    public allUserTube$: Observable<ReadonlyArray<Tube>>;

    private isAdminSafe$: Observable<boolean>;
    private graphCache = new Map<string, Observable<TubeGraphFile>>();

    public constructor(
        private fireStore: Firestore,
        private authService: AuthService
    ) {
        this.isAdminSafe$ = authService.isAdmin$.pipe(
            startWith(false),
            shareReplayLast()
        );

        // Normalize old model to new
        const normalizeUserTube = (tube: Tube): Tube => {
            tube.documents.forEach(d => d.label ||= (d as unknown as { description: string }).description);
            return tube;
        };

        const getTubes$ = (q: Query<DocumentData>): Observable<ReadonlyArray<Tube>> => from(getDocs(q)).pipe(
            map(docs => {
                const tubes = docs.docs.map(d => d.data() as Tube);
                // console.log('userTubes', tubes);
                return tubes;
            })
        );

        this.userTubes$ = authService.authenticatedUser$.pipe(
            switchMap(user => {
                if (user) {
                    // We don't fail here in case of firebase loose dayly credits
                    const q1 = query(collection(fireStore, 'tubes'), where('userId', '==', user.uid));
                    const q2 = query(collection(fireStore, 'tubes'), where('userId', '==', user.email));
                    return getTubes$(q1).pipe(
                        combineLatestWith(getTubes$(q2)),
                        map(([r1, r2]) => [...r1, ...r2]),
                        catchError(err => {
                            console.error('Fail to load user database', err);
                            return of(new Array<Tube>());
                        })
                    );
                } else {
                    return of(new Array<Tube>());
                }
            }),
            map(tubes => tubes.map(t => normalizeUserTube(t))),
            shareReplayLast()
        );

        this.allUserTube$ = of(undefined).pipe(
            switchMap(() => getTubes$(query(collection(fireStore, 'tubes')))),
            map(tubes => tubes.map(t => normalizeUserTube(t))),
            shareReplayLast()
        );
    }

    public deleteUserTube$(tube: Tube): Observable<void> {
        return this.authService.authenticatedUser$.pipe(
            take(1),
            withLatestFrom(this.isAdminSafe$),
            switchMap(([user, isAdmin]) => {
                if (!user) {
                    return throwError(() => new Error('Not authenticated.'));
                }

                const loggedUserId = user.email || user.uid;
                const userId = tube.userId;
                if (!userId) {
                    return throwError(() => new Error('Only user tubes can be deleted.'));
                } else if (userId !== loggedUserId && !isAdmin) {
                    return throwError(() => new Error('You can delete only your own tubes.'));
                }

                const docRef = doc(this.fireStore, `tubes/${userId}#${tube.id}`);
                return from(deleteDoc(docRef));
            })
        );
    }

    public getChart$(userId: string, filename: string): Observable<TubeGraphFile> {
        return this.authService.authenticatedUser$.pipe(
            take(1),
            switchMap(user => {
                userId ||= user.email || user.uid;

                if (!userId) {
                    return throwError(() => new Error('Not authenticated.'));
                }

                const cacheKey = `${userId}#${filename}`;
                if (!this.graphCache.get(cacheKey)) {
                    const q = query(collection(this.fireStore, 'charts'), where('userId', '==', userId), where('filename', '==', filename));
                    const getDocs$ = from(getDocs(q)).pipe(
                        switchMap(docs => {
                            if (docs.size > 0) {
                                if (docs.size > 1) {
                                    console.error('Error in database for user', userId, 'More than one chart found for filename', filename);
                                }
                                return of(docs.docs[0].data() as TubeGraphFile);
                            }

                            return throwError(() => new Error('Document not found.'));
                        }),
                        shareReplayLast()
                    );

                    this.graphCache.set(cacheKey, getDocs$);
                }

                return this.graphCache.get(cacheKey);
            })
        );
    }
}
