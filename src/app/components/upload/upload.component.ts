import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { catchError, filter, Observable, of, ReplaySubject, Subject, switchMap, takeUntil, tap } from 'rxjs';

import { FileService, UploadDocumentResult } from '../../services/file.service';
import { UploadData } from './upload.model';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadComponent {
    @Output()
    public readonly uploadDone = new EventEmitter<string>();

    @Input()
    public set uploadData(value: UploadData) {
        this.uploadData$.next(value);
    }

    public result$: Observable<UploadDocumentResult>;
    public cancel$ = new Subject<void>();
    public errorMessage: string;

    private uploadData$ = new ReplaySubject<UploadData>(1);

    public constructor(
        fileService: FileService,
        changeDetectorRef: ChangeDetectorRef
    ) {
        this.result$ = this.uploadData$.pipe(
            filter(Boolean),
            switchMap(uploadData => fileService.uploadDocument$(uploadData.filename, uploadData.data)),
            tap(result => {
                console.log('UploadComponent progress', result?.progress);

                if (result?.downloadUrl) {
                    console.log('UploadComponent close', result.filename);
                    this.uploadDone.emit(result.filename);
                }
            }),
            catchError((err: Error) => {
                this.errorMessage = err.message;
                changeDetectorRef.markForCheck();
                return of(undefined as UploadDocumentResult);
            }),
            takeUntil(this.cancel$)
        );
    }
}
