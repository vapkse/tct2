export interface UploadData {
    filename: string;
    data: Blob;
}
