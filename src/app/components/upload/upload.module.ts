import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { TranslationModule } from '../../translation/translation.module';
import { UploadComponent } from './upload.component';

@NgModule({
    declarations: [UploadComponent],
    exports: [UploadComponent],
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        TranslationModule
    ]
})
export class UploadModule { }
