import { ChangeDetectionStrategy, Component, Inject, Optional, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AttachmentsDialogdata } from './attachments-dialog.service';


@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-attachments-dialog',
    templateUrl: './attachments-dialog.component.html',
    styleUrls: ['./attachments-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AttachmentsDialogComponent {
    public constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public data: AttachmentsDialogdata
    ) { }
}
