import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';
import { Tube } from '../../models/tube.model';

export interface AttachmentsDialogdata {
    tube: Tube;
    documentFilter: RegExp;
    selectedId?: string;
}

@Injectable({
    providedIn: 'root'
})
export class AttachmentsDialogService extends DialogService<string, AttachmentsDialogdata> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {

        const dialogConfig = new DialogConfig<AttachmentsDialogdata>('800px');
        dialogConfig.panelClass = ['no-padding-dialog', 'attachments'];

        super(lazyLoaderService, dialog, dialogConfig);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./attachments-dialog.module').then(m => m.AttachmentsDialogModule);
    }
}
