import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from '../../common/lazy-loading/lazy-loader.service';
import { ThrottleEventModule } from '../../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../../translation/translation.module';
import { AttachmentsModule } from '../attachments/attachments.module';
import { AttachmentsDialogComponent } from './attachments-dialog.component';

@NgModule({
    imports: [
        AttachmentsModule,
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        ThrottleEventModule,
        TranslationModule
    ],
    entryComponents: [AttachmentsDialogComponent],
    exports: [AttachmentsDialogComponent],
    declarations: [AttachmentsDialogComponent]
})
export class AttachmentsDialogModule extends AbstractLazyModule<AttachmentsDialogComponent> {
    public constructor() {
        super(AttachmentsDialogComponent);
    }
}
