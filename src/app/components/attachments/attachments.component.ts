import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { combineLatestWith, filter, map, Observable, ReplaySubject, Subject, switchMap, tap, throttleTime, withLatestFrom } from 'rxjs';

import { shareReplayLast, subscribeWith } from '../../common/custom-operators';
import { Document } from '../../models/document.model';
import { Tube } from '../../models/tube.model';
import { TubeUsage } from '../../models/tube-usage.model';
import { FileService } from '../../services/file.service';
import { DownloadDocumentData, OpenService, UsageData } from '../../services/open.service';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-attachments',
    templateUrl: './attachments.component.html',
    styleUrls: ['./attachments.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AttachmentsComponent {
    @Output()
    public readonly selectedIdChange = new EventEmitter<string>();

    @Input()
    public selectedId: string;

    @Input()
    public set documentFilter(docFilter: RegExp) {
        this.documentFilter$.next(docFilter);
    }

    @Input()
    public set tube(tube: Tube) {
        this.tube$.next(tube);
    }

    public documentFilter$ = new ReplaySubject<RegExp>(1);
    public tube$ = new ReplaySubject<Tube>(1);
    public downloadDocument$ = new Subject<DownloadDocumentData>();
    public openUsage$ = new Subject<UsageData>();

    public documents$: Observable<ReadonlyArray<Document>>;
    public usages$: Observable<ReadonlyArray<TubeUsage>>;

    public constructor(
        public fileService: FileService,
        matDialogRef: MatDialogRef<AttachmentsComponent>,
        openService: OpenService
    ) {
        const downloadDocument$ = this.downloadDocument$.pipe(
            throttleTime(1000),
            withLatestFrom(this.tube$),
            switchMap(([data, tube]) => {
                data.userId = tube.userId;
                return openService.downloadDocument$(data);
            })
        );

        const openUsage$ = this.openUsage$.pipe(
            throttleTime(1000),
            withLatestFrom(this.tube$),
            switchMap(([data, tube]) => openService.openUsage$(tube, data).pipe(
                filter(() => !data.originalEvent.ctrlKey),
                tap(() => matDialogRef.close())
            ))
        );

        this.documents$ = this.tube$.pipe(
            combineLatestWith(this.documentFilter$),
            map(([tube, documentFilter]) => !documentFilter ? tube.documents : tube.documents?.filter(doc => documentFilter.test(doc.filename))),
            subscribeWith(downloadDocument$, openUsage$),
            shareReplayLast()
        );

        this.usages$ = this.documentFilter$.pipe(
            combineLatestWith(this.tube$),
            map(([documentFilter, tube]) => !documentFilter ? tube.usages : undefined)
        );
    }
}
