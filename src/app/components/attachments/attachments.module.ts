import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';

import { ThrottleEventModule } from '../../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../../translation/translation.module';
import { AttachmentsComponent } from './attachments.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatIconModule,
        MatRadioModule,
        ThrottleEventModule,
        TranslationModule
    ],
    exports: [AttachmentsComponent],
    declarations: [AttachmentsComponent]
})
export class AttachmentsModule {}
