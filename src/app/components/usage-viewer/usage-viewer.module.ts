import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RxIfModule } from '../../common/rx-if/rx-if.module';
import { TranslationModule } from '../../translation/translation.module';
import { TransferGraphModule } from '../transfer-graph/transfer-graph.module';
import { UsageViewerComponent } from './usage-viewer.component';

@NgModule({
    imports: [
        CommonModule,
        RxIfModule,
        TransferGraphModule,
        TranslationModule
    ],
    exports: [
        UsageViewerComponent
    ],
    declarations: [
        UsageViewerComponent
    ]
})
export class UsageViewerModule {
}
