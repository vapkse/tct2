import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { round } from 'lodash-es';
import { combineLatestWith, map, Observable, ReplaySubject, switchMap, withLatestFrom } from 'rxjs';

import { loading$ } from 'src/app/common/rx-if/loading-context';
import { Document } from 'src/app/models/document.model';

import { filterMap, shareReplayLast } from '../../common/custom-operators';
import { LoadingContext } from '../../common/rx-if/loading-context';
import { Tube } from '../../models/tube.model';
import { TubeGraph } from '../../models/tube-graph.model';
import { TubeGraphFile } from '../../models/tube-graph-file.model';
import { TubeUsage } from '../../models/tube-usage.model';
import { databaseChartPattern } from '../../services/constants';
import { DatabaseService } from '../../services/database.service';
import { FileService } from '../../services/file.service';
import { UnitService } from '../../services/unit.service';

@Component({
    selector: 'app-usage-viewer',
    templateUrl: './usage-viewer.component.html',
    styleUrls: ['./usage-viewer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        UnitService
    ]
})
export class UsageViewerComponent {
    @Input()
    public set tube(value: Tube) {
        if (value) {
            this.unitService.tube$.next(value);
            this.tube$.next(value);
        }
    }

    @Input()
    public set usage(value: TubeUsage) {
        if (value) {
            this.unitService.selectUnitIndex$.next(value.unit);
            this.usage$.next(value);
        }
    }

    @Input()
    public set document(document: Document) {
        if (document) {
            this.document$.next(document);
        }
    }

    public graphLoader$: Observable<LoadingContext<TubeGraph>>;

    public tube$ = new ReplaySubject<Tube>(1);
    public usage$ = new ReplaySubject<TubeUsage>(1);
    public document$ = new ReplaySubject<Document>(1);

    public round = round;

    public constructor(
        public unitService: UnitService,
        fileService: FileService,
        databaseService: DatabaseService
    ) {

        this.graphLoader$ = this.usage$.pipe(
            combineLatestWith(this.document$),
            filterMap(([usage, document]) => (usage.traces && document) || undefined),
            withLatestFrom(this.tube$),
            switchMap(([document, tube]) => {
                let graph$: Observable<TubeGraphFile>;
                if (databaseChartPattern.test(document.filename)) {
                    graph$ = databaseService.getChart$(tube.userId, document.filename);
                } else {
                    graph$ = fileService.getGraphContent$(`assets/usages/${document.filename}`);
                }

                return loading$(graph$).pipe(
                    map(loader => {
                        if (loader.data) {
                            return new LoadingContext(false, loader.data.tubeGraph);
                        }
                        return new LoadingContext(loader.loading, undefined as TubeGraph, loader.error);
                    })
                );
            }),
            shareReplayLast()
        );
    }
}
