import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from 'src/app/common/lazy-loading/lazy-loader.service';

import { RxIfModule } from '../../common/rx-if/rx-if.module';
import { TranslationModule } from '../../translation/translation.module';
import { TransferGraphModule } from '../transfer-graph/transfer-graph.module';
import { TransferGraphDialogComponent } from './transfer-graph-dialog.component';

@NgModule({
    declarations: [TransferGraphDialogComponent],
    exports: [TransferGraphDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        RxIfModule,
        TransferGraphModule,
        TranslationModule
    ]
})
export class TransferGraphDialogModule extends AbstractLazyModule<TransferGraphDialogComponent> {
    public constructor() {
        super(TransferGraphDialogComponent);
    }
}
