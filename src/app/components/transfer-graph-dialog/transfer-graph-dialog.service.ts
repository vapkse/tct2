import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';

export interface TransferGraphData {
    filename: string;
    userId: string;
    paMax?: number;
}

@Injectable({
    providedIn: 'root'
})
export class TransferGraphDialogService extends DialogService<unknown, TransferGraphData> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        super(lazyLoaderService, dialog, new DialogConfig<TransferGraphData>('1024px', '768px'));
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./transfer-graph-dialog.module').then(m => m.TransferGraphDialogModule);
    }
}
