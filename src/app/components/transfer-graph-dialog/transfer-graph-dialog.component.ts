import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { map, Observable } from 'rxjs';

import { loading$, LoadingContext } from 'src/app/common/rx-if/loading-context';

import { shareReplayLast } from '../../common/custom-operators';
import { TubeGraph } from '../../models/tube-graph.model';
import { TubeGraphFile } from '../../models/tube-graph-file.model';
import { databaseChartPattern } from '../../services/constants';
import { DatabaseService } from '../../services/database.service';
import { FileService } from '../../services/file.service';
import { TransferGraphData } from './transfer-graph-dialog.service';

@Component({
    selector: 'app-transfer-graph-dialog',
    templateUrl: './transfer-graph-dialog.component.html',
    styleUrls: ['./transfer-graph-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TransferGraphDialogComponent {
    public graphLoader$: Observable<LoadingContext<TubeGraph>>;

    public constructor(
        @Inject(MAT_DIALOG_DATA) public data: TransferGraphData,
        fileService: FileService,
        databaseService: DatabaseService
    ) {

        let graph$: Observable<TubeGraphFile>;
        if (databaseChartPattern.test(data.filename)) {
            graph$ = databaseService.getChart$(data.userId, data.filename);
        } else {
            graph$ = fileService.getGraphContent$(`assets/usages/${data.filename}`);
        }

        this.graphLoader$ = loading$(graph$).pipe(
            map(loader => {
                if (loader.data) {
                    return new LoadingContext(false, loader.data.tubeGraph);
                }
                return new LoadingContext(loader.loading, undefined as TubeGraph, loader.error);
            }),
            shareReplayLast()
        );
    }
}
