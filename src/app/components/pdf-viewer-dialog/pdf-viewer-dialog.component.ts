import { ChangeDetectionStrategy, Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { PdfViewerData } from './pdf-viewer-dialog.service';

@Component({
    selector: 'app-pdf-viewer-dialog',
    templateUrl: './pdf-viewer-dialog.component.html',
    styleUrls: ['./pdf-viewer-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class PdfViewerDialogComponent {
    public showCloseButton = true;

    public constructor(
        @Inject(MAT_DIALOG_DATA) public data: PdfViewerData,
        public dialogRef: MatDialogRef<PdfViewerDialogComponent>
    ) { }
}
