import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from 'src/app/common/lazy-loading/lazy-loader.service';

import { RxIfModule } from '../../common/rx-if/rx-if.module';
import { TranslationModule } from '../../translation/translation.module';
import { PdfViewerModule } from '../pdf-viewer/pdf-viewer.module';
import { PdfViewerDialogComponent } from './pdf-viewer-dialog.component';

@NgModule({
    declarations: [PdfViewerDialogComponent],
    exports: [PdfViewerDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        PdfViewerModule,
        RxIfModule,
        TranslationModule
    ]
})
export class PdfViewerDialogModule extends AbstractLazyModule<PdfViewerDialogComponent> {
    public constructor() {
        super(PdfViewerDialogComponent);
    }
}
