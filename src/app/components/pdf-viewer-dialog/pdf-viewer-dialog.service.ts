import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';

export interface PdfViewerData {
    filename: string;
}

@Injectable({
    providedIn: 'root'
})
export class PdfViewerDialogService extends DialogService<unknown, PdfViewerData> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        const config = new DialogConfig<PdfViewerData>('95vw', '95vh');
        super(lazyLoaderService, dialog, config);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./pdf-viewer-dialog.module').then(m => m.PdfViewerDialogModule);
    }
}
