import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, Output } from '@angular/core';
import { combineLatestWith, delay, filter, from, map, Observable, ReplaySubject, Subject, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs';

import WebViewer, { WebViewerInstance, WVOptions } from 'src/assets/lib/pdfjs-express-viewer';

import { shareReplayLast, subscribeWith } from '../../common/custom-operators';
import { DestroyDirective } from '../../common/destroy/destroy.directive';
import { ThemeService } from '../../services/theme.service';
import { TranslationService } from '../../translation/translation.service';

export const supportedLocales = ['fr', 'de', 'en', 'es', 'it', 'ja', 'ko', 'nl', 'br', 'ru', 'cn', 'tw'];

export interface CurrentPageImage {
    currentPage: number;
    dataUrl: string;
}

@Component({
    selector: 'app-pdf-viewer',
    template: '',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PdfViewerComponent extends DestroyDirective {
    @Output()
    public readonly closeClicked = new EventEmitter<void>();

    @Output()
    public readonly documentLoaded = new EventEmitter<void>();

    @Input()
    public set filename(value: string) {
        this.filename$.next(value);
    }

    @Input()
    public set isViewer(value: boolean) {
        this.isViewer$.next(value);
    }

    private filename$ = new Subject<string>();
    private isViewer$ = new ReplaySubject<boolean>(1);
    private viewerInstance$: Observable<WebViewerInstance>;

    public constructor(
        elementRef: ElementRef<HTMLElement>,
        themeService: ThemeService,
        translationService: TranslationService,
        ngZone: NgZone
    ) {
        super();
        // this.wvDocumentLoadedHandler = this.wvDocumentLoadedHandler.bind(this);

        this.viewerInstance$ = this.filename$.pipe(
            combineLatestWith(this.isViewer$),
            filter(Boolean),
            take(1), // We need to destroy first instance properly if we want to change the filename
            switchMap(([filename, isViewer]) => {
                const options = {
                    path: 'assets/lib/pdfjs-express-viewer/public',
                    initialDoc: filename,
                    licenseKey: 'qAMaO9wJbmMYzL3bAtZR',
                    css: 'assets/lib/pdfjs-express-viewer/pdf-viewer.css',
                    disabledElements: [
                        'themeChangeButton',
                        'thumbnailsPanelButton',
                        'contextMenuPopup',
                        'selectToolButton',
                        'panToolButton',
                        'searchButton'
                    ]
                } as WVOptions;

                if (!isViewer) {
                    options.disabledElements.push([
                        'menuButton',
                        'layoutHeader',
                        'singleLayoutButton',
                        'doubleLayoutButton',
                        'coverLayoutButton'
                    ]);
                }
                return from(WebViewer(options, elementRef.nativeElement));
            }),
            delay(0),
            shareReplayLast()
        );

        const themeViewer$ = this.viewerInstance$.pipe(
            combineLatestWith(themeService.isDark$),
            tap(([instance, isDark]) => instance.setTheme(isDark ? 'dark' : 'light'))
        );

        const setLanguage$ = this.viewerInstance$.pipe(
            combineLatestWith(translationService.currentLocale$),
            tap(([instance, locale]) => {
                try {
                    if (supportedLocales.includes(locale) && locale !== 'en') {
                        instance.UI.setLanguage(locale);
                    }
                } catch (err: unknown) {
                    console.warn('Warning PdfViewerComponent setLanguage', err);
                }
            })
        );

        this.viewerInstance$.pipe(
            withLatestFrom(this.isViewer$),
            delay(3000),
            subscribeWith(themeViewer$, setLanguage$),
            takeUntil(this.destroyed$)
        ).subscribe(([viewerInstance, isViewer]) => {
            viewerInstance.UI.setHeaderItems(header => {

                ngZone.run(() => {
                    this.documentLoaded.emit();
                });

                if (isViewer) {
                    const saveButton = {
                        type: 'actionButton',
                        img: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z"/></svg>',
                        onClick: (): void => {
                            const currentPageImage = this.saveCurrentPage(viewerInstance);
                            const a = document.createElement('a');
                            a.href = currentPageImage.dataUrl;
                            a.download = `page${currentPageImage.currentPage}.jpeg`;
                            document.body.appendChild(a);
                            a.click();
                        }
                    };
                    header.push(saveButton);
                }

                if (this.closeClicked.observed) {
                    const closeButton = {
                        type: 'actionButton',
                        img: '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"/></svg>',
                        onClick: (): void => {
                            ngZone.run(() => {
                                this.closeClicked.emit();
                            });
                        }
                    };
                    header.push(closeButton);
                }
            });
        });
    }

    public saveCurrentPage$(): Observable<CurrentPageImage> {
        return this.viewerInstance$.pipe(
            map(viewerInstance => this.saveCurrentPage(viewerInstance))
        );
    }

    private saveCurrentPage(viewerInstance: WebViewerInstance): CurrentPageImage {
        const ui = viewerInstance.UI;
        const currentPage = ui.getCurrentPageNumber();
        const pageContainer = ui.iframeWindow.document.getElementById(`pageContainer${currentPage}`);
        const canvas = pageContainer?.getElementsByClassName('hacc')[0] as HTMLCanvasElement;
        return {
            currentPage,
            dataUrl: canvas?.toDataURL('image/jpeg', 1.0)
        } as CurrentPageImage;
    }
}
