import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from 'src/app/common/lazy-loading/lazy-loader.service';

import { TranslationModule } from '../../translation/translation.module';
import { ImageModalComponent } from './image-modal.component';

@NgModule({
    declarations: [ImageModalComponent],
    exports: [ImageModalComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        TranslationModule
    ]
})
export class ImageModalModule extends AbstractLazyModule<ImageModalComponent> {
    public constructor() {
        super(ImageModalComponent);
    }
}
