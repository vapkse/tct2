import { ChangeDetectionStrategy, Component, ElementRef, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TooltipComponent } from 'src/app/common/tooltip/tooltip.component';

import { ImageModalData } from './image-modal.model';

@Component({
    selector: 'image-modal',
    templateUrl: './image-modal.component.html',
    styleUrls: ['./image-modal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageModalComponent extends TooltipComponent {
    public hasError = false;
    public waiter = true;

    public constructor(
        public elementRef: ElementRef<HTMLElement>,
        @Inject(MAT_DIALOG_DATA) public data: ImageModalData
    ) {
        super(elementRef);
    }
}
