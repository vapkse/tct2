import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';
import { ImageModalData } from './image-modal.model';

@Injectable({
    providedIn: 'root'
})
export class ImageDialogService extends DialogService<unknown, ImageModalData> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        const dialogConfig = new DialogConfig<ImageModalData>('1024px');

        super(lazyLoaderService, dialog, dialogConfig);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./image-modal.module').then(m => m.ImageModalModule);
    }
}
