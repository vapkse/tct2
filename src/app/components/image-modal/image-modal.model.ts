export interface ImageModalData {
    filename: string;
    title: string;
}
