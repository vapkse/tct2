import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';
import { TooltipComponent } from 'src/app/common/tooltip/tooltip.component';
import { TooltipConfig } from 'src/app/common/tooltip/tooltip.model';
import { TooltipService } from 'src/app/common/tooltip/tooltip.service';

import { ImageModalData } from './image-modal.model';

@Injectable({
    providedIn: 'root'
})
export class ImageTooltipService extends TooltipService<ImageModalData> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        const toolTipConfig = new TooltipConfig<ImageModalData>('400px', '430px');
        toolTipConfig.minWidth = '400px';

        super(lazyLoaderService, dialog, toolTipConfig);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<TooltipComponent>>> {
        return import('./image-modal.module').then(m => m.ImageModalModule);
    }
}
