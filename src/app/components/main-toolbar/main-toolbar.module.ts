import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';

import { ThrottleEventModule } from '../../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../../translation/translation.module';
import { MainToolbarComponent } from './main-toolbar.component';

@NgModule({
    declarations: [MainToolbarComponent],
    exports: [MainToolbarComponent],
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
        ThrottleEventModule,
        TranslationModule
    ]
})
export class MainToolbarModule {}
