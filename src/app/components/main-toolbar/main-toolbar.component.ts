import { ChangeDetectionStrategy, Component, Input, Optional, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { BehaviorSubject, map, Observable, Subject, take, takeUntil, throttleTime } from 'rxjs';

import { asyncMap, asyncSwitchMap } from '../../common/custom-operators';
import { DestroyDirective } from '../../common/destroy/destroy.directive';
import { AuthService } from '../../services/auth/auth.service';
import { CompareService } from '../../services/compare.service';
import { FileService } from '../../services/file.service';
import { OpenService } from '../../services/open.service';
import { SaveService } from '../../services/save.service';
import { ThemeService } from '../../services/theme.service';
import { UndoRedoService } from '../../services/undo-redo.service';
import { Locales, TranslationService } from '../../translation/translation.service';


@Component({
    selector: 'app-main-toolbar',
    templateUrl: './main-toolbar.component.html',
    styleUrls: ['./main-toolbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class MainToolbarComponent extends DestroyDirective {
    @Input()
    public comparatorEnabled = false;

    @ViewChild(MatMenuTrigger)
    private menuTrigger: MatMenuTrigger;

    public loadAvatarError$ = new BehaviorSubject<boolean>(false);

    public goHome$ = new Subject<MouseEvent>();

    public constructor(
        public themeService: ThemeService,
        public authService: AuthService,
        public translationService: TranslationService,
        public fileService: FileService,
        @Optional() public undoRedoService: UndoRedoService<unknown>,
        @Optional() public saveService: SaveService,
        @Optional() public compareService: CompareService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        openService: OpenService
    ) {
        super();

        this.goHome$.pipe(
            throttleTime(1000),
            map(event => openService.navigateTo(['home'], event.ctrlKey)),
            takeUntil(this.destroyed$)
        ).subscribe();
    }

    public logout$(): Observable<unknown> {
        return asyncSwitchMap(() => this.authService.signOut$().pipe(
            take(1)
        ));
    }

    public signIn(): void {
        const extras = {
            queryParams: { navto: `${location.pathname}${location.search}` },
            skipLocationChange: true
        } as NavigationExtras;
        void this.router.navigate(['/sign-in'], extras);
    }

    public undo$(): Observable<unknown> {
        return asyncSwitchMap(() => this.undoRedoService.undo$());
    }

    public redo$(): Observable<unknown> {
        return asyncSwitchMap(() => this.undoRedoService.redo$());
    }

    public save(): void {
        this.saveService.save$.next();
    }

    public updateLang$(lang: Locales): Observable<unknown> {
        return asyncMap(() => {
            this.translationService.locale$.next(lang);

            const extras = {
                queryParams: { lang },
                queryParamsHandling: 'merge',
                relativeTo: this.activatedRoute
            } as NavigationExtras;

            // Force to reload current route
            const urlTree = this.router.createUrlTree([], extras);
            return this.router.navigateByUrl('/waiter', { skipLocationChange: true }).then(() =>
                this.router.navigateByUrl(urlTree)
            );
        });
    }

    public printUnusedDocuments$(): Observable<unknown> {
        return asyncSwitchMap(() => {
            this.menuTrigger?.closeMenu();
            return this.fileService.printUnusedDocuments$();
        });
    }

    public fixUids$(): Observable<unknown> {
        return asyncSwitchMap(() => {
            this.menuTrigger?.closeMenu();
            return this.fileService.fixUids$();
        });
    }
}
