import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';
import { Tube } from '../../models/tube.model';
import { TubeGraph } from '../../models/tube-graph.model';

export interface ChartWizardDialogdata {
    tube: Tube;
}

@Injectable({
    providedIn: 'root'
})
export class ChartWizardDialogService extends DialogService<TubeGraph, ChartWizardDialogdata> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {

        const dialogConfig = new DialogConfig<ChartWizardDialogdata>('95vw', '95vw');
        dialogConfig.panelClass = ['no-padding-dialog'];
        dialogConfig.disableClose = true;

        super(lazyLoaderService, dialog, dialogConfig);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./chart-wizard-dialog.module').then(m => m.ChartWizardDialogModule);
    }
}
