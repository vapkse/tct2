import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, Optional, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { cloneDeep } from 'lodash-es';
import { BehaviorSubject, catchError, combineLatestWith, debounceTime, distinctUntilChanged, filter, fromEvent, map, mergeWith, Observable, of, ReplaySubject, startWith, Subject, switchMap, take, takeUntil, tap, throttleTime, withLatestFrom } from 'rxjs';

import { Document } from 'src/app/models/document.model';

import { asyncMap, asyncSwitchMap, filterMap, shareReplayLast, subscribeWith } from '../../common/custom-operators';
import { DestroyDirective } from '../../common/destroy/destroy.directive';
import { IdService } from '../../common/id.service';
import { InputAutoSizeService } from '../../common/input-autosize/input-autosize.service';
import { InputDialogData } from '../../common/input-dialog/input-dialog.model';
import { InputDialogService } from '../../common/input-dialog/input-dialog.service';
import { MessageBoxDialogService } from '../../common/message-box-dialog/message-box-dialog.service';
import { StatusService } from '../../common/status/status.service';
import { Tube } from '../../models/tube.model';
import { TubeGraph, TubeGraphCurve, TubeGraphPoint } from '../../models/tube-graph.model';
import { chartWizardAcceptedExtensionPattern, imageExtensionPattern, imageFileTypes } from '../../services/constants';
import { ErrorService } from '../../services/error.service';
import { FileService } from '../../services/file.service';
import { SaveService } from '../../services/save.service';
import { TubesService } from '../../services/tubes.service';
import { PdfViewerComponent } from '../pdf-viewer/pdf-viewer.component';
import { UploadData } from '../upload/upload.model';
import { UploadDialogService } from '../upload-dialog/upload-dialog.service';
import { ChartWizardDialogdata } from './chart-wizard-dialog.service';
import { DeleteCurveDialogService } from './delete-curve-dialog/delete-curve-dialog.service';
import { ChartAxis, ChartCurve } from './transfer-graph-editor/transfer-graph-editor.component';

type BoundserCursor = 'move' | 'w-resize' | 's-resize' | 'n-resize' | 'e-resize' | 'nw-resize' | 'ne-resize' | 'sw-resize' | 'se-resize';

interface ChartParameters extends ChartAxis {
    title: string;
    vg2?: number;
    isTriode?: boolean;
}

interface ChartBounds {
    imageOffset: DOMRect;
    imageBounds: DOMRect;
}

interface Storage {
    imageOpacity: number;
    selectedDocumentId?: string;
    chartBounds?: ChartBounds;
    chartParameters?: ChartParameters;
    chartCurves: Array<ChartCurve>;
}

const defaultStorage = {
    imageOpacity: 0.5,
    chartParameters: { vaMin: 0, iaMin: 0, isTriode: false } as ChartParameters,
    chartCurves: new Array<ChartCurve>()
} as Storage;

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-chart-wizard',
    templateUrl: './chart-wizard-dialog.component.html',
    styleUrls: ['./chart-wizard-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [SaveService]
})
export class ChartWizardDialogComponent extends DestroyDirective {
    @ViewChild('fileUpload')
    public fileUpload: ElementRef<HTMLInputElement>;

    @ViewChild(PdfViewerComponent)
    public pdfViewerComponent: PdfViewerComponent;

    @ViewChild('chartboundser')
    public set chartBoundser(elementRef: ElementRef<HTMLElement>) {
        this.chartBoundser$.next(elementRef?.nativeElement);
    }

    public tube: Tube;
    public step$ = new BehaviorSubject<number>(1);
    public savePdfImage$ = new Subject<void>();
    public nextDisabled$: Observable<boolean>;
    public storage$ = new BehaviorSubject<Storage>(defaultStorage);
    public lastStorage$: Observable<Storage>;

    // Document (image or pdf)
    public documentUrl$: Observable<string>;
    public hasError = false;
    public waiter = true;

    // pdf
    public uploadData$: Observable<UploadData>;
    public uploadDataDone$ = new Subject<string>();

    // image
    public changeImageOpacity$ = new Subject<number>();

    // Step 1
    public uploadDocument$ = new Subject<Event>();
    public chartWizardAcceptedExtensionPattern = chartWizardAcceptedExtensionPattern;
    public selectedDocumentId$ = new Subject<string>();
    public selectedDocument$: Observable<Document>;
    public selectedDocumentIsImage$: Observable<boolean>;

    // Step 2
    public cursor$: Observable<BoundserCursor>;
    public chartBounds$: Observable<DOMRect>;

    // Step 3
    public parametersForm$: Observable<IFormGroup<ChartParameters>>;

    // Step 4
    public addCurve$ = new Subject<void>();
    public cancelCurve$ = new Subject<void>();
    public finishCurve$ = new Subject<ChartCurve>();
    public updateCurve$ = new Subject<ChartCurve>();
    public editCurve$ = new Subject<ChartCurve>();
    public deleteCurve$ = new Subject<void>();
    public finishValidationError$: Observable<string>;
    public curveAdding$: Observable<ChartCurve>;
    public curves$: Observable<ReadonlyArray<ChartCurve>>;

    // Step 2
    private chartBoundser$ = new ReplaySubject<HTMLElement>(1);
    private imageElement$ = new ReplaySubject<HTMLImageElement>(1);

    public constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public data: ChartWizardDialogdata,
        public errorService: ErrorService,
        private fileService: FileService,
        private dialogRef: MatDialogRef<ChartWizardDialogComponent, TubeGraph>,
        idService: IdService,
        saveService: SaveService,
        tubeService: TubesService,
        uploadDialogService: UploadDialogService,
        changeDetectorRef: ChangeDetectorRef,
        statusService: StatusService,
        formBuilder: FormBuilder,
        inputDialogService: InputDialogService<number>,
        messageBoxDialogService: MessageBoxDialogService,
        deleteCurveDialogService: DeleteCurveDialogService,
        inputAutoSizeService: InputAutoSizeService
    ) {
        super();

        this.tube = cloneDeep(data.tube);

        const parametersForm$ = this.step$.pipe(
            debounceTime(100),
            withLatestFrom(this.storage$),
            map(([step, storage]) => {
                if (step !== 4) {
                    return undefined as IFormGroup<ChartParameters>;
                }

                const fb = formBuilder as IFormBuilder;
                return fb.group<ChartParameters>({
                    title: [storage.chartParameters.title, [Validators.required, inputAutoSizeService.getValidator('title')]],
                    vaMin: [storage.chartParameters.vaMin, [Validators.required, inputAutoSizeService.getValidator('vaMin')]],
                    vaMax: [storage.chartParameters.vaMax, [Validators.required, inputAutoSizeService.getValidator('vaMax')]],
                    iaMin: [storage.chartParameters.iaMin, [Validators.required, inputAutoSizeService.getValidator('iaMin')]],
                    iaMax: [storage.chartParameters.iaMax, [Validators.required, inputAutoSizeService.getValidator('iaMax')]],
                    vg2: [storage.chartParameters.vg2, inputAutoSizeService.getValidator('vg2')],
                    isTriode: storage.chartParameters.isTriode
                });
            }),
            shareReplayLast()
        );

        const parametersFormChange$ = parametersForm$.pipe(
            filter(Boolean),
            switchMap(form => form.valueChanges.pipe(
                withLatestFrom(this.storage$),
                tap(([_, storage]) => {
                    // Store parameters for the other steps
                    const values = form.getRawValue();
                    storage.chartParameters = values;
                    this.storage$.next(storage);

                    if (values.isTriode) {
                        form.controls.vg2.disable({ emitEvent: false });
                        form.controls.vg2.setValidators([]);
                    } else {
                        form.controls.vg2.enable({ emitEvent: false });
                        form.controls.vg2.setValidators([Validators.required]);
                    }
                })
            ))
        );

        this.parametersForm$ = parametersForm$.pipe(
            subscribeWith(parametersFormChange$),
            shareReplayLast()
        );

        this.uploadDocument$.pipe(
            switchMap(event => {
                const target = event.target as HTMLInputElement;
                const file = target.files[0];
                if (!imageFileTypes.includes(file.type)) {
                    return statusService.showStatus$(new Error('Invalid file type.')).pipe(
                        map(() => undefined as Tube)
                    );
                }

                return uploadDialogService.open$({ filename: `docs/${file.name}`, data: file }).pipe(
                    switchMap(resultFileName => {
                        this.fileUpload.nativeElement.value = '';
                        if (resultFileName) {
                            // Clone tube before modifications to avoid a modification of the original instance
                            const updatedTube = cloneDeep(this.tube);
                            const document = {
                                id: idService.generate(),
                                label: file.name,
                                filename: file.name
                            } as Document;
                            updatedTube.documents = [...(updatedTube.documents || []), document];
                            changeDetectorRef.markForCheck();
                            return saveService.saveTube$(updatedTube).pipe(
                                take(1),
                                tap(tubeSaved => {
                                    tubeService.updateTube$.next(tubeSaved);
                                    // update document list
                                    this.tube = tubeSaved;
                                    changeDetectorRef.markForCheck();
                                })
                            );
                        }
                        return of(undefined as Tube);
                    }),
                    catchError((err: unknown) => statusService.showStatus$(err).pipe(
                        map(() => undefined as Tube)
                    ))
                );
            }),
            takeUntil(this.destroyed$)
        ).subscribe();

        this.selectedDocument$ = this.storage$.pipe(
            filterMap(storage => storage.selectedDocumentId || undefined),
            distinctUntilChanged(),
            map(selectedDocumentId => this.tube.documents.find(d => d.id === selectedDocumentId)),
            startWith(undefined as Document),
            shareReplayLast()
        );

        this.selectedDocumentIsImage$ = this.selectedDocument$.pipe(
            map(selectedDocument => imageExtensionPattern.test(selectedDocument?.filename)),
            startWith(false),
            shareReplayLast()
        );

        this.documentUrl$ = this.step$.pipe(
            combineLatestWith(this.selectedDocument$),
            map(([step, selectedDocument]) => ((step > 1 && step < 6) && selectedDocument?.filename) || undefined),
            distinctUntilChanged(),
            switchMap(filename => {
                if (!filename) {
                    return of(undefined);
                }

                this.waiter = true;
                this.hasError = false;
                return this.fileService.getDocumentUrl$(`docs/${filename}`);
            }),
            shareReplayLast()
        );

        const storeDocumentId$ = this.selectedDocumentId$.pipe(
            debounceTime(100),
            withLatestFrom(this.storage$),
            tap(([selectedDocumentId, storage]) => {
                storage.selectedDocumentId = selectedDocumentId;
                this.storage$.next(storage);
            })
        );

        const storeImageOpacity$ = this.changeImageOpacity$.pipe(
            debounceTime(50),
            withLatestFrom(this.storage$),
            tap(([imageOpacity, storage]) => {
                storage.imageOpacity = imageOpacity;
                this.storage$.next(storage);
            })
        );

        const deleteCurve$ = this.deleteCurve$.pipe(
            throttleTime(1000),
            switchMap(() => this.storage$.pipe(
                take(1),
                switchMap(storage => deleteCurveDialogService.open$(storage.chartCurves).pipe(
                    filter(Boolean),
                    map(curves => {
                        storage.chartCurves = curves;
                        this.storage$.next(storage);
                    })
                ))
            ))
        );

        // Store current work to storage
        this.storage$.pipe(
            combineLatestWith(this.step$),
            debounceTime(100),
            filterMap(([storage, step]) => step >= 5 ? storage : undefined),
            subscribeWith(storeDocumentId$, storeImageOpacity$, deleteCurve$),
            takeUntil(this.destroyed$)
        ).subscribe(storage => {
            if (localStorage) {
                const key = `chartwizard-${this.tube.id}`;
                localStorage.setItem(key, JSON.stringify(storage));
            }
        });

        this.lastStorage$ = this.step$.pipe(
            filter(step => localStorage && step === 1),
            map(() => {
                const key = `chartwizard-${this.tube.id}`;
                const storageValue = localStorage.getItem(key);
                const storage = storageValue && JSON.parse(storageValue) as Storage;
                return storage?.chartParameters?.title ? storage : undefined;
            }),
            shareReplayLast()
        );

        const chartBoundserElement$ = this.chartBoundser$.pipe(
            filter(Boolean),
            shareReplayLast()
        );

        const resized$ = fromEvent(window, 'resize').pipe(
            startWith(undefined as Event),
            shareReplayLast()
        );

        const restorePosition$ = chartBoundserElement$.pipe(
            tap(element => element.style.opacity = '0'),
            combineLatestWith(this.imageElement$, this.step$, resized$),
            debounceTime(0),
            filterMap(([element, imageElement, step]) => step >= 3 && step < 6 ? [element, imageElement] : undefined),
            withLatestFrom(this.storage$),
            map(([[element, imageElement], storage]) => {
                const parentBounds = element.parentElement.getBoundingClientRect();
                const imageBounds = imageElement.getBoundingClientRect();

                if (storage.chartBounds) {
                    const previousImageBounds = storage.chartBounds.imageBounds;

                    // Calc new offset
                    const offset = storage.chartBounds.imageOffset;
                    let x = offset.x * imageBounds.width / previousImageBounds.width;
                    let y = offset.y * imageBounds.height / previousImageBounds.height;
                    const width = offset.width * imageBounds.width / previousImageBounds.width;
                    const height = offset.height * imageBounds.height / previousImageBounds.height;

                    // Relative ot the element
                    x += imageBounds.left - parentBounds.left;
                    y += imageBounds.top - parentBounds.top;

                    return { x, y, width, height } as DOMRect;
                } else {
                    return {
                        x: imageBounds.left - parentBounds.left,
                        y: imageBounds.top - parentBounds.top,
                        width: imageBounds.width,
                        height: imageBounds.height
                    } as DOMRect;
                }
            }),
            shareReplayLast()
        );

        const bounds$ = chartBoundserElement$.pipe(
            tap(element => element.style.opacity = '0'),
            combineLatestWith(this.imageElement$),
            withLatestFrom(this.step$),
            filterMap(([elements, step]) => step === 3 ? elements : undefined),
            switchMap(([element]) => fromEvent<MouseEvent>(element, 'mousedown').pipe(
                withLatestFrom(this.cursor$),
                switchMap(([downEvent, cursor]) => {
                    const parentBounds = element.parentElement.getBoundingClientRect();
                    const startBounds = element.getBoundingClientRect();

                    const clientBounds = {
                        width: startBounds.width,
                        height: startBounds.height,
                        left: startBounds.x - parentBounds.x,
                        top: startBounds.y - parentBounds.y,
                        right: startBounds.x - parentBounds.x + startBounds.width,
                        bottom: startBounds.y - parentBounds.y + startBounds.height
                    };

                    const move$ = fromEvent<MouseEvent>(document, 'mousemove').pipe(
                        shareReplayLast()
                    );

                    const kill$ = move$.pipe(
                        filter(moveEvent => moveEvent.buttons !== 1)
                    );

                    return move$.pipe(
                        map(moveEvent => {
                            const xOffset = moveEvent.pageX - downEvent.pageX;
                            const yOffset = moveEvent.pageY - downEvent.pageY;
                            let { left, top, width, height } = clientBounds;

                            if (cursor === 'move') {
                                left = Math.max(0, Math.min(clientBounds.left + xOffset, parentBounds.width - clientBounds.width));
                                top = Math.max(0, Math.min(clientBounds.top + yOffset, parentBounds.height - clientBounds.height));
                            } else {
                                if (cursor === 's-resize' || cursor === 'se-resize' || cursor === 'ne-resize') {
                                    height = Math.max(30, Math.min(clientBounds.height + yOffset, parentBounds.bottom - startBounds.y));
                                }
                                if (cursor === 'w-resize' || cursor === 'se-resize' || cursor === 'sw-resize') {
                                    width = Math.max(30, Math.min(clientBounds.width + xOffset, parentBounds.right - startBounds.x));
                                }
                                if (cursor === 'e-resize' || cursor === 'nw-resize' || cursor === 'ne-resize') {
                                    left = Math.max(0, Math.min(clientBounds.left + xOffset, clientBounds.right - 30));
                                    width = clientBounds.right - left;
                                }
                                if (cursor === 'n-resize' || cursor === 'nw-resize' || cursor === 'sw-resize') {
                                    top = Math.max(0, Math.min(clientBounds.top + yOffset, clientBounds.bottom - 30));
                                    height = clientBounds.bottom - top;
                                }
                            }

                            return { x: left, y: top, width, height } as DOMRect;
                        }),
                        takeUntil(fromEvent<MouseEvent>(document, 'mouseup').pipe(
                            mergeWith(fromEvent<MouseEvent>(document, 'mouseleave'), kill$)
                        ))
                    );
                })
            )),
            shareReplayLast()
        );

        this.chartBounds$ = bounds$.pipe(
            mergeWith(restorePosition$),
            withLatestFrom(chartBoundserElement$),
            map(([bounds, element]) => {
                element.style.left = `${bounds.x}px`;
                element.style.top = `${bounds.y}px`;
                element.style.width = `${bounds.width}px`;
                element.style.height = `${bounds.height}px`;
                element.style.opacity = '1';
                return bounds;
            }),
            debounceTime(10),
            shareReplayLast()
        );

        const storage$ = bounds$.pipe(
            debounceTime(500),
            withLatestFrom(chartBoundserElement$, this.imageElement$, this.storage$),
            tap(([_, element, imageElement, storage]) => {
                const bounds = element.getBoundingClientRect();
                const imageBounds = imageElement.getBoundingClientRect();

                storage.chartBounds = {
                    imageOffset: {
                        x: bounds.x - imageBounds.x,
                        y: bounds.y - imageBounds.y,
                        width: bounds.width,
                        height: bounds.height
                    } as DOMRect,
                    imageBounds
                };
                this.storage$.next(storage);
            })
        );

        this.cursor$ = chartBoundserElement$.pipe(
            switchMap(element => fromEvent<MouseEvent>(element, 'mouseenter').pipe(
                withLatestFrom(this.step$),
                switchMap(([_, step]) => {
                    if (step !== 3) {
                        return of(null);
                    }

                    return fromEvent<MouseEvent>(element, 'mousemove').pipe(
                        filter(moveEvent => moveEvent.buttons !== 1),
                        map(moveEvent => {
                            let cursor = 'move' as BoundserCursor;
                            const bounds = element.getBoundingClientRect();
                            const x = moveEvent.pageX;
                            const y = moveEvent.pageY;

                            if (x < bounds.x + 10 && y < bounds.y + 10) {
                                cursor = 'nw-resize';
                            } else if (x < bounds.x + 10 && y > bounds.bottom - 10) {
                                cursor = 'ne-resize';
                            } else if (x > bounds.right - 10 && y < bounds.y + 10) {
                                cursor = 'sw-resize';
                            } else if (x > bounds.right - 10 && y > bounds.bottom - 10) {
                                cursor = 'se-resize';
                            } else if (x < bounds.x + 10) {
                                cursor = 'e-resize';
                            } else if (y < bounds.y + 10) {
                                cursor = 'n-resize';
                            } else if (x > bounds.right - 10) {
                                cursor = 'w-resize';
                            } else if (y > bounds.bottom - 10) {
                                cursor = 's-resize';
                            }
                            return cursor;
                        }),
                        takeUntil(fromEvent<MouseEvent>(element, 'mouseleave').pipe(
                            mergeWith(fromEvent<MouseEvent>(element, 'mouseenter'))
                        ))
                    );
                })
            )),
            subscribeWith(this.chartBounds$, storage$),
            shareReplayLast()
        );

        this.nextDisabled$ = this.step$.pipe(
            combineLatestWith(this.storage$),
            switchMap(([step, storage]) => {
                if (step === 1) {
                    return this.selectedDocument$.pipe(
                        map(selectedDocument => !selectedDocument)
                    );
                } else if (step === 2) {
                    return of(!this.waiter);
                } else if (step === 3) {
                    return of(!storage.chartBounds);
                } else if (step === 4) {
                    return this.parametersForm$.pipe(
                        filter(Boolean),
                        switchMap(form => parametersFormChange$.pipe(
                            debounceTime(100),
                            map(() => form.invalid),
                            startWith(form.invalid)
                        ))
                    );
                }
                return of(false);
            }),
            shareReplayLast()
        );

        const askForVg1$ = this.addCurve$.pipe(
            throttleTime(1000),
            switchMap(() => {
                const dialogData = {
                    text: 'Enter the grid 1 voltage corresponding to your curve.',
                    dialogType: 'number',
                    fieldLabel: 'Grid1 voltage',
                    value: undefined
                } as InputDialogData<number>;
                return inputDialogService.open$(dialogData).pipe(
                    withLatestFrom(this.storage$),
                    switchMap(([vg1, storage]) => {
                        if (vg1 === undefined) {
                            return of(undefined as ChartCurve);
                        }

                        const existingIndex = storage.chartCurves.findIndex(c => c.vg1 === vg1);
                        const chartCurve = { vg1, p: [{ va: 0, ik: 0 }] } as ChartCurve;
                        if (existingIndex >= 0) {
                            return messageBoxDialogService.openConfirmation$(`the curve for ${vg1} volts already exist, would you like to verride it?`).pipe(
                                map(response => response === 'ok' ? chartCurve : undefined)
                            );
                        } else {
                            return of(chartCurve);
                        }
                    })
                );
            }),
            shareReplayLast()
        );

        const curveCanceled$ = this.cancelCurve$.pipe(
            throttleTime(500),
            map(() => undefined as ChartCurve),
            shareReplayLast()
        );

        const curveFinished$ = this.finishCurve$.pipe(
            throttleTime(500),
            withLatestFrom(this.storage$),
            map(([chartCurve, storage]) => {
                // Add curve to storage
                const existingIndex = storage.chartCurves.findIndex(c => c.vg1 === chartCurve.vg1);
                if (existingIndex >= 0) {
                    storage.chartCurves.splice(existingIndex, 1, chartCurve);
                } else {
                    storage.chartCurves.push(chartCurve);
                }
                storage.chartCurves.sort((c1, c2) => c2.vg1 - c1.vg1);
                this.storage$.next(storage);
                return undefined as ChartCurve;
            }),
            shareReplayLast()
        );

        const curveUpdated$ = this.updateCurve$.pipe(
            switchMap(updatedCurve => this.curveAdding$.pipe(
                take(1),
                map(chartCurve => {
                    chartCurve.p = updatedCurve.p;
                    return chartCurve;
                })
            )),
            shareReplayLast()
        );

        const curveEdited$ = this.editCurve$.pipe(
            withLatestFrom(this.step$),
            filterMap(([curve, step]) => step === 5 ? curve : undefined),
            shareReplayLast()
        );

        this.curveAdding$ = this.addCurve$.pipe(
            map(() => ({ p: [] } as ChartCurve)),
            mergeWith(askForVg1$, curveCanceled$, curveFinished$, curveUpdated$, curveEdited$),
            startWith(undefined),
            shareReplayLast()
        );

        this.curves$ = this.step$.pipe(
            combineLatestWith(this.storage$, this.curveAdding$),
            debounceTime(100),
            map(([step, storage, curveAdding]) => {
                if (step < 5) {
                    return undefined;
                }

                return curveAdding ? [curveAdding] : [...storage.chartCurves];
            }),
            shareReplayLast()
        );

        this.finishValidationError$ = this.storage$.pipe(
            debounceTime(500),
            map(storage => storage.chartCurves?.length < 3),
            startWith(true),
            map(invalid => invalid ? 'At least 3 curves must be added to validate the graphic.' : undefined),
            shareReplayLast()
        );

        // Save selected pdf page to image file and select it
        this.uploadData$ = this.savePdfImage$.pipe(
            switchMap(() => this.pdfViewerComponent.saveCurrentPage$().pipe(
                withLatestFrom(this.selectedDocument$),
                map(([currentPageImage, selectedDocument]) => {
                    // convert base64 to raw binary data held in a string
                    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
                    const arr = currentPageImage.dataUrl.split(',');

                    // eslint-disable-next-line deprecation/deprecation
                    const byteString = atob(arr[1]);

                    // separate out the mime component
                    const mimeString = arr[0].split(':')[1].split(';')[0];

                    // write the bytes of the string to an ArrayBuffer
                    const ab = new ArrayBuffer(byteString.length);

                    // create a view into the buffer
                    const ia = new Uint8Array(ab);

                    // set the bytes of the buffer to the correct values
                    // eslint-disable-next-line no-loops/no-loops
                    for (let i = 0; i < byteString.length; i++) {
                        ia[i] = byteString.charCodeAt(i);
                    }

                    // write the ArrayBuffer to a blob, and you're done
                    const blob = new Blob([ab], { type: mimeString });

                    const filename = `docs/${selectedDocument.filename.replace(/\.[^/.]+$/, '')}_page${currentPageImage.currentPage}.jpg`;

                    return { filename, data: blob } as UploadData;
                })
                // no share replay here
            ))
        );

        this.uploadDataDone$.pipe(
            switchMap(filename => {
                // Clone tube before modifications to avoid a modification of the original instance
                const updatedTube = cloneDeep(this.tube);
                const docFilename = filename.replace(/^docs\//, '');
                const document = {
                    id: idService.generate(),
                    label: docFilename,
                    filename: docFilename
                } as Document;
                updatedTube.documents = [...(updatedTube.documents || []), document];
                return saveService.saveTube$(updatedTube).pipe(
                    take(1),
                    withLatestFrom(this.storage$),
                    tap(([tubeSaved, storage]) => {
                        tubeService.updateTube$.next(tubeSaved);
                        // update document list
                        this.tube = tubeSaved;
                        storage.selectedDocumentId = document.id;
                        this.storage$.next(storage);
                        this.step$.next(3);
                    })
                );
            }),
            takeUntil(this.destroyed$)
        ).subscribe();
    }

    public previous$(): Observable<unknown> {
        return asyncSwitchMap(() => this.step$.pipe(
            take(1),
            tap(step => {
                if (step === 3) {
                    this.step$.next(1);
                } else {
                    this.step$.next(step - 1);
                }
            })
        ));
    }

    public next$(): Observable<unknown> {
        return asyncSwitchMap(() => this.step$.pipe(
            combineLatestWith(this.selectedDocumentIsImage$),
            debounceTime(100),
            take(1),
            tap(([step, selectedDocumentIsImage]) => {
                if (step === 1 && selectedDocumentIsImage) {
                    this.step$.next(3);
                } else if (step === 2) {
                    this.savePdfImage$.next();
                } else {
                    this.step$.next(step + 1);
                }
            })
        ));
    }

    public save$(): Observable<unknown> {
        return asyncSwitchMap(() => this.storage$.pipe(
            take(1),
            map(storage => {
                const curves = storage.chartCurves.map(c => {
                    const points = c.p.map(p => ({
                        va: p.va,
                        ik: p.ik / 1000
                    } as TubeGraphPoint));

                    return {
                        p: points,
                        vg1: c.vg1
                    } as TubeGraphCurve;
                });

                const tubeGraph = {
                    c: curves,
                    vg2: storage.chartParameters.vg2,
                    name: storage.chartParameters.title,
                    triode: storage.chartParameters.isTriode
                } as TubeGraph;

                this.dialogRef.close(tubeGraph);
            })
        ));
    }

    public imageLoaded(image: HTMLImageElement): void {
        this.waiter = false;
        this.imageElement$.next(image);
    }

    public loadStorage$(storage: Storage): Observable<void> {
        return asyncMap(() => {
            this.storage$.next(storage);
            this.step$.next(5);
        });
    }
}
