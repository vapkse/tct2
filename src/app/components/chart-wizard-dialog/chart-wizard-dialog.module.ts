import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';

import { InputAutosizeModule } from '../../common/input-autosize/input-autosize.module';
import { AbstractLazyModule } from '../../common/lazy-loading/lazy-loader.service';
import { NumericStepperModule } from '../../common/numeric-stepper/numeric-stepper.module';
import { ThrottleEventModule } from '../../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../../translation/translation.module';
import { AttachmentsModule } from '../attachments/attachments.module';
import { PdfViewerModule } from '../pdf-viewer/pdf-viewer.module';
import { UploadModule } from '../upload/upload.module';
import { ChartWizardDialogComponent } from './chart-wizard-dialog.component';
import { TransferGraphEditorModule } from './transfer-graph-editor/transfer-graph-editor.module';

@NgModule({
    imports: [
        AttachmentsModule,
        CommonModule,
        InputAutosizeModule,
        MatButtonModule,
        MatCardModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatRadioModule,
        MatSliderModule,
        MatToolbarModule,
        NumericStepperModule,
        PdfViewerModule,
        ReactiveFormsModule,
        ThrottleEventModule,
        TransferGraphEditorModule,
        TranslationModule,
        UploadModule
    ],
    entryComponents: [ChartWizardDialogComponent],
    exports: [ChartWizardDialogComponent],
    declarations: [ChartWizardDialogComponent]
})
export class ChartWizardDialogModule extends AbstractLazyModule<ChartWizardDialogComponent> {
    public constructor() {
        super(ChartWizardDialogComponent);
    }
}
