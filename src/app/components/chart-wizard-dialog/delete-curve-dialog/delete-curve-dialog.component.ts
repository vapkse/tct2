import { ChangeDetectionStrategy, Component, Inject, QueryList, ViewChildren } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { asyncMap } from 'src/app/common/custom-operators';

import { ChartCurve } from '../transfer-graph-editor/transfer-graph-editor.component';


@Component({
    selector: 'app-delete-curve-dialog',
    templateUrl: './delete-curve-dialog.component.html',
    styleUrls: ['./delete-curve-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteCurveDialogComponent {
    @ViewChildren(MatCheckbox)
    public checkBoxes: QueryList<MatCheckbox>;

    public constructor(
        @Inject(MAT_DIALOG_DATA) public curves: Array<ChartCurve>,
        private dialogRef: MatDialogRef<DeleteCurveDialogComponent, Array<ChartCurve>>
    ) { }

    public validate$(): Observable<void> {
        return asyncMap(() => {
            const curvesMap = this.curves.reduce((m, curve) => m.set(curve.vg1, curve), new Map<number, ChartCurve>());
            const curves = this.checkBoxes
                .filter(checkBox => !checkBox.checked)
                .map(checkBox => curvesMap.get(+checkBox.id));
            this.dialogRef.close(curves);
        });
    }
}
