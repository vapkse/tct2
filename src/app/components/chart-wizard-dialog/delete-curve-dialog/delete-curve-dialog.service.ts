import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { AbstractLazyModule, DialogService, LazyLoaderService } from 'src/app/common/lazy-loading';

import { ChartCurve } from '../transfer-graph-editor/transfer-graph-editor.component';

@Injectable({
    providedIn: 'root'
})
export class DeleteCurveDialogService extends DialogService<Array<ChartCurve>, Array<ChartCurve>> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        super(lazyLoaderService, dialog);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./delete-curve-dialog.module').then(m => m.DeleteCurveDialogModule);
    }
}
