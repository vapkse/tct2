import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from 'src/app/common/lazy-loading';

import { ThrottleEventModule } from '../../../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../../../translation/translation.module';
import { DeleteCurveDialogComponent } from './delete-curve-dialog.component';

@NgModule({
    declarations: [DeleteCurveDialogComponent],
    exports: [DeleteCurveDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        ThrottleEventModule,
        TranslationModule
    ]
})
export class DeleteCurveDialogModule extends AbstractLazyModule<DeleteCurveDialogComponent> {
    public constructor() {
        super(DeleteCurveDialogComponent);
    }
}
