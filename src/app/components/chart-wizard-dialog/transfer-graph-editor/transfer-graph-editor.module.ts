import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HighchartsChartModule } from 'highcharts-angular';

import { TransferGraphEditorComponent } from './transfer-graph-editor.component';

@NgModule({
    declarations: [TransferGraphEditorComponent],
    exports: [TransferGraphEditorComponent],
    imports: [
        CommonModule,
        HighchartsChartModule
    ]
})
export class TransferGraphEditorModule { }
