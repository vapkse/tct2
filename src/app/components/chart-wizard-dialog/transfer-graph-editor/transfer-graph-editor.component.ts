import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Chart, Options, Point, PointOptionsObject, Series, SeriesAfterAnimateEventObject, SeriesSplineOptions, SVGElement } from 'highcharts';
import { HighchartsChartComponent } from 'highcharts-angular';
import { isEqual } from 'lodash-es';
import { BehaviorSubject, combineLatestWith, debounceTime, distinctUntilChanged, filter, fromEvent, map, Observable, ReplaySubject, startWith, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs';

import { filterMap, shareReplayLast, subscribeWith } from '../../../common/custom-operators';
import { DestroyDirective } from '../../../common/destroy/destroy.directive';
import { ThemeService } from '../../../services/theme.service';

export interface ShowEvent {
    target: Series;
}

export interface ChartSeriesOptions extends SeriesSplineOptions {
    vg1: number;
    curve: ChartCurve;
}

export interface ChartCurve {
    vg1: number;
    p: Array<ChartPoint>;
}

export interface ChartPoint {
    ik: number;
    va: number;
}

export interface ChartAxis {
    vaMin: number;
    vaMax: number;
    iaMin: number;
    iaMax: number;
}

interface TargetPoint extends Point {
    graphic: SVGElement;
}

interface TargetSerie extends Series {
    options: ChartSeriesOptions;
    group: SVGElement;
    graph: SVGElement;
    tracker: SVGElement;
    points: Array<TargetPoint>;
}

@Component({
    selector: 'app-transfer-graph-editor',
    templateUrl: './transfer-graph-editor.component.html',
    styleUrls: ['./transfer-graph-editor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class TransferGraphEditorComponent extends DestroyDirective {
    @ViewChild(HighchartsChartComponent)
    public set chart(value: HighchartsChartComponent) {
        const chartComponent = value as unknown as {
            chart: Chart;
            el: ElementRef<HTMLElement>;
        };
        this.chartElement$.next(chartComponent?.el?.nativeElement);
        this.chartInstance$.next(chartComponent?.chart);
    }

    @Output()
    public readonly curveChange = new EventEmitter<ChartCurve>();

    @Output()
    public readonly curveDoubleClick = new EventEmitter<ChartCurve>();

    @Input()
    public set curves(curves: ReadonlyArray<ChartCurve>) {
        this.curves$.next(curves);
    }

    @Input()
    public set bounds(bounds: DOMRect) {
        this.bounds$.next(bounds);
    }

    @Input()
    public set axis(axis: ChartAxis) {
        this.axis$.next(axis);
    }

    @Input()
    public set showAxis(showAxis: boolean) {
        this.showAxis$.next(showAxis);
    }

    @Input()
    public set title(title: string) {
        this.title$.next(title);
    }

    @Input()
    public set editable(editable: boolean) {
        this.editable$.next(editable);
    }

    public highcharts = Highcharts;
    public chartOptions$: Observable<Options>;

    private chartElement$ = new ReplaySubject<HTMLElement>(1);
    private chartInstance$ = new ReplaySubject<Chart>(1);
    private bounds$ = new ReplaySubject<DOMRect>(1);
    private curves$ = new ReplaySubject<ReadonlyArray<ChartCurve>>(1);
    private axis$ = new ReplaySubject<ChartAxis>(1);
    private showAxis$ = new ReplaySubject<boolean>(1);
    private title$ = new BehaviorSubject<string>('');
    private editable$ = new BehaviorSubject<boolean>(false);

    public constructor(
        themeService: ThemeService,
        elementRef: ElementRef<HTMLElement>
    ) {
        super();

        const placeChart$ = this.chartElement$.pipe(
            filter(Boolean),
            tap(chartElement => {
                chartElement.style.opacity = '0';
            }),
            combineLatestWith(this.bounds$, this.chartInstance$),
            debounceTime(100),
            tap(([chartElement, bounds, chartInstance]) => {
                if (bounds) {
                    chartElement.style.left = `${bounds.x}px`;
                    chartElement.style.top = `${bounds.y}px`;
                    chartElement.style.width = `${bounds.width}px`;
                    chartElement.style.height = `${bounds.height}px`;
                    chartElement.style.opacity = '1';

                    if (chartInstance) {
                        chartInstance.reflow();
                    }
                }
            })
        );

        const updated$ = this.curveChange.pipe(
            startWith(undefined as ChartCurve),
            shareReplayLast()
        );

        const curves$ = this.curves$.pipe(
            combineLatestWith(updated$),
            debounceTime(10),
            withLatestFrom(this.editable$),
            filterMap(([[curves, updated], editable]) => !editable || !isEqual(curves[0], updated) ? curves : undefined),
            shareReplayLast()
        );

        const setSeries$ = this.chartInstance$.pipe(
            filter(Boolean),
            combineLatestWith(curves$, themeService.materialPallets$),
            tap(([chartInstance, curves, materialPallets]) => {
                const series = curves?.map((curve, index) => {
                    const points = curve.p.map(p => ({
                        x: p.va,
                        y: p.ik,
                        id: `${p.va}#${p.ik}`
                    } as PointOptionsObject));
                    const name = `${curve.vg1}V`;
                    const id = `c${curve.vg1}`;
                    const serie = {
                        type: 'spline',
                        lineWidth: 3,
                        name,
                        vg1: curve.vg1,
                        curve,
                        data: points,
                        color: materialPallets[index % materialPallets.length]['500'],
                        index: index,
                        zIndex: 100 - index,
                        id,
                        visible: true,
                        marker: {
                            enabled: true,
                            symbol: 'circle',
                            radius: 6
                        }
                    } as ChartSeriesOptions;
                    return serie;
                }) || [];

                if (series.length === 0) {
                    series.push({
                        type: 'spline',
                        visible: true
                    } as ChartSeriesOptions);
                }

                let length = chartInstance.series.length;
                // eslint-disable-next-line no-loops/no-loops
                while (--length >= 0) {
                    chartInstance.series[0].remove(false, false);
                }

                series.forEach(() => {
                    chartInstance.addSeries({} as ChartSeriesOptions, false, false);
                });

                if (series.length) {
                    chartInstance.update({
                        series
                    }, true, false, false);
                }
            })
        );

        const cancelEdition$ = this.editable$.pipe(
            filter(editable => !editable)
        );

        const updatePoints$ = this.editable$.pipe(
            filter(Boolean),
            switchMap(() => this.chartElement$),
            filter(Boolean),
            switchMap(chartElement => fromEvent<MouseEvent>(chartElement, 'mousedown').pipe(
                withLatestFrom(this.chartInstance$),
                switchMap(([downEvent, chartInstance]) => {
                    const clickedElement = downEvent.target as HTMLElement;
                    const serie = chartInstance.series[0]; // Only one serie in editable mode
                    const isPoint = clickedElement.classList.contains('highcharts-point');
                    const point = isPoint && serie.points.find(p => (p as TargetPoint).graphic.element === clickedElement);
                    let hasMoved = false;
                    const bounds = chartElement.getBoundingClientRect();
                    const va = serie.xAxis.toValue(downEvent.pageX - bounds.x, false);
                    const ia = serie.yAxis.toValue(downEvent.pageY - bounds.y, false);

                    const setPoint = (x: number, y: number, pt?: Point): void => {
                        const pointOptions = {
                            x: Math.max(Math.min(x, serie.xAxis.max), serie.xAxis.min),
                            y: Math.max(Math.min(y, serie.yAxis.max), serie.yAxis.min)
                        } as PointOptionsObject;
                        if (!pt) {
                            serie.addPoint(pointOptions, true, false, false);
                        } else {
                            pt.update(pointOptions, true, false);
                        }
                    };

                    const raiseEvent = (): void => {
                        const options = serie.options as ChartSeriesOptions;
                        const curve = options.curve;
                        curve.p = serie.points.map(p => ({
                            va: p.x,
                            ik: p.y
                        } as ChartPoint));
                        this.curveChange.emit(curve);
                    };

                    const mouseUp$ = fromEvent<MouseEvent>(document, 'mouseup').pipe(
                        tap(upEvent => {
                            if (hasMoved) {
                                // Move operation
                                raiseEvent();
                            } else if (Math.abs(upEvent.pageX - downEvent.pageX) <= 3 && Math.abs(upEvent.pageY - downEvent.pageY) <= 3) {
                                // Click operation
                                if (isPoint) {
                                    if (serie.points.length > 1) {
                                        // Remove clicked point
                                        const index = serie.points.findIndex(p => (p as TargetPoint).graphic.element === clickedElement);
                                        if (index >= 0) {
                                            serie.removePoint(index, true, false);
                                            raiseEvent();
                                        }
                                    }
                                } else {
                                    // Add point at the clicked place
                                    setPoint(va, ia);
                                    raiseEvent();
                                }
                            }
                        })
                    );

                    if (!point) {
                        return mouseUp$;
                    }

                    return fromEvent<MouseEvent>(chartElement, 'mousemove').pipe(
                        debounceTime(0),
                        distinctUntilChanged((e1, e2) => e1.pageX === e2.pageX && e1.pageY === e2.pageY),
                        tap(moveEvent => {
                            if (!hasMoved && (Math.abs(moveEvent.pageX - downEvent.pageX) > 3 || Math.abs(moveEvent.pageY - downEvent.pageY) > 3)) {
                                hasMoved = true;
                            } else if (hasMoved) {
                                const newVa = serie.xAxis.toValue(moveEvent.pageX - bounds.x, false);
                                const newIa = serie.yAxis.toValue(moveEvent.pageY - bounds.y, false);
                                setPoint(newVa, newIa, point);
                            }
                        }),
                        takeUntil(mouseUp$)
                    );
                }),
                takeUntil(cancelEdition$)
            ))
        );

        const doubleClickWatcher$ = fromEvent(elementRef.nativeElement, 'dblclick').pipe(
            withLatestFrom(this.chartInstance$),
            tap(([event, chartInstance]) => {
                let target = event.target as HTMLElement;

                // eslint-disable-next-line no-loops/no-loops
                while (target && !target.hasAttribute('tct-serie')) {
                    target = target.parentElement;
                    if (target === elementRef.nativeElement) {
                        target = undefined;
                        break;
                    }
                }

                if (target) {
                    const id = target.getAttribute('s-id');
                    const serie = chartInstance.series.find(s => s.options.id === id);
                    const options = serie?.options as ChartSeriesOptions;
                    if (options?.curve) {
                        this.curveDoubleClick.emit(options?.curve);
                    }
                }
            })
        );

        this.chartOptions$ = this.axis$.pipe(
            combineLatestWith(this.showAxis$, this.title$),
            debounceTime(100),
            map(([axis, showAxis, title]) => ({
                title: {
                    text: title
                },
                subTitle: {
                    text: ''
                },
                chart: {
                    animation: false,
                    borderWidth: 0,
                    zoomType: null,
                    backgroundColor: 'transparent',
                    type: 'line',
                    margin: [0, 0, 0, 0]
                },
                xAxis: {
                    type: 'linear',
                    visible: showAxis,
                    min: axis.vaMin,
                    max: axis.vaMax
                },
                yAxis: {
                    type: 'linear',
                    visible: showAxis,
                    min: axis.iaMin,
                    max: axis.iaMax
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        animation: false,
                        lineWidth: 3,
                        states: {
                            inactive: {
                                opacity: 1
                            }
                        },
                        events: {
                            afterAnimate: (event: SeriesAfterAnimateEventObject) => {
                                const serie = event.target as TargetSerie;
                                const options = serie.options;
                                const id = options.id;
                                serie.graph?.element.setAttribute('s-id', id);
                                serie.graph?.element.setAttribute('tct-serie', id);
                                serie.group?.element.setAttribute('s-id', id);
                                serie.group?.element.setAttribute('tct-serie', id);
                                serie.tracker?.element.setAttribute('s-id', id);
                                serie.tracker?.element.setAttribute('tct-serie', id);
                                serie.points?.forEach(p => {
                                    p.graphic?.element.setAttribute('s-id', id);
                                    p.graphic?.element.setAttribute('tct-serie', id);
                                });
                            }
                        }
                    }
                },
                series: []
            } as Options)),
            subscribeWith(placeChart$, setSeries$, updatePoints$, doubleClickWatcher$)
        );
    }
}
