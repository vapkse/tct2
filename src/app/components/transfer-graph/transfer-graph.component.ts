import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Chart, Options, Point, Series, SVGElement } from 'highcharts';
import { HighchartsChartComponent } from 'highcharts-angular';
import { round } from 'lodash-es';
import { BehaviorSubject, combineLatestWith, debounceTime, distinctUntilChanged, filter, fromEvent, map, mergeWith, Observable, of, Subject, switchMap, takeUntil, tap, withLatestFrom } from 'rxjs';

import { shareReplayLast, subscribeWith } from '../../common/custom-operators';
import { TubeGraph } from '../../models/tube-graph.model';
import { TubeUsage } from '../../models/tube-usage.model';
import { TubeUsageZoomX, TubeUsageZoomY } from '../../models/tube-usage-zoom.model';
import { TranslationService } from '../../translation/translation.service';
import { GraphSeriesOptions, TransferGraphChartService } from './services/transfer-graph-chart.service';
import { TransferGraphComputeService } from './services/transfer-graph-compute.service';
import { TransferGraphUsageService } from './services/transfer-graph-usage.service';

interface TargetPoint extends Point {
    graphic: SVGElement;
}

export interface ShowEvent {
    target: Series;
}

@Component({
    selector: 'app-transfer-graph',
    templateUrl: './transfer-graph.component.html',
    styleUrls: ['./transfer-graph.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    providers: [TransferGraphChartService, TransferGraphUsageService, TransferGraphComputeService]
})
export class TransferGraphComponent {
    @ViewChild(HighchartsChartComponent)
    public set chart(value: HighchartsChartComponent) {
        this.chartInstance = (value as unknown as { chart: Chart })?.chart;
        this.transferGraphComputeService.chart$.next(this.chartInstance);
    }

    @Input()
    public set graph(value: TubeGraph) {
        this.transferGraphChartService.graph$.next(value);
    }

    @Input()
    public set usage(value: TubeUsage) {
        this.fullUsage = value;
        this.transferGraphUsageService.partialUsage$.next(value);
    }

    @Input()
    public set paMax(value: number) {
        this.transferGraphComputeService.paMax$.next(value);
    }

    @Input()
    public set editable(value: boolean) {
        this.editable$.next(value);
    }

    @Output()
    public readonly zoomChangedX = new EventEmitter<TubeUsageZoomX>();

    @Output()
    public readonly zoomChangedY = new EventEmitter<TubeUsageZoomY>();

    @Output()
    public readonly showSerie = new EventEmitter<Series>();

    @Output()
    public readonly hideSerie = new EventEmitter<Series>();

    @Output()
    public readonly dragDrop = new EventEmitter<Partial<TubeUsage>>(); // Changes made with drag and drop

    @Output()
    public readonly dragEnd = new EventEmitter<TubeUsage>(); // Changes made with drag and drop

    public highcharts = Highcharts;
    public chartOptions$: Observable<Options>;

    public round = round;
    public zoomEnabled$ = new BehaviorSubject<boolean>(false);
    public editable$ = new BehaviorSubject<boolean>(false);

    private chartInstance: Chart;
    private fullUsage: TubeUsage;

    public constructor(
        public transferGraphComputeService: TransferGraphComputeService,
        public transferGraphUsageService: TransferGraphUsageService,
        private transferGraphChartService: TransferGraphChartService,
        translationService: TranslationService
    ) {
        const addRemoveOrUpdateSeries$ = this.transferGraphComputeService.chart$.pipe(
            filter(Boolean),
            debounceTime(50),
            switchMap(() => transferGraphUsageService.aClassSerie$.pipe(
                mergeWith(transferGraphUsageService.workingPointSerie$, transferGraphUsageService.bClassSerie$, transferGraphComputeService.vg1MinSerie$, transferGraphComputeService.vg1MaxSerie$, transferGraphComputeService.paMaxSerie$) // , transferGraphComputeService.crossPointsSerie$)
            )),
            filter(Boolean),
            withLatestFrom(transferGraphUsageService.hiddenSeries$),
            tap(([newSerie, hiddenSeries]) => {
                try {
                    newSerie.visible = !hiddenSeries.has(newSerie.id);
                    const existingSerie = this.chartInstance.series.find(s => s.options.id === newSerie.id);
                    if (existingSerie && !newSerie.data) {
                        existingSerie.remove(true, false);
                    } else if (existingSerie) {
                        if (existingSerie.options.color !== newSerie.color) {
                            existingSerie.update(newSerie, true);
                        } else {
                            existingSerie.setData(newSerie.data, true, false);
                        }
                    } else if (newSerie.data) {
                        this.chartInstance.addSeries(newSerie, true, false);
                    }
                } catch (err: unknown) {
                    console.error('Error in series update, may be the chart is wrong', err);
                }
            })
        );

        const refreshSeriesVisibility$ = new Subject<void>();
        const ensureSeriesVisibility$ = refreshSeriesVisibility$.pipe(
            combineLatestWith(transferGraphUsageService.hiddenSeries$),
            debounceTime(10),
            tap(([_, hiddenSeries]) => {
                if (!this.chartInstance) {
                    console.warn('ensureSeriesVisibility called without chart instance');
                    return;
                }

                const updated = this.chartInstance.series.filter(s => {
                    const options = s.options as GraphSeriesOptions;
                    const isVisible = !hiddenSeries.has(options.id);
                    if (s.visible !== isVisible) {
                        s.setVisible(isVisible, false);
                        return true;
                    }
                    return false;
                });

                if (updated.length) {
                    this.chartInstance.redraw(false);
                }
            })
        );

        const setZoom$ = transferGraphUsageService.partialUsage$.pipe(
            map(usage => usage.zoom),
            filter(Boolean),
            debounceTime(10),
            tap(zoom => {
                if (this.chartInstance) {
                    try {
                        const xExtremes = this.chartInstance.xAxis[0].getExtremes();
                        const yExtremes = this.chartInstance.yAxis[0].getExtremes();
                        const minX = zoom?.minX || 0;
                        const maxX = zoom?.maxX;
                        const minY = zoom?.minY || 0;
                        const maxY = zoom?.maxY;
                        const xHasChanged = !!xExtremes.userMin !== !!minX || !!xExtremes.userMax !== !!maxX;
                        const yHasChanged = !!yExtremes.userMin !== !!minY || !!yExtremes.userMax !== !!maxY;

                        if (xHasChanged) {
                            if (maxX) {
                                this.chartInstance.xAxis[0].setExtremes(zoom.minX || 0, zoom.maxX, true, false);
                            } else {
                                this.chartInstance.xAxis[0].setExtremes(undefined, undefined, true, false);
                            }
                        }

                        if (yHasChanged) {
                            if (maxY) {
                                this.chartInstance.yAxis[0].setExtremes(zoom.minY || 0, zoom.maxY, true, false);
                            } else {
                                this.chartInstance.yAxis[0].setExtremes(undefined, undefined, true, false);
                            }
                        }

                    } catch (err: unknown) {
                        console.error('Error in series set zoom', err);
                    }
                }
            })
        );

        const mouseDown$ = this.editable$.pipe(
            filter(Boolean),
            switchMap(() => this.transferGraphComputeService.chart$),
            filter(Boolean),
            switchMap(chart => fromEvent<MouseEvent>(chart.container, 'mousedown').pipe(
                withLatestFrom(this.transferGraphComputeService.crossPoints$, this.zoomEnabled$),
                switchMap(([mouseDownEvent, crossPoints, zoomEnabled]) => {
                    const closestDragableElement = (element: HTMLElement): HTMLElement => {
                        // eslint-disable-next-line no-loops/no-loops
                        while (element && !element.classList.contains('tct-draggable')) {
                            element = element.parentElement;
                        }
                        return element;
                    };

                    const closest = closestDragableElement(mouseDownEvent.target as HTMLElement);
                    if (!closest || closest.classList.contains('highcharts-legend-item')) {
                        return of(undefined as MouseEvent);
                    }

                    const isClassA = closest.classList.contains('tct-class-a');
                    const isWorkingPoint = closest.classList.contains('tct-working-point');
                    const isVg1Min = closest.classList.contains('tct-vg1-min');
                    const isVg1Max = closest.classList.contains('tct-vg1-max');
                    const seriesMap = chart.series.reduce((m, serie) => m.set(serie.options.id, serie), new Map<string, Series>());

                    const serie = (isClassA && seriesMap.get('class-a')) ||
                        (isWorkingPoint && seriesMap.get('working-point')) ||
                        (isVg1Min && seriesMap.get('vg1-min')) ||
                        (isVg1Max && seriesMap.get('vg1-max'));

                    if (!serie?.visible) {
                        return of(undefined as MouseEvent);
                    }

                    mouseDownEvent.preventDefault();

                    // disable zoom and tooltips
                    this.chartInstance.update({
                        tooltip: {
                            style: {
                                display: 'none'
                            }
                        },
                        chart: {
                            zoomType: null
                        }
                    });
                    this.disableAutoScaling();

                    const originalWorkingPointXinPx = serie.xAxis.toPixels(this.fullUsage.va, true);
                    const workingPoint = this.transferGraphComputeService.calcPointFromVa(this.fullUsage.va, true, crossPoints);
                    const vaMax = (isVg1Min || isVg1Max || isClassA) && (serie.data[1]?.x || serie.data[0].x);
                    const iaMax = isClassA && serie.data[0].y;

                    const isPointClassA0 = isClassA && (serie.points[0] as TargetPoint)?.graphic.element === mouseDownEvent.target;
                    const isPointClassA1 = isClassA && (serie.points[1] as TargetPoint)?.graphic.element === mouseDownEvent.target;

                    const kill$ = fromEvent<MouseEvent>(chart.container, 'mouseup').pipe(
                        mergeWith(fromEvent<MouseEvent>(chart.container, 'mouseleave')),
                        tap(() => {
                            // enable zoom and tooltips
                            this.chartInstance.update({
                                chart: {
                                    zoomType: zoomEnabled ? 'xy' : null
                                },
                                tooltip: {
                                    style: {
                                        display: 'block'
                                    }
                                }
                            });
                            this.enableAutoScaling();
                            this.dragEnd.emit(this.fullUsage);
                        })
                    );

                    return fromEvent<MouseEvent>(chart.container, 'mousemove').pipe(
                        distinctUntilChanged((previousEvent, mouseMoveEvent) => previousEvent.pageX === mouseMoveEvent.pageX && previousEvent.pageY === mouseMoveEvent.pageY),
                        debounceTime(0),
                        distinctUntilChanged((previousEvent, mouseMoveEvent) => {
                            mouseMoveEvent.preventDefault();
                            // const yOffset = mouseMoveEvent.pageY - mouseDownEvent.pageY;

                            if (isWorkingPoint) {
                                if (previousEvent.pageX !== mouseMoveEvent.pageX) {
                                    // Calc new class working points
                                    const xOffset = mouseMoveEvent.pageX - mouseDownEvent.pageX;
                                    let va = Math.max(serie.xAxis.toValue(originalWorkingPointXinPx + xOffset, true), 0.001);
                                    const z = transferGraphUsageService.getLoadEffectiveZ(this.fullUsage.mode, this.fullUsage.load);
                                    const iaDiff = 1000 * (va - this.fullUsage.va) / z;
                                    let iazero = this.fullUsage.iazero - iaDiff;
                                    if (iazero < 0) {
                                        va = crossPoints[0].va;
                                        iazero = crossPoints[0].ia;
                                    }

                                    const partialUsage = {
                                        va,
                                        iazero,
                                        load: this.fullUsage.load,
                                        mode: this.fullUsage.mode,
                                        vinpp: this.fullUsage.vinpp
                                    };
                                    this.dragDrop.emit({ va, iazero });
                                    this.fullUsage = { ...this.fullUsage, ...partialUsage };
                                    this.transferGraphUsageService.partialUsage$.next(partialUsage);
                                    return false;
                                }
                            } else if (isVg1Min || isVg1Max) {
                                if (previousEvent.pageX !== mouseMoveEvent.pageX) {
                                    // Calc new class working points
                                    const xOffset = mouseMoveEvent.pageX - mouseDownEvent.pageX;
                                    const vdiff = serie.xAxis.toValue(xOffset, true);
                                    const va = vaMax + vdiff;

                                    // Calc new vg1 min or max
                                    const newPoint = this.transferGraphComputeService.calcPointFromVa(va, true, crossPoints);
                                    const vinpp = 2 * Math.max(0, Math.abs(newPoint.vg1 - workingPoint.vg1));

                                    const partialUsage = {
                                        va: this.fullUsage.va,
                                        iazero: this.fullUsage.iazero,
                                        load: this.fullUsage.load,
                                        mode: this.fullUsage.mode,
                                        vinpp
                                    };
                                    this.dragDrop.emit({ vinpp });
                                    this.fullUsage = { ...this.fullUsage, ...partialUsage };
                                    this.transferGraphUsageService.partialUsage$.next(partialUsage);

                                    return false;
                                }
                            } else if (isPointClassA0) {
                                if (previousEvent.pageY !== mouseMoveEvent.pageY) {
                                    const iaAxisMax = serie.yAxis.toValue(0, true);
                                    const yOffset = mouseMoveEvent.pageY - mouseDownEvent.pageY;
                                    const idiff = serie.yAxis.toValue(yOffset, true) - iaAxisMax;
                                    const i = Math.max(iaAxisMax / 100, Math.min(iaMax + idiff, iaAxisMax));
                                    const z = 1000 * vaMax / i;
                                    const load = transferGraphUsageService.getLoad(this.fullUsage.mode, z);
                                    const iazero = 1000 * (vaMax - this.fullUsage.va) / z;

                                    const partialUsage = {
                                        va: this.fullUsage.va,
                                        iazero,
                                        load,
                                        mode: this.fullUsage.mode,
                                        vinpp: this.fullUsage.vinpp
                                    };
                                    this.dragDrop.emit({ load, iazero });
                                    this.fullUsage = { ...this.fullUsage, ...partialUsage };
                                    this.transferGraphUsageService.partialUsage$.next(partialUsage);

                                    return false;
                                }

                            } else if (isPointClassA1) {
                                if (previousEvent.pageX !== mouseMoveEvent.pageX) {
                                    const xOffset = mouseMoveEvent.pageX - mouseDownEvent.pageX;
                                    const vdiff = serie.xAxis.toValue(xOffset, true);
                                    let newVaMax = vaMax + vdiff;
                                    if (newVaMax < vaMax / 10) {
                                        newVaMax = vaMax / 10;
                                    }
                                    const z = 1000 * newVaMax / iaMax;
                                    const load = transferGraphUsageService.getLoad(this.fullUsage.mode, z);
                                    const va = newVaMax - (newVaMax * this.fullUsage.iazero) / iaMax;

                                    const partialUsage = {
                                        va,
                                        iazero: this.fullUsage.iazero,
                                        load,
                                        mode: this.fullUsage.mode,
                                        vinpp: this.fullUsage.vinpp
                                    };
                                    this.dragDrop.emit({ load, va });
                                    this.fullUsage = { ...this.fullUsage, ...partialUsage };
                                    this.transferGraphUsageService.partialUsage$.next(partialUsage);

                                    return false;
                                }

                            } else if (isClassA) {
                                if (previousEvent.pageY !== mouseMoveEvent.pageY) {
                                    const iaAxisMax = serie.yAxis.toValue(0, true);
                                    const yOffset = mouseMoveEvent.pageY - mouseDownEvent.pageY;
                                    const idiff = serie.yAxis.toValue(yOffset, true) - iaAxisMax;
                                    const iazero = Math.max(Math.min(workingPoint.ia + idiff, iaAxisMax), iaAxisMax / 100);

                                    const partialUsage = {
                                        va: this.fullUsage.va,
                                        iazero,
                                        load: this.fullUsage.load,
                                        mode: this.fullUsage.mode,
                                        vinpp: this.fullUsage.vinpp
                                    };
                                    this.dragDrop.emit({ iazero });
                                    this.fullUsage = { ...this.fullUsage, ...partialUsage };
                                    this.transferGraphUsageService.partialUsage$.next(partialUsage);

                                    return false;
                                }
                            }

                            return true;
                        }),
                        takeUntil(kill$)
                    );
                })
            ))
        );

        const manageZoomEnability$ = this.zoomEnabled$.pipe(
            filter(() => !!this.chartInstance),
            tap(zoomEnabled => {
                this.chartInstance?.update({
                    chart: {
                        zoomType: zoomEnabled ? 'xy' : null
                    }
                });
            })
        );

        this.chartOptions$ = transferGraphChartService.graphSeriesParams$.pipe(
            withLatestFrom(this.transferGraphChartService.graph$, this.zoomEnabled$),
            map(([seriesParams, graph, zoomEnabled]) => {
                const options = {
                    title: {
                        text: graph.name
                    },
                    subtitle: {
                        // eslint-disable-next-line @typescript-eslint/naming-convention
                        useHTML: true,
                        text: `<span translate>${translationService.translate('Transfer Function')}</span>`
                    },
                    chart: {
                        animation: false,
                        borderWidth: 1,
                        zoomType: zoomEnabled ? 'xy' : null,
                        zoomKey: 'ctrl',
                        backgroundColor: 'rgba(0,0,0,0)'
                    },
                    xAxis: {
                        type: 'linear',
                        title: {
                            // eslint-disable-next-line @typescript-eslint/naming-convention
                            useHTML: true,
                            text: `<span>${translationService.translate('Anode Voltage')} (V)</span>`
                        },
                        labels: {
                            overflow: 'justify'
                        },
                        events: {
                            setExtremes: event => {
                                const xAxis = this.chartInstance.xAxis[0];
                                const extrems = xAxis.getExtremes();
                                const zoomFactor = Math.abs((event.max - event.min) / (extrems.dataMax - extrems.dataMin));
                                if (zoomFactor < 0.2) {
                                    event.preventDefault();
                                }
                            },
                            afterSetExtremes: event => {
                                console.log('afterSetExtremes x', event);
                                this.zoomChangedX.emit({
                                    minX: event.min > event.dataMin ? event.min : undefined,
                                    maxX: event.max < event.dataMax ? event.max : undefined
                                });
                            }
                        }
                    },
                    yAxis: {
                        type: 'linear',
                        title: {
                            // eslint-disable-next-line @typescript-eslint/naming-convention
                            useHTML: true,
                            text: `<span translate>${translationService.translate('Anode Current')} (mA)</span>`
                        },
                        lineWidth: 1,
                        minorGridLineWidth: 1,
                        gridLineWidth: 1,
                        alternateGridColor: null,
                        events: {
                            setExtremes: event => {
                                const yAxis = this.chartInstance.yAxis[0];
                                const extrems = yAxis.getExtremes();
                                const zoomFactor = Math.abs((event.max - event.min) / (extrems.dataMax - extrems.dataMin));
                                if (zoomFactor < 0.2) {
                                    event.preventDefault();
                                }
                            },
                            afterSetExtremes: event => {
                                console.log('afterSetExtremes y', event);
                                this.zoomChangedY.emit({
                                    minY: event.min > event.dataMin ? event.min : undefined,
                                    maxY: event.max < event.dataMax ? event.max : undefined
                                });
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.x:.1f} V</b>, <b>{point.y:.2f} mA</b><br/>'
                    },
                    plotOptions: {
                        line: {
                            lineWidth: 4,
                            states: {
                                hover: {
                                    lineWidth: 5
                                }
                            },
                            marker: {
                                enabled: false
                            },
                            pointInterval: 1
                        },
                        series: {
                            states: {
                                inactive: {
                                    opacity: 0.9
                                }
                            },
                            events: {
                                show: (e: unknown) => {
                                    const event = e as ShowEvent;
                                    this.showSerie.emit(event.target);
                                },
                                hide: (e: unknown) => {
                                    const event = e as ShowEvent;
                                    this.hideSerie.emit(event.target);
                                }
                            }
                        }
                    },
                    series: seriesParams.graphSeries
                } as Options;

                if (this.chartInstance) {
                    console.log('Updating Highcharts', options);
                    this.chartInstance.update(options, false, true, false);
                } else {
                    console.log('Creating Highcharts', options);
                }

                refreshSeriesVisibility$.next();

                return options;
            }),
            subscribeWith(addRemoveOrUpdateSeries$, setZoom$, ensureSeriesVisibility$, mouseDown$, manageZoomEnability$),
            shareReplayLast()
        );
    }

    public disableAutoScaling(): void {
        if (!this.chartInstance) {
            return;
        }
        const yAxis = this.chartInstance.yAxis[0];
        const yExtrems = yAxis.getExtremes();
        yAxis.setExtremes(yExtrems.min, yExtrems.max);
        const xAxis = this.chartInstance.xAxis[0];
        const xExtrems = xAxis.getExtremes();
        xAxis.setExtremes(xExtrems.min, xExtrems.max);
    }

    public enableAutoScaling(): void {
        if (!this.chartInstance) {
            return;
        }
        const yAxis = this.chartInstance.yAxis[0];
        yAxis.setExtremes();
        const xAxis = this.chartInstance.xAxis[0];
        xAxis.setExtremes();
    }

    public setZoomEnability(value: boolean): void {
        this.zoomEnabled$.next(value);
    }

    public formatPower(power: number): string {
        let unit = 'W';
        if (power < 1) {
            power = power * 1000;
            unit = 'mW';
        } else if (power > 1000) {
            power = power / 1000;
            unit = 'kW';
        }

        if (power < 10) {
            return `${round(power, 2)} ${unit}`;
        } else if (power < 100) {
            return `${round(power, 1)} ${unit}`;
        } else {
            return `${round(power, 0)} ${unit}`;
        }
    }
}
