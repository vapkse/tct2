import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HighchartsChartModule } from 'highcharts-angular';

import { TranslationModule } from '../../translation/translation.module';
import { TransferGraphComponent } from './transfer-graph.component';

@NgModule({
    declarations: [TransferGraphComponent],
    exports: [TransferGraphComponent],
    imports: [
        CommonModule,
        HighchartsChartModule,
        MatCheckboxModule,
        TranslationModule
    ]
})
export class TransferGraphModule { }
