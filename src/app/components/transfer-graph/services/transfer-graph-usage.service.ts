import { Injectable } from '@angular/core';
import { combineLatestWith, filter, map, Observable, ReplaySubject, startWith, withLatestFrom } from 'rxjs';

import { shareReplayLast } from 'src/app/common/custom-operators';
import { LoadLine } from 'src/app/models/load-line.interface';
import { TubeUsage, TubeUsageMode } from 'src/app/models/tube-usage.model';

import { GraphSeriesOptions, TransferGraphChartService } from './transfer-graph-chart.service';

@Injectable()
export class TransferGraphUsageService {
    public partialUsage$ = new ReplaySubject<Partial<TubeUsage>>(1);

    public aClassLoad$: Observable<LoadLine>;
    public bClassLoad$: Observable<LoadLine>;
    public aClassSerie$: Observable<GraphSeriesOptions>;
    public bClassSerie$: Observable<GraphSeriesOptions>;
    public workingPointSerie$: Observable<GraphSeriesOptions>;
    public hiddenSeries$: Observable<Set<string>>;

    public constructor(
        transferGraphChartService: TransferGraphChartService
    ) {

        this.hiddenSeries$ = this.partialUsage$.pipe(
            filter(usage => usage.series !== undefined),
            map(usage => Object.keys(usage.series).reduce((s, k) => {
                // eslint-disable-next-line @typescript-eslint/no-unnecessary-boolean-literal-compare
                if (usage.series[k] === false) {
                    s.add(k);
                }
                return s;
            }, new Set<string>())),
            startWith(new Set<string>()),
            shareReplayLast()
        );

        this.workingPointSerie$ = this.partialUsage$.pipe(
            filter(usage => usage.va !== undefined && usage.iazero !== undefined),
            map(usage => {
                if (!usage) {
                    return {
                        id: 'working-point'
                    } as GraphSeriesOptions;
                }

                return {
                    type: 'spline',
                    name: 'Working Point',
                    color: 'orange',
                    index: 10,
                    zIndex: 1010,
                    id: 'working-point',
                    className: 'tct-draggable tct-working-point',
                    showInLegend: true,
                    marker: {
                        symbol: 'square',
                        radius: 6,
                        lineWidth: 1
                    },
                    data: [[usage.va, usage.iazero]]
                } as GraphSeriesOptions;
            }),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        this.aClassLoad$ = this.partialUsage$.pipe(
            filter(usage => usage.va !== undefined && usage.iazero !== undefined && usage.mode !== undefined && usage.load !== undefined),
            combineLatestWith(transferGraphChartService.graphSeriesParams$),
            map(([usage, graphSeriesParams]) => {
                if (!usage || !graphSeriesParams) {
                    return undefined;
                }

                const umin = Math.max(0, graphSeriesParams.vmin || 0);
                const imin = Math.max(0, graphSeriesParams.imin || 0);

                const iamax = 0;
                const vamin = 0;
                let vamax = 0;
                let iamin = 0;

                const vazero = usage.va;
                const load = this.getLoadEffectiveZ(usage.mode, usage.load);
                const iazero = usage.iazero;
                if (load && vazero) {
                    vamax = (iazero - imin) * load / 1000 + vazero;
                    iamin = (vazero - umin) * 1000 / load + iazero;
                }

                return {
                    vazero,
                    iazero,
                    vamin,
                    vamax,
                    iamin,
                    iamax,
                    load
                } as LoadLine;
            }),
            shareReplayLast()
        );

        this.aClassSerie$ = this.aClassLoad$.pipe(
            map(aload => {
                if (!aload) {
                    return {
                        id: 'class-a'
                    } as GraphSeriesOptions;
                }

                return {
                    type: 'spline',
                    name: 'Class A',
                    color: 'orange',
                    index: 1,
                    zIndex: 1001,
                    id: 'class-a',
                    className: 'tct-draggable tct-class-a',
                    showInLegend: true,
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 6,
                        lineWidth: 1
                    },
                    data: [[aload.vamin, aload.iamin], [aload.vamax, aload.iamax]]
                } as GraphSeriesOptions;
            }),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        this.bClassLoad$ = this.aClassLoad$.pipe(
            withLatestFrom(this.partialUsage$, transferGraphChartService.graphSeriesParams$),
            map(([aload, usage, graphSeriesParams]) => {
                if (!aload || !usage?.mode || usage.mode === 'se') {
                    return undefined;
                }

                // Clac class B curve
                const ubmax = aload.vazero;
                const ibmax = graphSeriesParams.imin;
                const ubmin = graphSeriesParams.vmin;
                const ibmin = (ubmax - graphSeriesParams.vmin) * 2000 / aload.load;
                if (ibmin <= aload.iamin) {
                    return undefined;
                }

                return {
                    vazero: aload.vazero,
                    iazero: aload.iazero,
                    vamin: ubmin,
                    vamax: ubmax,
                    iamin: ibmin,
                    iamax: ibmax,
                    load: aload.load / 2
                } as LoadLine;
            }),
            shareReplayLast()
        );

        this.bClassSerie$ = this.bClassLoad$.pipe(
            map(bload => {
                if (!bload) {
                    return {
                        id: 'class-b'
                    } as GraphSeriesOptions;
                }

                return {
                    type: 'spline',
                    name: 'Class B',
                    color: 'blue',
                    index: 2,
                    zIndex: 1002,
                    id: 'class-b',
                    showInLegend: true,
                    marker: {
                        symbol: 'circle',
                        radius: 3,
                        lineWidth: 1
                    },
                    data: [[bload.vamin, bload.iamin], [bload.vamax, bload.iamax]]
                } as GraphSeriesOptions;
            }),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );
    }

    public getLoadEffectiveZ(mode: TubeUsageMode, load: number): number {
        // Calc new load param
        switch (mode) {
            case 'se':
                return load;
            case 'pp':
                return load / 2;
            case '2pp':
                return load;
            case '3pp':
                return load * 3 / 2;
            case '4pp':
                return load * 2;
            case '5pp':
                return load * 5 / 2;
            case '6pp':
                return load * 3;
            default:
                return null;
        }
    }

    public getLoad(mode: TubeUsageMode, effectiveZ: number): number {
        // Calc new load param
        switch (mode) {
            case 'se':
                return effectiveZ;
            case 'pp':
                return effectiveZ * 2;
            case '2pp':
                return effectiveZ;
            case '3pp':
                return effectiveZ * 2 / 3;
            case '4pp':
                return effectiveZ / 2;
            case '5pp':
                return effectiveZ * 2 / 5;
            case '6pp':
                return effectiveZ / 3;
            default:
                return null;
        }
    }
}
