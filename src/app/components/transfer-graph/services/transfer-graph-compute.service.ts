import { Injectable } from '@angular/core';
import { Chart, Series } from 'highcharts';
import { combineLatestWith, debounceTime, filter, map, Observable, ReplaySubject, startWith, switchMap, withLatestFrom } from 'rxjs';

import { shareReplayLast } from 'src/app/common/custom-operators';
import { VPoint } from 'src/app/models/v-point.interface';

import { LoadLine } from '../../../models/load-line.interface';
import { TubeUsage } from '../../../models/tube-usage.model';
import { isUndefined } from '../../../services/constants';
import { GraphSeriesOptions } from './transfer-graph-chart.service';
import { TransferGraphUsageService } from './transfer-graph-usage.service';

interface Point {
    va: number;
    ia: number;
}

export interface PowerInfos {
    pmax: number;
    vapp: number;
}

@Injectable()
export class TransferGraphComputeService {
    public chart$ = new ReplaySubject<Chart>(1); // instance of the chart
    public paMax$ = new ReplaySubject<number>(1);
    public aClassSerie$ = new ReplaySubject<Series>(1);

    public crossPointsSerie$: Observable<GraphSeriesOptions>;
    public vg1MinSerie$: Observable<GraphSeriesOptions>;
    public vg1MaxSerie$: Observable<GraphSeriesOptions>;
    public paMaxSerie$: Observable<GraphSeriesOptions>;
    public gain$: Observable<number>;
    public h2$: Observable<number>;
    public h3$: Observable<number>;
    public h4$: Observable<number>;
    public htot$: Observable<number>;
    public powerMax$: Observable<PowerInfos>;
    public transconductance$: Observable<number>;
    public internalResistance$: Observable<number>;
    public crossPoints$: Observable<ReadonlyArray<VPoint>>;

    public constructor(
        transferGraphUsageService: TransferGraphUsageService
    ) {
        const getAbIntersectionPoint = (aload: LoadLine, bload: LoadLine): Point => {
            if (!aload || !bload) {
                return undefined;
            }

            const d = ((bload.iamax - bload.iamin) * (aload.vamax - aload.vamin)) - ((bload.vamax - bload.vamin) * (aload.iamax - aload.iamin));
            if (d === 0) {
                return undefined;
            }

            const a = aload.iamin - bload.iamin;
            const b = aload.vamin - bload.vamin;
            const aa = ((bload.vamax - bload.vamin) * a - (bload.iamax - bload.iamin) * b) / d;
            if (aa <= 0 || aa > 1) {
                return undefined;
            }

            const bb = ((aload.vamax - aload.vamin) * a - (aload.iamax - aload.iamin) * b) / d;
            if (bb <= 0 || bb > 1) {
                return undefined;
            }

            return {
                va: aload.vamin + (aa * (aload.vamax - aload.vamin)),
                ia: aload.iamin + (aa * (aload.iamax - aload.iamin))
            };
        };

        const chart$ = this.chart$.pipe(
            filter(Boolean),
            debounceTime(100), // Ensure series
            shareReplayLast()
        );

        const crossPoints$ = chart$.pipe(
            switchMap(chart => transferGraphUsageService.partialUsage$.pipe(
                withLatestFrom(transferGraphUsageService.aClassLoad$, transferGraphUsageService.bClassLoad$),
                filter(([usage]) => usage.va !== undefined && usage.iazero !== undefined && usage.mode !== undefined && usage.load !== undefined && usage.vinpp !== undefined),
                map(([usage, aClassLoad, bClassLoad]) => {
                    const abIntersectionPoint = getAbIntersectionPoint(aClassLoad, bClassLoad);
                    const seriesMap = chart.series.reduce((m, serie) => m.set(serie.options.id, serie), new Map<string, Series>());
                    const aClassSerie = seriesMap.get('class-a');

                    const graphSeries = chart.series
                        .filter(s => {
                            const options = s.options as GraphSeriesOptions;
                            return options.vg1 !== undefined;
                        });

                    graphSeries.sort((s1, s2) => (s1.options as GraphSeriesOptions).vg1 - (s2.options as GraphSeriesOptions).vg1);

                    const firstVg1SerieId = graphSeries
                        .map(s => s.options as GraphSeriesOptions)
                        .reduce((first, options) => {
                            if (!first || options.vg1 < first.vg1) {
                                return options;
                            } else {
                                return first;
                            }
                        }, undefined as GraphSeriesOptions);


                    if (!aClassSerie || !usage?.load || isUndefined(usage.va)) {
                        return [new Array<VPoint>(), usage] as const;
                    }

                    // Working point and load
                    const load = transferGraphUsageService.getLoadEffectiveZ(usage.mode, usage.load);

                    // A coordonates in units
                    const vamin = 0;
                    const vamax = usage.va + load * usage.iazero / 1000;
                    const iamin = usage.iazero + 1000 * usage.va / load;
                    const iamax = 0;

                    // B load and coordonates in unit
                    const bload = load / 2;
                    const vbmin = 0;
                    const vbmax = usage.va;
                    const ibmin = (usage.va - vbmin) * 1000 / bload;
                    const ibmax = 0;

                    // Pixels ab coordonates and crossing point
                    const a1x = aClassSerie.xAxis.toPixels(vamin, true);
                    const a2x = aClassSerie.xAxis.toPixels(vamax, true);
                    const a1y = aClassSerie.yAxis.toPixels(iamin, true);
                    const a2y = aClassSerie.yAxis.toPixels(iamax, true);
                    let classA = true;
                    let limitedRange = false; // If last curve are hidden, calculation is stopped at the last visible one

                    // If class B serie, calc cross point with class A
                    // B coordonate and crossing point in pixels
                    const bClassSerie = seriesMap.get('class-b');
                    const b1x = abIntersectionPoint && bClassSerie?.xAxis.toPixels(vbmin, true);
                    const b2x = abIntersectionPoint && bClassSerie?.xAxis.toPixels(vbmax, true);
                    const b1y = abIntersectionPoint && bClassSerie?.yAxis.toPixels(ibmin, true);
                    const b2y = abIntersectionPoint && bClassSerie?.yAxis.toPixels(ibmax, true);
                    const abx = abIntersectionPoint && bClassSerie?.xAxis.toPixels(abIntersectionPoint.va, true);

                    const calcOutOfRangePoints = (points: Array<VPoint>): void => {
                        let va = points[0].va;
                        const va0 = (vamax - va);
                        const ia0 = points[0].ia;
                        let vg = points[0].vg1;
                        const vgstep = -points[0].vg1 + points[1].vg1;
                        const vastep = va0 / 20;
                        let next = va;

                        // eslint-disable-next-line no-loops/no-loops
                        while (next < vamax) {
                            // Add a last point
                            va = next;
                            next = Math.min(vamax, next + (vamax - va) / 2 + vastep);
                            const ia = ia0 * (vamax - next) / va0;
                            vg -= vgstep;
                            points.splice(0, 0, {
                                vg1: vg,
                                va: next,
                                ia: ia,
                                classA: true,
                                overflow: false
                            });
                        }
                    };

                    const calcMorePoints = (points: Array<VPoint>, maxCorrectionFactor: number): void => {
                        const dv = [] as Array<VPoint>;
                        const bckp = [] as Array<VPoint>;
                        // calc distance between points
                        const length = points.length;
                        // eslint-disable-next-line no-loops/no-loops
                        for (let i = 0; i < length - 1; i++) {
                            dv.push({
                                va: points[i + 1].va - points[i].va,
                                ia: 0,
                                vg1: points[i + 1].vg1 - points[i].vg1,
                                classA: undefined
                            });
                            bckp.push(points[i]);
                        }
                        bckp.push(points[length - 1]);

                        let insertpoint = 1;
                        // eslint-disable-next-line no-loops/no-loops
                        for (let i = 0; i < length - 1; i++) {
                            // Correcteur
                            let m = 1;
                            let p = 1;
                            let ratio = 1;

                            if (i > 0) {
                                m = p;
                            }
                            if (i < dv.length - 1) {
                                p = Math.sqrt(Math.sqrt(dv[i].va * dv[i + 1].vg1 / (dv[i].vg1 * dv[i + 1].va)));
                                ratio = p / m;
                                // Pentode in the vertical lines, forget the correction
                                if (isNaN(ratio)) {
                                    ratio = 1;
                                    p = 1;
                                } else if (ratio > (1 + maxCorrectionFactor)) {
                                    ratio = 1 + maxCorrectionFactor;
                                    p = ratio / m;
                                } else if (ratio < (1 - maxCorrectionFactor)) {
                                    ratio = 1 - maxCorrectionFactor;
                                    p = ratio / m;
                                }
                            }

                            const vg1 = bckp[i].vg1 + (bckp[i + 1].vg1 - bckp[i].vg1) / 2;
                            const x = bckp[i].va + (dv[i].va / 2) * ratio;
                            const bclass = abIntersectionPoint && x <= abIntersectionPoint.va;
                            const y = bclass ? ibmin * (vbmax - x) / vbmax : iamin * (vamax - x) / vamax;
                            points.splice(insertpoint, 0, {
                                va: x,
                                ia: y,
                                vg1: vg1,
                                classA: !bclass
                            });
                            insertpoint += 2;
                        }
                    };

                    const calcLineIntersection = (ax1: number, ay1: number, ax2: number, ay2: number, bx1: number, by1: number, bx2: number, by2: number): Point => {
                        // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans for whether line segment 1 or line segment 2 contain the point
                        const d = ((by2 - by1) * (ax2 - ax1)) - ((bx2 - bx1) * (ay2 - ay1));
                        if (d === 0) {
                            return undefined;
                        }

                        const a = ay1 - by1;
                        const b = ax1 - bx1;
                        const aa = ((bx2 - bx1) * a - (by2 - by1) * b) / d;
                        if (aa <= 0 || aa > 1) {
                            return undefined;
                        }

                        const bb = ((ax2 - ax1) * a - (ay2 - ay1) * b) / d;
                        if (bb <= 0 || bb > 1) {
                            return undefined;
                        }

                        return {
                            va: ax1 + (aa * (ax2 - ax1)),
                            ia: ay1 + (aa * (ay2 - ay1))
                        } as Point;
                    };

                    const calcSerie = (serie: Series): Array<VPoint> => {
                        const data = new Array<VPoint>();
                        const serieOptions = serie.options as GraphSeriesOptions;
                        const path = serie.graph?.element as SVGGeometryElement;
                        if (!serie.visible || !path) {
                            if (serieOptions.id === firstVg1SerieId.id) {
                                limitedRange = true;
                            }

                            return data;
                        }

                        const length = path.getTotalLength();
                        const endOffset = length / 10000;
                        let tmOffset = length / 50;
                        let cm: [number, number];
                        let c1 = path.getPointAtLength(0);
                        let c2 = path.getPointAtLength(length);
                        let l1 = 0;
                        let l2 = length;

                        const calcPointAtLength = (): void => {
                            const lmid = l1 + (l2 - l1) / 2;
                            const cmid = path.getPointAtLength(lmid);
                            let x1: number;
                            let x2: number;
                            let y1: number;
                            let y2: number;

                            if (classA) {
                                x1 = a1x;
                                y1 = a1y;
                                x2 = a2x;
                                y2 = a2y;
                            } else {
                                x1 = b1x;
                                y1 = b1y;
                                x2 = b2x;
                                y2 = b2y;
                            }

                            const i1 = calcLineIntersection(x1, y1, x2, y2, c1.x, c1.y, cmid.x, cmid.y);
                            if (i1) {
                                l2 = lmid;
                            } else {
                                const i2 = calcLineIntersection(x1, y1, x2, y2, cmid.x, cmid.y, c2.x, c2.y);
                                if (i2) {
                                    l1 = lmid;
                                } else {
                                    return;
                                }
                            }

                            const offset = Math.abs(l2 - l1);
                            if (offset < tmOffset) {
                                tmOffset = 0;
                                cm = [cmid.x, cmid.y];
                            }

                            if (offset > endOffset) {
                                calcPointAtLength();
                                return;
                            } else if (classA && cmid.x <= abx) {
                                // Reste
                                classA = false;
                                l1 = 0;
                                l2 = length;
                                c1 = path.getPointAtLength(0);
                                c2 = path.getPointAtLength(length);
                                // Occurs again for classB
                                calcPointAtLength();
                                return;
                            } else {
                                data.push({
                                    va: serie.xAxis.toValue(cmid.x, true),
                                    ia: serie.yAxis.toValue(cmid.y, true),
                                    vg1: serieOptions.vg1,
                                    classA: classA,
                                    // Add point for internal resistance calculation
                                    vt: cm && serie.xAxis.toValue(cm[0], true),
                                    it: cm && serie.yAxis.toValue(cm[1], true)
                                });
                                // console.log(serie.name + ' ' + n + ' iterations');
                                return;
                            }
                        };

                        calcPointAtLength();
                        return data;
                    };

                    const points = graphSeries.reduce((data, serie) => [...data, ...calcSerie(serie)], new Array<VPoint>());

                    if (!limitedRange) {
                        if (points.length < 2) {
                            limitedRange = true;
                        } else {
                            // Because two last curves are not hidden, interpolate the vg1 points until x axis
                            calcOutOfRangePoints(points);
                        }
                    }
                    calcMorePoints(points, 0.1);
                    calcMorePoints(points, 0.2);
                    calcMorePoints(points, 0.3);
                    calcMorePoints(points, 0.4);
                    calcMorePoints(points, 0.5);
                    return [points, usage, abIntersectionPoint, aClassLoad] as const;
                })
            )),
            shareReplayLast()
        );

        this.crossPoints$ = crossPoints$.pipe(
            map(([crossPoints]) => crossPoints),
            shareReplayLast()
        );

        this.crossPointsSerie$ = crossPoints$.pipe(
            filter(([crossPoints]) => crossPoints.length > 0),
            map(([crossPoints]) => {
                const points = crossPoints.map(crossPoint => ([crossPoint.va, crossPoint.ia]));
                return {
                    type: 'spline',
                    lineWidth: 2,
                    name: 'ComputePoints',
                    data: points,
                    color: '#e91e63',
                    index: 3,
                    zIndex: 1003,
                    id: 'compute-points',
                    opacity: 0.7,
                    dashStyle: 'Dot',
                    visible: true
                } as GraphSeriesOptions;
            }),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        const workingPoint$ = crossPoints$.pipe(
            map(([crossPoints, usage, abIntersectionPoint, aClassLoad]) => {
                const workingPoint = crossPoints.length && this.calcPointFromVa(usage.va, true, crossPoints);
                return [workingPoint, crossPoints, usage, abIntersectionPoint, aClassLoad] as const;
            }),
            shareReplayLast()
        );

        const vg1MaxPoint$ = workingPoint$.pipe(
            filter(([workingPoint, crossPoints]) => workingPoint && crossPoints.length > 0),
            map(([workingPoint, crossPoints, usage]) => this.calcPointFromVg1(workingPoint.vg1 + usage.vinpp / 2, crossPoints)),
            shareReplayLast()
        );

        this.vg1MaxSerie$ = vg1MaxPoint$.pipe(
            map(vg1Max => ({
                type: 'spline',
                name: 'vg1-max',
                color: vg1Max.overflow ? 'red' : 'green',
                data: [[vg1Max.va, 0], [vg1Max.va, vg1Max.ia]],
                lineWidth: 1,
                index: 105,
                zIndex: 1005,
                id: 'vg1-max',
                className: 'tct-draggable tct-vg1-max',
                showInLegend: false,
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 6
                }
            } as GraphSeriesOptions)),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        const vg1MinPoint$ = vg1MaxPoint$.pipe(
            withLatestFrom(workingPoint$),
            map(([vg1MaxPoint, [workingPoint, crossPoints, usage, _abIntersectionPoint, aClassLoad]]) => {
                const vg1Min = this.calcPointFromVg1(workingPoint.vg1 - usage.vinpp / 2, crossPoints);
                if (vg1Min.overflow && !vg1MaxPoint.classA) {
                    return {
                        vg1: undefined,
                        va: aClassLoad.vamax,
                        ia: 0,
                        classA: false,
                        overflow: true
                    } as VPoint;
                } else {
                    return vg1Min;
                }
            }),
            shareReplayLast()
        );

        this.vg1MinSerie$ = vg1MinPoint$.pipe(
            map(vg1Min => ({
                type: 'spline',
                name: 'vg1-min',
                color: vg1Min.overflow ? 'red' : 'green',
                data: vg1Min.overflow ? [[vg1Min.va, 0]] : [[vg1Min.va, 0], [vg1Min.va, vg1Min.ia]],
                lineWidth: 1,
                index: 106,
                zIndex: 1006,
                id: 'vg1-min',
                className: 'tct-draggable tct-vg1-min',
                showInLegend: false,
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 6
                }
            } as GraphSeriesOptions)),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        const maxAxisPoint$ = chart$.pipe(
            map(chart => chart.series.reduce((point, s) => {
                const serie = s.options as GraphSeriesOptions;
                // eslint-disable-next-line @typescript-eslint/no-unnecessary-boolean-literal-compare
                if (serie.vg1 !== undefined && serie.visible !== false) {
                    if (serie.umax > point.va) {
                        point.va = serie.umax;
                    }
                    if (serie.imax > point.ia) {
                        point.ia = serie.imax;
                    }
                }
                return point;
            }, { va: 0, ia: 0 } as Point))
        );

        const partialUsage$ = transferGraphUsageService.partialUsage$.pipe(
            startWith(undefined as Partial<TubeUsage>),
            debounceTime(10)
        );

        this.paMaxSerie$ = this.paMax$.pipe(
            filter(Boolean),
            combineLatestWith(maxAxisPoint$, partialUsage$), // partialUsage$ is used to trigger the observable in case of chart is modified
            map(([pmaxW, maxAxisPoint]) => {
                const pmax = pmaxW && pmaxW * 1000; // En mW
                const maxAxis = pmax ? maxAxisPoint : { va: 0, ia: 0 } as Point;
                const pamax = new Array<[number, number]>();
                if (maxAxis.ia > 0) {
                    let v = pmax / maxAxis.ia;
                    const step = (maxAxis.va - v) / 50;
                    const last = maxAxis.va + step;
                    // eslint-disable-next-line no-loops/no-loops
                    while (v <= last) {
                        pamax.push([v, pmax / v]);
                        v += step;
                    }
                }

                return [pamax, pmaxW] as const;
            }),
            filter(pamax => pamax.length > 0),
            map(([pamax, pmaxW]) => ({
                type: 'spline',
                lineWidth: 2,
                name: `pmax (${pmaxW}W)`,
                color: 'red',
                index: 107,
                zIndex: 999,
                data: pamax,
                showInLegend: true,
                id: 'pamax',
                marker: {
                    enabled: false
                }
            } as GraphSeriesOptions)),
            startWith(undefined as GraphSeriesOptions),
            shareReplayLast()
        );

        this.gain$ = vg1MinPoint$.pipe(
            combineLatestWith(vg1MaxPoint$),
            map(([vg1MinPoint, vg1MaxPoint]) => vg1MinPoint && vg1MaxPoint && (vg1MinPoint.va - vg1MaxPoint.va) / (vg1MaxPoint.vg1 - vg1MinPoint.vg1)),
            startWith(undefined as number),
            shareReplayLast()
        );

        const hbase$ = workingPoint$.pipe(
            combineLatestWith(vg1MinPoint$, vg1MaxPoint$),
            map(([[workingPoint, crossPoints, usage], vg1MinPoint, vg1MaxPoint]) => {
                if (usage.mode === 'se') {
                    const vgMin = workingPoint.vg1 - (workingPoint.vg1 - vg1MinPoint.vg1) / 2;
                    const vgMax = workingPoint.vg1 - (workingPoint.vg1 - vg1MaxPoint.vg1) / 2;
                    const ptMin = this.calcPointFromVg1(vgMin, crossPoints);
                    const ptMax = this.calcPointFromVg1(vgMax, crossPoints);
                    const iaMin = vg1MinPoint.ia;
                    const iaMax = vg1MaxPoint.ia;
                    const div = iaMax + ptMax.ia - ptMin.ia - iaMin;
                    return { singleEnded: true, iaMin, iaMax, div, ptMin, ptMax, wpIa: workingPoint.ia };
                } else {
                    const vgm = workingPoint.vg1 - (workingPoint.vg1 - vg1MaxPoint.vg1) / 2;
                    const ptm = this.calcPointFromVg1(vgm, crossPoints);
                    const ia = vg1MaxPoint.ia - workingPoint.ia;
                    const ib = ptm.ia - workingPoint.ia;
                    const div = 2 * (vg1MaxPoint.ia + ptm.ia);
                    return { singleEnded: false, ia, ib, div };
                }
            }),
            shareReplayLast()
        );

        this.h2$ = hbase$.pipe(
            map(params => {
                if (params.singleEnded) {
                    return Math.abs(75 * (params.iaMax - 2 * params.wpIa + params.iaMin) / params.div);
                }
                return 0;
            }),
            startWith(undefined as number),
            shareReplayLast()
        );

        this.h3$ = hbase$.pipe(
            map(params => {
                if (params.singleEnded) {
                    return Math.abs(50 * (params.iaMax - 2 * params.ptMax.ia + 2 * params.ptMin.ia - params.iaMin) / params.div);
                }
                return Math.abs(50 * (2 * params.ia - 4 * params.ib) / params.div);
            }),
            startWith(undefined as number),
            shareReplayLast()
        );

        this.h4$ = hbase$.pipe(
            map(params => {
                if (params.singleEnded) {
                    return Math.abs(25 * (params.iaMax - 4 * params.ptMax.ia + 6 * params.wpIa - 4 * params.ptMin.ia + params.iaMin) / params.div);
                }
                return 0;
            }),
            startWith(undefined as number),
            shareReplayLast()
        );

        this.htot$ = this.h2$.pipe(
            combineLatestWith(this.h3$, this.h4$),
            debounceTime(1),
            map(hall => hall.reduce((htot, h) => htot + h, 0)),
            startWith(undefined as number),
            shareReplayLast()
        );

        this.powerMax$ = workingPoint$.pipe(
            combineLatestWith(vg1MinPoint$, vg1MaxPoint$),
            map(([[workingPoint, _crossPoints, usage, abIntersectionPoint], vg1MinPoint, vg1MaxPoint]) => {
                // Calc pmax with ui methode
                /* const calcPmaxWithUIMethod = (): PowerInfos => {
                    let pmax = 0;
                    let vapp = 0;
                    let iap = 0;
                    if (usage.mode === 'se') {
                        vapp = vg1MaxPoint && vg1MinPoint && (vg1MaxPoint.va - vg1MinPoint.va) / 2;
                        iap = vg1MaxPoint && vg1MinPoint && (vg1MinPoint.ia - vg1MaxPoint.ia) / 2;
                        pmax = vapp && iap && vapp * iap / 2000;
                    } else {
                        vapp = vg1MaxPoint && workingPoint.va - vg1MaxPoint.va;
                        iap = vg1MaxPoint && vg1MaxPoint.ia - workingPoint.ia;
                        pmax = vapp && iap && vapp * iap / 2000;
                        // Calc new load param
                        switch (usage.mode) {
                            case '2pp':
                                pmax *= 2;
                                break;
                            case '3pp':
                                pmax *= 3;
                                break;
                            case '4pp':
                                pmax *= 4;
                                break;
                            case '5pp':
                                pmax *= 5;
                                break;
                            case '6pp':
                                pmax *= 6;
                                break;
                            default:
                        }
                    }

                    return { pmax, vapp };
                }; */

                // Calc pmax with ui2 methode
                /* const calcPmaxWithUI2Method = (): PowerInfos => {
                    let pmax = 0;
                    let vapp = 0;
                    if (vg1MaxPoint && vg1MinPoint) {
                        vapp = vg1MinPoint.va - vg1MaxPoint.va;
                        const iapp = vg1MaxPoint.ia - vg1MinPoint.ia;
                        pmax = vapp * iapp / 8000;
                    }
                    if (usage.mode !== 'se') {
                        pmax *= 2;
                    }

                    return { pmax, vapp };
                }; */

                // Calc pmax with z1 methode
                const calcPmaxWithZ1Method = (): PowerInfos => {
                    let pmax = 0;
                    let vapp = 0;
                    let vp = 0;
                    // Calc new load param
                    if (vg1MaxPoint && vg1MinPoint) {
                        vp = vg1MinPoint.va - vg1MaxPoint.va;
                        if (usage.mode !== 'se') {
                            vp *= 2;
                            const vcross = abIntersectionPoint && abIntersectionPoint.va - 0.15 * abIntersectionPoint.va;
                            if (vcross && vcross <= vg1MinPoint.va && vcross >= vg1MaxPoint.va) {
                                const vaa = vg1MinPoint.va - vcross;
                                const vab = vcross - vg1MaxPoint.va;
                                vp = (vaa + vab * 2) * 2;
                            }
                        }
                    }
                    pmax = usage.load && vp && vp * vp / 8 / usage.load;

                    // Calc voltage peak-peak anode-anode
                    if (vg1MaxPoint) {
                        if (usage.mode === 'se') {
                            vapp = vp;
                        } else {
                            vapp = 2 * (workingPoint.va - vg1MaxPoint.va);
                        }
                    }

                    return { pmax, vapp };
                };

                // Calc pmax with z2 methode
                /* const calcPmaxWithZ2Method = (): PowerInfos => {
                    let pmax = 0;
                    let vapp = 0;
                    if (usage.mode === 'se') {
                        if (vg1MaxPoint && vg1MinPoint) {
                            vapp = vg1MinPoint.va - vg1MaxPoint.va;
                            pmax = usage.load && vapp * vapp / 8 / usage.load;
                        }
                    } else {
                        let vaa = 0;
                        let vab = 0;

                        // Calc new load param
                        if (vg1MaxPoint && vg1MinPoint) {
                            if (abIntersectionPoint && abIntersectionPoint.va <= vg1MinPoint.va && abIntersectionPoint.va >= vg1MaxPoint.va) {
                                vaa = vg1MinPoint.va - abIntersectionPoint.va;
                                vab = abIntersectionPoint.va - vg1MaxPoint.va;
                            } else {
                                vaa = vg1MinPoint.va - vg1MaxPoint.va;
                                vab = 0;
                            }
                        }

                        vapp = 2 * (vaa + vab);
                        const load = transferGraphUsageService.getLoadEffectiveZ(usage.mode, usage.load);
                        const pa = load && vaa * vaa / load;
                        const pb = load && 2 * vab * vab / load;
                        pmax = pa + pb;
                    }

                    return { pmax, vapp };
                }; */

                return calcPmaxWithZ1Method();
            }),
            startWith(undefined as PowerInfos),
            shareReplayLast()
        );

        const pointForTransconductance$ = workingPoint$.pipe(
            filter(([workingPoint, crossPoints]) => workingPoint && crossPoints.length > 0),
            map(([workingPoint, crossPoints]) => {
                const pts = this.getNearestTrueCrossingLinePoints(workingPoint.va, crossPoints);
                if (!pts.p1) {
                    return undefined;
                }

                // Calc transconductance at point 1
                const op1 = pts.isLast && !pts.p2 ? -1 : 1;
                const pta = this.calcPointFromVa(pts.p1.va + op1 * Math.abs(pts.p1.vt - pts.p1.va), true, crossPoints);
                if (!pta) {
                    return undefined;
                }
                return [pts, pta, workingPoint, crossPoints] as const;
            }),
            shareReplayLast()
        );

        this.transconductance$ = pointForTransconductance$.pipe(
            map(params => {
                if (!params) {
                    return undefined as number;
                }
                const [pts, pta, workingPoint, crossPoints] = params;
                let s = Math.abs((Math.abs(pts.p1.it - pts.p1.ia) + Math.abs(pts.p1.ia - pta.ia)) / (pts.p1.vg1 - pta.vg1));
                const op2 = pts.isLast ? -1 : 1;
                const ptb = pts.p2 && this.calcPointFromVa(pts.p2.va + op2 * Math.abs(pts.p2.vt - pts.p2.va), true, crossPoints);
                if (ptb) {
                    const s2 = Math.abs((Math.abs(pts.p2.it - pts.p2.ia) + Math.abs(pts.p2.ia - ptb.ia)) / (pts.p2.vg1 - ptb.vg1));
                    s = s - (s - s2) * (workingPoint.va - pts.p1.va) / (pts.p2.va - pts.p1.va);
                }

                return s;
            }),
            startWith(undefined as number),
            shareReplayLast()
        );

        this.internalResistance$ = pointForTransconductance$.pipe(
            map(params => {
                if (!params) {
                    return undefined as number;
                }
                const [pts, , workingPoint, crossPoints] = params;
                let ri = Math.abs((pts.p1.vt - pts.p1.va) / (pts.p1.it - pts.p1.ia));
                const op = pts.isLast ? -1 : 1;
                const ptb = pts.p2 && this.calcPointFromVa(pts.p2.va + op * Math.abs(pts.p2.vt - pts.p2.va), true, crossPoints);
                if (ptb) {
                    const ri2 = Math.abs((pts.p2.vt - pts.p2.va) / (pts.p2.it - pts.p2.ia));
                    ri = ri - (ri - ri2) * (workingPoint.va - pts.p1.va) / (pts.p2.va - pts.p1.va);
                }

                return ri;
            }),
            startWith(undefined as number),
            shareReplayLast()
        );
    }

    public calcPointFromVa(u: number, nearest: boolean, crossPoints: ReadonlyArray<VPoint>): VPoint {
        const point = {
            va: u,
            ia: null,
            vg1: null,
            overflow: false,
            classA: true
        } as VPoint;

        let p0: VPoint;
        let p1: VPoint;
        if (nearest) {
            p0 = p1 = crossPoints[0];
            crossPoints.find(p => {
                if (p.va < u) {
                    p1 = p;
                    return true;
                }
                p0 = p1 = p;
                return false;
            });
        } else {
            let i = crossPoints.length;
            p0 = p1 = crossPoints[i - 1];
            // eslint-disable-next-line no-loops/no-loops
            while (--i >= 0) {
                const p = crossPoints[i];
                if (p.va > u) {
                    p0 = p;
                    break;
                }
                p0 = p1 = p;
            }
        }

        if (p1 && p0) {
            // Calc best point between p0 and p1
            point.vg1 = p0.vg1;
            point.ia = p0.ia;
            point.classA = p1.classA;
            if (p0.va !== p1.va) {
                point.vg1 += (u - p0.va) * (p1.vg1 - p0.vg1) / (p1.va - p0.va);
                point.ia -= (p0.ia - p1.ia) * (u - p0.va) / (p1.va - p0.va);
            } else {
                point.overflow = true;
            }
        }

        return point;
    }

    public calcPointFromVg1(vg1: number, crossPoints: VPoint[]): VPoint {
        const point = {
            va: undefined,
            ia: undefined,
            vg1: vg1,
            overflow: false,
            classA: true
        } as VPoint;

        let p0: VPoint;
        let p1: VPoint;
        p0 = p1 = crossPoints[0];
        crossPoints.find(p => {
            if (p.vg1 > vg1) {
                p1 = p;
                return true;
            }
            p0 = p1 = p;
            return false;
        });

        if (p1 && p0) {
            point.va = p0.va;
            point.ia = p0.ia;
            point.classA = p1.classA;
            if (p0.vg1 !== p1.vg1) {
                // Calc best point between p0 and p1
                point.va += (vg1 - p0.vg1) * (p1.va - p0.va) / (p1.vg1 - p0.vg1);
                point.ia -= (p0.ia - p1.ia) * (point.va - p0.va) / (p1.va - p0.va);
            } else {
                point.overflow = true;
            }
        }

        return point;
    }

    public getNearestTrueCrossingLinePoints(va: number, crossPoints: VPoint[]): { p1?: VPoint; p2?: VPoint; isLast?: boolean } {
        let i = -1;
        let n = 0;
        let last: VPoint;
        // eslint-disable-next-line no-loops/no-loops
        while (++i < crossPoints.length) {
            const p = crossPoints[i];
            if (p.vt && p.it) {
                if (p.va === va) {
                    return {
                        p1: p,
                        isLast: n === 0
                    };
                } else if (p.va < va) {
                    return {
                        p1: p,
                        p2: last,
                        isLast: n <= 1
                    };
                }
                last = p;
                n++;
            }
        }

        return {};
    }
}
