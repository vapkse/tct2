import { Injectable } from '@angular/core';
import { SeriesSplineOptions } from 'highcharts';
import { combineLatestWith, map, Observable, ReplaySubject } from 'rxjs';

import { shareReplayLast } from 'src/app/common/custom-operators';
import { TubeGraph } from 'src/app/models/tube-graph.model';
import { ThemeService } from 'src/app/services/theme.service';

export interface GraphSeriesOptions extends SeriesSplineOptions {
    vg1?: number;
    umax?: number;
    imax?: number;
}

export interface GraphSeriesParams {
    graphSeries: GraphSeriesOptions[];
    vmin: number;
    vmax: number;
    imin: number;
    imax: number;
}

@Injectable()
export class TransferGraphChartService {
    public graph$ = new ReplaySubject<TubeGraph>(1);

    public graphSeriesParams$: Observable<GraphSeriesParams>;

    public constructor(
        themeService: ThemeService
    ) {
        this.graphSeriesParams$ = this.graph$.pipe(
            combineLatestWith(themeService.isDark$, themeService.materialPallets$),
            map(([graph, isDark, materialPallets]) => {
                // Order is important, class A first
                const curves = graph.c;
                curves?.sort((a, b) => a.vg1 - b.vg1);

                let imax = 0;
                let vmax = 0;
                let imin = undefined as number;
                let vmin = undefined as number;
                const graphSeries = curves.map((curve, index) => {
                    const points = curve.p.map(point => {
                        const i = point.ik * 1000;
                        if (vmax < point.va) {
                            vmax = point.va;
                        }
                        if (imax < i) {
                            imax = i;
                        }
                        if (vmin === undefined || vmin > point.va) {
                            vmin = point.va;
                        }
                        if (imin === undefined || imin > i) {
                            imin = i;
                        }
                        return [point.va, i];
                    });

                    const name = `${curve.vg1}V`;
                    const id = String(curve.vg1);
                    return {
                        type: 'spline',
                        lineWidth: 2,
                        name,
                        vg1: curve.vg1,
                        data: points,
                        umax: vmax,
                        imax,
                        color: materialPallets[index % materialPallets.length][isDark ? '500' : '800'],
                        index: index + 1000,
                        zIndex: index,
                        id,
                        visible: true
                    } as GraphSeriesOptions;
                });

                return { graphSeries, vmin, vmax, imin, imax };
            }),
            shareReplayLast()
        );
    }
}
