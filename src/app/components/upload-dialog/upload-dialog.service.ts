import { Injectable, Type } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogService } from 'src/app/common/lazy-loading/dialog.service';
import { AbstractLazyModule, LazyLoaderService } from 'src/app/common/lazy-loading/lazy-loader.service';

import { DialogConfig } from '../../common/lazy-loading/dialog.service';
import { UploadData } from '../upload/upload.model';

@Injectable({
    providedIn: 'root'
})
export class UploadDialogService extends DialogService<string, UploadData> {
    public constructor(
        lazyLoaderService: LazyLoaderService,
        dialog: MatDialog
    ) {
        const dialogConfig = new DialogConfig<UploadData>();
        dialogConfig.disableClose = true;

        super(lazyLoaderService, dialog, dialogConfig);
    }

    protected getModule(): Promise<Type<AbstractLazyModule<unknown>>> {
        return import('./upload-dialog.module').then(m => m.UploadDialogModule);
    }
}
