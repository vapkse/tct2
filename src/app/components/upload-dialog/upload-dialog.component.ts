import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { UploadData } from '../upload/upload.model';

@Component({
    selector: 'app-upload-dialog',
    templateUrl: './upload-dialog.component.html',
    styleUrls: ['./upload-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploadDialogComponent {
    public constructor(
        @Inject(MAT_DIALOG_DATA) public data: UploadData,
        public dialogRef: MatDialogRef<UploadDialogComponent, string>
    ) { }
}
