import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AbstractLazyModule } from 'src/app/common/lazy-loading';

import { TranslationModule } from '../../translation/translation.module';
import { UploadModule } from '../upload/upload.module';
import { UploadDialogComponent } from './upload-dialog.component';

@NgModule({
    declarations: [UploadDialogComponent],
    exports: [UploadDialogComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        MatIconModule,
        MatToolbarModule,
        TranslationModule,
        UploadModule
    ]
})
export class UploadDialogModule extends AbstractLazyModule<UploadDialogComponent> {
    public constructor() {
        super(UploadDialogComponent);
    }
}
