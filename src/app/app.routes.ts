import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth/auth-guard.service';
import { TranslationGuardService } from './translation/translation-guard.service';


export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'sign-in', loadChildren: (): Promise<unknown> => import('./sign-in/sign-in.module').then(m => m.SignInModule), canActivate: [TranslationGuardService] },
    { path: 'home', loadChildren: (): Promise<unknown> => import('./home/home.module').then(m => m.HomeModule), canActivate: [TranslationGuardService] },
    { path: 'view', loadChildren: (): Promise<unknown> => import('./viewer/viewer.module').then(m => m.ViewerModule), canActivate: [TranslationGuardService] },
    { path: 'edit', loadChildren: (): Promise<unknown> => import('./editor/editor.module').then(m => m.EditorModule), canActivate: [AuthGuardService, TranslationGuardService] },
    { path: 'creator', loadChildren: (): Promise<unknown> => import('./creator/creator.module').then(m => m.CreatorModule), canActivate: [AuthGuardService, TranslationGuardService] },
    { path: 'waiter', loadChildren: (): Promise<unknown> => import('./waiter/waiter.module').then(m => m.WaiterModule) },
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(routes, { useHash: false, relativeLinkResolution: 'legacy' });
