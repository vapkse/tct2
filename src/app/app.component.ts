import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

import { ThemeService } from './services/theme.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
    public constructor(
        public themeService: ThemeService
    ) { }
}
