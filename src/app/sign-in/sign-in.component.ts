import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { IFormBuilder, IFormGroup } from '@rxweb/types';
import { UserCredential } from 'firebase/auth';
import { catchError, debounceTime, delay, filter, Observable, of, Subject, switchMap, takeUntil, tap, timeout } from 'rxjs';

import { asyncSwitchMap } from '../common/custom-operators';
import { DestroyDirective } from '../common/destroy/destroy.directive';
import { AuthProvider, AuthService } from '../services/auth/auth.service';
import { ErrorService } from '../services/error.service';

interface LoginForm {
    email: string;
    password: string;
    password2: string;
}

type LoginFormType = 'login' | 'signin' | 'reset';

@Component({
    selector: 'app-sign-in',
    styleUrls: ['./sign-in.component.scss'],
    templateUrl: './sign-in.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class SignInComponent extends DestroyDirective {
    @ViewChild('ngForm')
    public ngForm: NgForm;

    public submit$ = new Subject<void>();
    public loginForm: IFormGroup<LoginForm>;

    private _loginFormType = 'login' as LoginFormType;

    public set loginFormType(value: LoginFormType) {
        if (this._loginFormType !== value) {
            this._loginFormType = value;

            if (this._loginFormType === 'reset') {
                this.loginForm.controls.password.clearValidators();
            } else {
                this.loginForm.controls.password.setValidators(Validators.required);
            }
            this.ngForm.resetForm();
            this.loginForm.reset();
        }
    }

    public get loginFormType(): LoginFormType {
        return this._loginFormType;
    }

    public constructor(
        public errorService: ErrorService,
        private authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        formBuilder: FormBuilder,
        changeDetectorRef: ChangeDetectorRef
    ) {
        super();

        /** ICONS **/
        iconRegistry.addSvgIcon(
            'google',
            sanitizer.bypassSecurityTrustResourceUrl('assets/icons/auth/google.svg'));

        iconRegistry.addSvgIcon(
            'github',
            sanitizer.bypassSecurityTrustResourceUrl('assets/icons/auth/github.svg'));

        const fb = formBuilder as IFormBuilder;

        this.loginForm = fb.group<LoginForm>({
            email: [null, [Validators.required, Validators.email]],
            password: [null, Validators.required],
            password2: null
        });

        this.submit$.pipe(
            debounceTime(250),
            filter(() => this.loginForm.valid),
            switchMap(() => {
                const value = this.loginForm.value;
                let userCredentials$: Observable<UserCredential>;

                if (this.loginFormType === 'reset') {
                    return authService.sendPasswordResetEmail$(value.email).pipe(
                        tap(() => this.loginFormType = 'login')
                    );
                }

                if (this.loginFormType === 'signin') {
                    if (value.password !== value.password2) {
                        this.loginForm.setErrors({ passwordnotmatch: true });
                        changeDetectorRef.markForCheck();
                        return of(undefined);
                    }

                    userCredentials$ = authService.createUserWithEmailAndPassword$(value.email, value.password);
                } else {
                    userCredentials$ = authService.signInWithEmail$(value.email, value.password);
                }

                return userCredentials$.pipe(
                    switchMap(() => authService.isAuthenticated$.pipe(
                        filter(Boolean),
                        timeout(10000)
                    )),
                    catchError((err: unknown) => {
                        const validationError = errorService.toValidationError(err);
                        this.loginForm.setErrors(validationError);
                        changeDetectorRef.markForCheck();
                        return of(undefined);
                    }),
                    tap(userCredentials => {
                        if (userCredentials) {
                            console.log('Navigate back from login after authentification to', this.activatedRoute.snapshot.queryParams.navto);
                            void this.router.navigateByUrl(this.activatedRoute.snapshot.queryParams.navto as string || '/home');
                        }
                    })
                );
            }),
            takeUntil(this.destroyed$)
        ).subscribe();
    }

    public signIn$(provider: AuthProvider): Observable<unknown> {
        return asyncSwitchMap(() => this.authService.signInWithProvider$(provider).pipe(
            switchMap(() => this.authService.isAuthenticated$.pipe(
                filter(Boolean),
                timeout(10000)
            )),
            delay(500),
            tap(() => {
                console.log('Navigate back from login after authentification to', this.activatedRoute.snapshot.queryParams.navto);
                void this.router.navigateByUrl(this.activatedRoute.snapshot.queryParams.navto as string || '/home');
            })
        ));
    }
}
