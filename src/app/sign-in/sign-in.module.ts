import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';

import { ThrottleEventModule } from '../common/throttle-event/throttle-event.module';
import { TranslationModule } from '../translation/translation.module';
import { SignInComponent } from './sign-in.component';

const routes: Routes = [
    { path: '', component: SignInComponent }
];

@NgModule({
    declarations: [
        SignInComponent
    ],
    exports: [
        SignInComponent
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        RouterModule.forChild(routes),
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
        ThrottleEventModule,
        TranslationModule
    ]
})

export class SignInModule { }
