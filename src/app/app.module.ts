import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, enableProdMode, ErrorHandler, NgModule } from '@angular/core';
import { provideFirebaseApp } from '@angular/fire/app';
import { AuthModule } from '@angular/fire/auth';
import { FirestoreModule } from '@angular/fire/firestore';
import { getStorage, provideStorage, StorageModule } from '@angular/fire/storage';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { initializeApp } from 'firebase/app';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { ErrorService } from './services/error.service';


if (environment.production) {
    enableProdMode();
}

@NgModule({
    imports: [
        AuthModule,
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        FirestoreModule,
        HttpClientModule,
        MatDialogModule,
        MatSnackBarModule,
        routing,
        StorageModule,
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        provideStorage(() => getStorage())
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    providers: [
        {
            provide: ErrorHandler,
            useClass: ErrorService
        }
    ]
})
export class AppModule { }
