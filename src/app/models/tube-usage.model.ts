

import { IdLabel } from './id-label.model';
import { TubeUsageZoom } from './tube-usage-zoom.model';

export type TubeUsageMode = 'se' | 'pp' | '2pp' | '3pp' | '4pp' | '5pp' | '6pp';

export const tubeUsageModes = ['se', 'pp', '2pp', '3pp', '4pp', '5pp', '6pp'] as ReadonlyArray<TubeUsageMode>;

export interface TubeUsage extends IdLabel<string> {
    va: number;
    iazero: number;
    vinpp: number;
    mode: TubeUsageMode;
    load: number;
    unit: number;
    traces: string;
    zoom?: TubeUsageZoom;
    series?: { [key: string]: false };
    note?: string;
}
