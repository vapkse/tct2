import { IdLabel } from './id-label.model';

export type TubeBase = IdLabel<number>;
