export type TubeMode = 'se' | 'pp' | '2pp' | '3pp' | '4pp' | '5pp' | '6pp';

export interface TubeLoad {
    z?: number;
    mode?: TubeMode;
}
