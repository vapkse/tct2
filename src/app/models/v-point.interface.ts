export interface VPoint {
    vg1: number;
    va: number;
    ia: number;
    classA: boolean;
    overflow?: boolean;
    vt?: number;
    it?: number;
}
