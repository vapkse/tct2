
import { IdLabel } from './id-label.model';
import { TubePins } from './tube-pins.model';

export interface TubeType extends IdLabel<number> {
    config: Array<TubePins>;
    sym: boolean;
    uid: string;
}
