import { IdLabel } from './id-label.model';

export type TubePinout = IdLabel<number>;
