
export interface TubeGraphPoint {
    va: number;
    ik: number;
}

export interface TubeGraphCurve {
    p: Array<TubeGraphPoint>;
    vg1: number;
}

export interface TubeGraph {
    c: Array<TubeGraphCurve>;
    vg2: number;
    name: string;
    triode: boolean;
}
