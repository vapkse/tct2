import { Document } from './document.model';
import { IdLabel } from './id-label.model';
import { TubeUnit } from './tube-unit.model';
import { TubeUsage } from './tube-usage.model';

export interface Tube extends IdLabel<number> {
    baseId: number;
    pinoutId: number;
    typeId: number;
    documents: ReadonlyArray<Document>;
    units: ReadonlyArray<TubeUnit>;
    usages: ReadonlyArray<TubeUsage>;
    notes: string;
    dh: boolean;
    vh1: number;
    ih1: number;
    vh2: number;
    ih2: number;
    userId?: string;
    date: number;
    text?: string; // Not used on this version
    seen?: boolean;
    merged?: boolean;
}
