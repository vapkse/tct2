import { IdLabel } from './id-label.model';
import { Printable } from './printable.model';

export type CriterionOperator = '=' | '~' | '!=' | '!~' | '<' | '<=' | '>' | '>=' | '!*' | '=*';

export type CriterionValueType = 'string' | 'number' | 'boolean' | 'lookup';

export type CriterionValue = string | number | boolean;

export class Criterion implements Printable {
    public type: CriterionValueType;
    public label: string;
    public fieldPath: string;
    public lookup: ReadonlyArray<IdLabel<unknown>>;

    public constructor(public id: string, public operator?: CriterionOperator, public value?: CriterionValue, public unit?: string) {

    }

    public static fromCriterion(original: Criterion): Criterion {
        const criterion = new Criterion(original.id, original.operator, original.value);
        criterion.type = original.type;
        criterion.label = original.label;
        criterion.fieldPath = original.fieldPath;
        criterion.lookup = original.lookup;
        criterion.unit = original.unit;
        return criterion;
    }

    public static numericField(id: string, name: string, unit: string, fieldPath: string): Criterion {
        const criterion = new Criterion(id);
        criterion.type = 'number';
        criterion.label = name;
        criterion.unit = unit;
        criterion.fieldPath = fieldPath;
        return criterion;
    }

    public static textField(id: string, name: string, fieldPath: string): Criterion {
        const criterion = new Criterion(id);
        criterion.type = 'string';
        criterion.label = name;
        criterion.fieldPath = fieldPath;
        return criterion;
    }

    public static booleanField(id: string, name: string, fieldPath: string): Criterion {
        const criterion = new Criterion(id);
        criterion.type = 'boolean';
        criterion.label = name;
        criterion.fieldPath = fieldPath;
        return criterion;
    }

    public static lookupField(id: string, name: string, fieldPath: string, lookUp: ReadonlyArray<IdLabel<unknown>>): Criterion {
        const criterion = new Criterion(id);
        criterion.type = 'lookup';
        criterion.label = name;
        criterion.fieldPath = fieldPath;
        criterion.lookup = lookUp;
        return criterion;
    }

    public setUndefined(): this {
        this.value = undefined;
        return this;
    }

    public setOperator(value: CriterionOperator): this {
        this.operator = value;
        return this;
    }

    public setValue(value: string): this {
        this.value = value;
        return this;
    }

    public toString(): string {
        return this.label;
    }
}
