import { Printable } from './printable.model';
export interface IdLabel<T> extends Printable {
    id: T;
}
