export interface LoadLine {
    vazero: number;
    iazero: number;
    vamin: number;
    vamax: number;
    iamin: number;
    iamax: number;
    load: number;
}
