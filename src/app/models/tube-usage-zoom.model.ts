export interface TubeUsageZoomY {
    minY: number;
    maxY: number;
}

export interface TubeUsageZoomX {
    minX: number;
    maxX: number;
}

export interface TubeUsageZoom extends TubeUsageZoomX, TubeUsageZoomY {
}
