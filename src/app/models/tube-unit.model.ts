
export interface TubeUnit {
    vamax: number;
    vamaxp: number;
    pamax: number;
    vg2max: number;
    vg2maxp: number;
    ig2max: number;
    ig2maxp: number;
    pg2max: number;
    vhknmax: number;
    vhkpmax: number;
    ikmax: number;
    ikmaxp: number;
    vg1nmax: number;
    vg1nmaxp: number;
    ig1max: number;
    ig1maxp: number;
    cgk: number;
    cak: number;
    cga: number;
}
