import { IdLabel } from './id-label.model';

export interface Document extends IdLabel<string> {
    filename: string;
}
