import { TubeGraph } from './tube-graph.model';

export interface TubeGraphFile {
    tubeGraph: TubeGraph;
    filename: string;
    userId?: string;
    date?: number;
}
