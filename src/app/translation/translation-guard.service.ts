import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { delay, map, Observable } from 'rxjs';

import { TranslationService } from './translation.service';

@Injectable({
    providedIn: 'root'
})
export class TranslationGuardService implements CanActivate {
    public constructor(
        private translationService: TranslationService
    ) { }

    public canActivate(_route: ActivatedRouteSnapshot): Observable<boolean> {
        return this.translationService.translations$.pipe(
            delay(1),
            map(() => true)
        );
    }
}
