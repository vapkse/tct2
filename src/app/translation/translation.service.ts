import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, combineLatestWith, distinctUntilChanged, map, Observable, of, ReplaySubject, startWith, switchMap, tap } from 'rxjs';

import { shareReplayLast } from '../common/custom-operators';
import { StatusService } from '../common/status/status.service';

export type Locales = 'en' | 'fr';

interface TranslationFile {
    locale: string;
    translations: { [key: string]: string };
}

export interface LangItem {
    id: Locales;
    label: string;
}

@Injectable({
    providedIn: 'root'
})
export class TranslationService {
    public locale$ = new ReplaySubject<Locales>(1);

    public translations$: Observable<Map<string, string>>;
    public currentLocale$: Observable<Locales>;

    // Asynchronous for directive
    public currentTranslationMap: Map<string, string>;

    public langs = [
        { id: 'en', label: 'English' },
        { id: 'fr', label: 'Français' }
    ] as ReadonlyArray<LangItem>;

    private translationsMapCache = new Map<Locales, Observable<Map<string, string>>>();

    public constructor(
        httpClient: HttpClient,
        statusService: StatusService
    ) {

        const english$ = httpClient.get<TranslationFile>('assets/locale/en.json').pipe(
            map(translationFile => Object.keys(translationFile.translations).reduce((m, key) => m.set(key, translationFile.translations[key]), new Map<string, string>())),
            shareReplayLast()
        );

        this.currentLocale$ = this.locale$.pipe(
            startWith(location.search.replace(/^.*lang=([a-z][a-z]).*$/, '$1') as Locales),
            distinctUntilChanged(),
            map(locale => {
                if (this.langs.some(item => item.id === locale)) {
                    try {
                        if (locale) {
                            localStorage.setItem('tct-locale', locale);
                        } else {
                            localStorage.removeItem('tct-locale');
                        }
                    } catch (_e) {
                        console.log('Fail to set your locale to the local storage.');
                    }
                } else {
                    try {
                        locale = localStorage.getItem('tct-locale') as Locales;
                    } catch (_e) {
                        console.log('Fail to get your locale from the local storage.');
                    }
                }
                return locale || 'en';
            }),
            shareReplayLast()
        );

        this.translations$ = this.currentLocale$.pipe(
            switchMap(locale => {
                if (!locale || locale === 'en') {
                    return of(this.currentTranslationMap = null as Map<string, string>);
                }

                if (!this.translationsMapCache.has(locale)) {
                    this.translationsMapCache.set(locale, httpClient.get<TranslationFile>(`assets/locale/${locale}.json`).pipe(
                        combineLatestWith(english$),
                        map(([current, english]) => Object.keys(current.translations).reduce((m, key) => m.set(english.get(key), current.translations[key]), new Map<string, string>())),
                        catchError((err: unknown) => statusService.showStatus$(err).pipe(
                            map(() => null as Map<string, string>)
                        )),
                        shareReplayLast(),
                        tap(translationsMap => this.currentTranslationMap = translationsMap)
                    ));
                }

                return this.translationsMapCache.get(locale);
            })
        );
    }

    public translate(value: string): string {
        return this.currentTranslationMap?.get(value) || value;
    }

    public translateWithArgs(value: string, args: string | string[]): string {
        if (args instanceof Array) {
            return args.reduce((tr, arg, index) => tr.replace(`$${index}`, arg), this.currentTranslationMap?.get(value) || value);
        } else {
            const translated = this.currentTranslationMap?.get(value) || value;
            return translated.replace('$1', args);
        }
    }
}
