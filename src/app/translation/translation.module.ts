import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslationDirective, TranslationPipe, TranslationTitleDirective } from './translation.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        TranslationDirective,
        TranslationPipe,
        TranslationTitleDirective
    ],
    declarations: [
        TranslationDirective,
        TranslationPipe,
        TranslationTitleDirective
    ]
})
export class TranslationModule { }

export * from './translation.directive';
