import { Directive, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';

import { TranslationService } from './translation.service';

@Directive({
    selector: '[translate]'
})
export class TranslationDirective implements OnInit {
    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        private translationService: TranslationService
    ) { }

    public ngOnInit(): void {
        const currentTranslationMap = this.translationService.currentTranslationMap;
        if (!currentTranslationMap) {
            return;
        }

        const translated = currentTranslationMap.get(this.elementRef.nativeElement.innerText);
        if (translated) {
            this.elementRef.nativeElement.innerText = translated;
        }
    }
}

@Directive({
    selector: '[translate-title]'
})
export class TranslationTitleDirective implements OnInit {
    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        private translationService: TranslationService
    ) { }

    public ngOnInit(): void {
        const currentTranslationMap = this.translationService.currentTranslationMap;
        if (!currentTranslationMap) {
            return;
        }

        const key = this.elementRef.nativeElement.getAttribute('title');
        const translated = currentTranslationMap.get(key);
        if (translated) {
            this.elementRef.nativeElement.setAttribute('title', translated);
        }
    }
}

@Pipe({ name: 'trans' })
export class TranslationPipe implements PipeTransform {
    public constructor(
        private translationService: TranslationService
    ) { }

    public transform(value: string): string {
        return this.translationService.translate(value);
    }
}
