import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { cloneDeep } from 'lodash-es';
import { catchError, filter, map, Observable, of, startWith, Subject, switchMap, take, takeUntil, tap, throttleTime } from 'rxjs';

import { asyncSwitchMap, shareReplayLast } from 'src/app/common/custom-operators';
import { Document } from 'src/app/models/document.model';

import { subscribeWith } from '../../common/custom-operators';
import { DestroyDirective } from '../../common/destroy/destroy.directive';
import { IdService } from '../../common/id.service';
import { MessageBoxDialogService } from '../../common/message-box-dialog/message-box-dialog.service';
import { StatusService } from '../../common/status/status.service';
import { AttachmentsDialogService } from '../../components/attachments-dialog/attachments-dialog.service';
import { ChartWizardDialogService } from '../../components/chart-wizard-dialog/chart-wizard-dialog.service';
import { ImageModalData } from '../../components/image-modal/image-modal.model';
import { ImageTooltipService } from '../../components/image-modal/image-tooltip.service';
import { UploadDialogService } from '../../components/upload-dialog/upload-dialog.service';
import { Tube } from '../../models/tube.model';
import { TubeBase } from '../../models/tube-base.model';
import { TubePinout } from '../../models/tube-pinout.model';
import { TubeType } from '../../models/tube-type.model';
import { AuthService } from '../../services/auth/auth.service';
import { allAcceptedFileType } from '../../services/constants';
import { DatabaseService } from '../../services/database.service';
import { EditTubeData, OpenService } from '../../services/open.service';
import { SaveService } from '../../services/save.service';
import { ThemeService } from '../../services/theme.service';
import { TubesService } from '../../services/tubes.service';
import { TranslationService } from '../../translation/translation.service';


@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'tube-card',
    templateUrl: './tube-card.component.html',
    styleUrls: ['./tube-card.component.scss']
})
export class TubeCardComponent extends DestroyDirective implements OnInit {
    @ViewChild('fileUpload')
    public fileUpload: ElementRef<HTMLInputElement>;

    @Input()
    public tube: Tube;

    @ViewChild(MatMenuTrigger)
    private menuTrigger: MatMenuTrigger;

    public unit = 0;

    public uploadDocument$ = new Subject<Event>();

    public openViewer$ = new Subject<MouseEvent>();
    public createUsage$ = new Subject<MouseEvent>();
    public editTube$ = new Subject<EditTubeData>();

    public type$: Observable<TubeType>;
    public base$: Observable<TubeBase>;
    public pinout$: Observable<TubePinout>;
    public hasGrid2$: Observable<boolean>;

    public allAcceptedFileType = allAcceptedFileType;

    public constructor(
        public authService: AuthService,
        public tubeService: TubesService,
        public databaseService: DatabaseService,
        private svgTooltipService: ImageTooltipService,
        private attachmentsDialogService: AttachmentsDialogService,
        private themeService: ThemeService,
        private messageBoxDialogService: MessageBoxDialogService,
        private statusService: StatusService,
        private chartWizardDialogService: ChartWizardDialogService,
        private saveService: SaveService,
        private changeDetectorRef: ChangeDetectorRef,
        private idService: IdService,
        private translationService: TranslationService,
        uploadDialogService: UploadDialogService,
        openService: OpenService
    ) {
        super();

        const editTube$ = this.editTube$.pipe(
            throttleTime(1000),
            switchMap(data => openService.editTube$(this.tube, data))
        );

        const createUsage$ = this.createUsage$.pipe(
            throttleTime(1000),
            map(event => openService.navigateTo(['creator', `${this.tube.id}`], event.ctrlKey))
        );

        this.openViewer$.pipe(
            throttleTime(1000),
            switchMap(event => openService.viewTube$(this.tube, event.ctrlKey)),
            subscribeWith(editTube$, createUsage$),
            takeUntil(this.destroyed$)
        ).subscribe();

        this.uploadDocument$.pipe(
            throttleTime(1000),
            switchMap(event => {
                const target = event.target as HTMLInputElement;
                const file = target.files[0];
                if (!allAcceptedFileType.includes(file.type)) {
                    return statusService.showStatus$(new Error('Invalid file type.')).pipe(
                        map(() => undefined as Document)
                    );
                }

                // const blobToDataUrl = (blob: Blob): Promise<string> => new Promise<string>((resolve, reject) => {
                //     const reader = new FileReader();
                //     reader.onload = (): unknown => resolve(reader.result as string);
                //     reader.onerror = (): unknown => reject(reader.error);
                //     reader.onabort = (): unknown => reject(new Error('Read aborted'));
                //     reader.readAsDataURL(blob);
                // });

                // console.log(blobToDataUrl(file));

                return uploadDialogService.open$({ filename: `docs/${file.name}`, data: file }).pipe(
                    switchMap(resultFileName => {
                        this.fileUpload.nativeElement.value = '';
                        if (resultFileName) {
                            // Clone tube before modifications to avoid a modification of the original instance
                            const updatedTube = cloneDeep(this.tube);
                            const document = {
                                id: idService.generate(),
                                filename: file.name,
                                label: file.name
                            } as Document;
                            updatedTube.documents = [...(updatedTube.documents || []), document];
                            changeDetectorRef.markForCheck();
                            return saveService.saveTube$(updatedTube).pipe(
                                take(1),
                                tap(tubeSaved => this.tubeService.updateTube$.next(tubeSaved))
                            );
                        }
                        return of(undefined as Tube);
                    }),
                    catchError((err: unknown) => statusService.showStatus$(err).pipe(
                        map(() => undefined as Tube)
                    ))
                );
            }),
            takeUntil(this.destroyed$)
        ).subscribe();
    }

    public ngOnInit(): void {
        this.type$ = this.tubeService.tubeTypeMap$.pipe(
            filter(() => !!this.tube.typeId),
            map(tubeTypeMap => tubeTypeMap.get(this.tube.typeId)),
            shareReplayLast()
        );

        this.hasGrid2$ = this.type$.pipe(
            map(type => type?.config?.[this.unit]?.pins?.some(p => p === 'g2')),
            shareReplayLast()
        );

        this.base$ = this.tubeService.tubeBaseMap$.pipe(
            filter(() => !!this.tube.baseId),
            map(tubeBaseMap => tubeBaseMap.get(this.tube.baseId)),
            startWith({} as TubeBase),
            shareReplayLast()
        );

        this.pinout$ = this.tubeService.tubePinoutMap$.pipe(
            filter(() => !!this.tube.pinoutId),
            map(tubePinoutMap => tubePinoutMap.get(this.tube.pinoutId)),
            startWith({} as TubePinout),
            shareReplayLast()
        );
    }

    public heatherInfos(): string {
        const infos = [];
        if (this.tube.vh1) {
            infos.push(this.tube.vh1);
            infos.push('V   ');
        }
        if (this.tube.ih1) {
            infos.push(this.tube.ih1);
            infos.push('A');
        }
        return infos.join('') || '-';
    }

    public anodeInfos(): string {
        const infos = [];
        const unit = this.tube?.units[this.unit];
        if (unit?.vamax) {
            infos.push(unit.vamax);
            infos.push('V   ');
        }
        if (unit?.pamax) {
            infos.push(unit.pamax);
            infos.push('W');
        }
        return infos.join('') || '-';
    }

    public g2Infos(): string {
        const infos = [];
        const unit = this.tube.units[this.unit];
        if (unit?.vg2max) {
            infos.push(unit.vg2max);
            infos.push('V   ');
        }
        if (unit?.vg2max) {
            infos.push(unit.pg2max);
            infos.push('W');
        }
        return infos.join('') || '-';
    }

    public baseTooltip$(base: TubeBase): Observable<unknown> {
        if (!base.label) {
            return of(undefined as void);
        }

        return asyncSwitchMap<unknown>(() => this.themeService.isDark$.pipe(
            switchMap(isDark => {
                const basePath = isDark ? 'bases-dark' : 'bases-light';
                const data = {
                    title: base.label,
                    filename: `assets/images/${basePath}/${base.label}.svg`
                } as ImageModalData;

                return this.svgTooltipService.open$(data);
            })
        ));
    }

    public pinoutTooltip$(pinout: TubePinout): Observable<unknown> {
        if (!pinout.label) {
            return of(undefined as void);
        }

        return asyncSwitchMap<unknown>(() => this.themeService.isDark$.pipe(
            switchMap(isDark => {
                const pinoutsPath = isDark ? 'pinouts-dark' : 'pinouts-light';
                const data = {
                    title: pinout.label,
                    filename: `assets/images/${pinoutsPath}/${pinout.label}.svg`
                } as ImageModalData;

                return this.svgTooltipService.open$(data);
            })
        ));
    }

    public openAttachmentsDialog$(): Observable<unknown> {
        if (!this.tube.documents?.length && !this.tube.usages?.length) {
            return of(undefined as void);
        }
        return asyncSwitchMap(() => this.attachmentsDialogService.open$({ tube: this.tube, documentFilter: null }));
    }

    public canCopy$(): Observable<boolean> {
        if (!this.tube.userId) {
            return of(false);
        }

        return this.authService.isAdmin$.pipe(
            switchMap(isAdmin => {
                if (!isAdmin) {
                    return of(false);
                }

                return this.authService.authenticatedUser$.pipe(
                    map(authenticatedUser => {
                        const userId = authenticatedUser.email || authenticatedUser.uid;
                        return userId !== this.tube.userId;
                    })
                );
            })
        );
    }

    public createChart$(): Observable<unknown> {
        return asyncSwitchMap(() => {
            this.menuTrigger?.closeMenu();
            return this.chartWizardDialogService.open$({ tube: this.tube }).pipe(
                filter(Boolean),
                switchMap(tubeGraph =>
                    // Save json into firebase as a document
                    this.saveService.saveChart$(this.tube.id, tubeGraph).pipe(
                        switchMap(graphFile => {
                            // Clone tube before modifications to avoid a modification of the original instance
                            const updatedTube = cloneDeep(this.tube);

                            // check if document already exists
                            const existingIndex = updatedTube.documents.findIndex(d => d.filename === graphFile.filename);
                            const documents = updatedTube.documents.filter((_d, index) => index !== existingIndex);
                            documents.push({
                                id: this.idService.generate(),
                                label: graphFile.tubeGraph.name,
                                filename: graphFile.filename
                            } as Document);
                            updatedTube.documents = documents;
                            this.changeDetectorRef.markForCheck();
                            return this.saveService.saveTube$(updatedTube).pipe(
                                take(1),
                                tap(tubeSaved => this.tubeService.updateTube$.next(tubeSaved))
                            );
                        })
                    )
                ),
                catchError((err: unknown) => this.statusService.showStatus$(err))
            );
        });
    }

    public delete$(): Observable<unknown> {
        return asyncSwitchMap(() => {
            this.menuTrigger?.closeMenu();
            const msg = this.translationService.translateWithArgs('Are you sure you want to delete your version of the $1.<br/>After deleting your tube, you will see the version of the global database.', this.tube.label);
            return this.messageBoxDialogService.openConfirmation$(msg).pipe(
                filter(response => response === 'ok'),
                switchMap(() => this.databaseService.deleteUserTube$(this.tube).pipe(
                    tap(() => this.tubeService.removeTube$.next(this.tube)),
                    catchError((err: unknown) => this.statusService.showStatus$(err))
                ))
            );
        });
    }

    public getDisplayDate(timeStamp: number): string {
        return (new Date(timeStamp)).toLocaleDateString();
    }
}
