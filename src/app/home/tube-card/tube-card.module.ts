import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

import { ThrottleEventModule } from 'src/app/common/throttle-event/throttle-event.module';
import { TooltipModule } from 'src/app/common/tooltip/tooltip.module';
import { ViewPortModule } from 'src/app/common/viewport/viewport.module';

import { SaveService } from '../../services/save.service';
import { TranslationModule } from '../../translation/translation.module';
import { TubeCardComponent } from './tube-card.component';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatIconModule,
        MatMenuModule,
        ThrottleEventModule,
        TooltipModule,
        TranslationModule,
        ViewPortModule
    ],
    exports: [TubeCardComponent],
    declarations: [TubeCardComponent],
    providers: [SaveService]
})
export class TubeCardModule {
}
