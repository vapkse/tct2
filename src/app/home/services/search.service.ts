import { Injectable } from '@angular/core';
import { map, Observable, take } from 'rxjs';

import { Criterion } from '../../models/criterion.model';
import { Tube } from '../../models/tube.model';
import { isDefined, isUndefined } from '../../services/constants';
import { CriteriaService } from './criterion.service';

interface SearchResult {
    tube: Tube;
    match: number;
}

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    public constructor(
        private criteriaService: CriteriaService
    ) { }

    public filter$(tubes: ReadonlyArray<Tube>, search: string): Observable<ReadonlyArray<Tube>> {
        return this.criteriaService.parse$(search).pipe(
            take(1),
            map(criteria => {
                if (!criteria.length) {
                    return tubes;
                }

                return tubes
                    .map(tube => ({ match: this.tubeMatch(tube, criteria), tube } as SearchResult))
                    .filter(result => result.match > 0)
                    .sort((result1, result2) => result1.match - result2.match || result1.tube.label.localeCompare(result2.tube.label))
                    .map(result => result.tube);
            })
        );
    }

    private tubeMatch(tube: Tube, criteria: ReadonlyArray<Criterion>): number {
        const valueMatch = (value: unknown, criterion: Criterion): number => {
            // Renvoie le score de la recherche, plus le nombre est petit, plus le résultat apparait en tête

            if (criterion.operator === '!*') {
                return isUndefined(value) ? 2 : 0;
            } else if (criterion.operator === '=*') {
                return isUndefined(value) ? 0 : 2;
            } else if (typeof value === 'number') {
                if (isDefined(criterion.value)) {
                    const criterionValue = +criterion.value;
                    if (!isNaN(criterionValue)) {
                        const numValue = value;
                        if (criterion.operator === '=' && numValue === criterionValue) {
                            // Exact match
                            return 1;
                        } else if (criterion.operator === '!=' && numValue !== criterionValue) {
                            // Exact match
                            return 1;
                        } else if (criterion.operator === '>=' && numValue >= criterionValue) {
                            // Greather or equals than
                            return 3 + value - criterionValue;
                        } else if (criterion.operator === '>' && numValue > criterionValue) {
                            // Greather than
                            return 3 + value - criterionValue;
                        } else if (criterion.operator === '<=' && numValue <= criterionValue) {
                            // Lower or equals than
                            return 3 + criterionValue - value;
                        } else if (criterion.operator === '<' && numValue < criterionValue) {
                            // Lower than
                            return 3 + criterionValue - value;
                        }
                    }
                }
                return 0;
            } else {
                if (isUndefined(criterion.value) || isUndefined(value)) {
                    return isUndefined(criterion.value) === isUndefined(value) ? 1 : 0;
                }

                let score = 0;

                // String or object
                if (typeof value === 'object') {
                    score++;
                }

                const text = criterion.value.toString().trim();
                const ltext = text.toLowerCase();
                const valueStr = String(value).trim().toLowerCase();

                if (criterion.operator === '!=') {
                    return valueStr !== ltext ? 1 : 0;
                } else {
                    const textLength = text.length;
                    const valueLength = valueStr.length;
                    const pos = valueStr.toLowerCase().indexOf(ltext);
                    if (criterion.operator === '!~') {
                        return pos === -1 ? 1 : 0;
                    } else if (pos === -1) {
                        return 0;
                    } else {
                        score++;
                        if (pos === 0 && valueStr === text) {
                            // Exact match, first
                        } else if (pos === 0 && textLength === valueLength) {
                            // Exact match, case unsensitive, second
                            score += 1;
                        } else if (criterion.operator === '~') {
                            // Contains
                            score += pos + 2;
                        } else {
                            // Whole word only
                            return 0;
                        }
                        return score;
                    }
                }
            }
        };

        const tubeMatch = (criterion: Criterion): number => {
            // Read values
            const paths = criterion.fieldPath.split('/');

            const getValues = (value: Record<string, unknown>, index: number): ReadonlyArray<Record<string, unknown> | string | number> => {
                if (index >= paths.length) {
                    return [value];
                }

                const path = paths[index];
                if (path === 'date') {
                    const date = value[path] as number;
                    if (!date) {
                        return [undefined];
                    }
                    return [Math.floor((Date.now() - date) / 86400000)];
                }

                const val = value[path] as Record<string, unknown> | ReadonlyArray<Record<string, unknown>>;
                if (val instanceof Array) {
                    return val.reduce((arr, v) => [...arr, ...getValues(v, index + 1) as ReadonlyArray<Record<string, unknown>>], new Array<Record<string, unknown>>());
                } else {
                    return getValues(val, index + 1);
                }
            };

            const values = getValues(tube as Record<keyof Tube, unknown>, 0);
            let score = 0;
            values.find((value: unknown) => {
                const res = valueMatch(value, criterion);
                if (res === 0) {
                    // Continue next value
                    return false;
                }
                if (score === 0 || res < score) {
                    score = res;
                }
                // Si peut pas faire mieux, interrompt la boucle
                return score === 1;
            });

            return score;
        };

        let bestScore = 0;
        criteria.find(criterion => {
            const res = tubeMatch(criterion);
            if (res === 0) {
                bestScore = 0;
                return true;
            }
            if (bestScore === 0 || res < bestScore) {
                bestScore = res;
            }
            // Continue avec le critère suivant
            return false;
        });

        return bestScore;
    }
}
