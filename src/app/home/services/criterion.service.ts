import { Injectable } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { combineLatestWith, from, map, mergeMap, Observable, of, switchMap, take, throwError, toArray } from 'rxjs';

import { shareReplayLast } from '../../common/custom-operators';
import { Criterion, CriterionOperator, CriterionValueType } from '../../models/criterion.model';
import { AuthService } from '../../services/auth/auth.service';
import { isUndefined } from '../../services/constants';
import { TubesService } from '../../services/tubes.service';
import { TranslationService } from '../../translation/translation.service';

export interface Operator {
    title: string;
    value: string;
}

@Injectable({
    providedIn: 'root'
})
export class CriteriaService {
    public stringOperators: ReadonlyArray<string>;
    public numberOperators: ReadonlyArray<string>;
    public booleanOperators: ReadonlyArray<string>;
    public lookUpOperators: ReadonlyArray<string>;

    public operatorDefinitions$: Observable<ReadonlyArray<Operator>>;
    public operatorDefinitionMap$: Observable<Map<CriterionValueType, ReadonlyArray<Operator>>>;

    public criterionDefinitions$: Observable<ReadonlyArray<Criterion>>;
    public criterionDefinitionMap$: Observable<Map<string, Criterion>>;

    public constructor(
        private tubesService: TubesService,
        translationService: TranslationService,
        authService: AuthService
    ) {

        this.operatorDefinitions$ = translationService.translations$.pipe(
            map(() => ([{
                value: '=',
                title: translationService.translate('equals')
            }, {
                value: '~',
                title: translationService.translate('contains')
            }, {
                value: '!=',
                title: translationService.translate('not equals')
            }, {
                value: '!~',
                title: translationService.translate('not contains')
            }, {
                value: '=*',
                title: translationService.translate('is defined')
            }, {
                value: '!*',
                title: translationService.translate('is undefined')
            }, {
                value: '<',
                title: translationService.translate('lower than')
            }, {
                value: '<=',
                title: translationService.translate('lower or equals than')
            }, {
                value: '>',
                title: translationService.translate('greather than')
            }, {
                value: '>=',
                title: translationService.translate('greather or equals than')
            }] as ReadonlyArray<Operator>)),
            shareReplayLast()
        );

        this.stringOperators = ['=', '~', '!=', '!~', '!*', '=*'];
        this.numberOperators = ['=', '!=', '<', '<=', '>', '>=', '!*', '=*'];
        this.lookUpOperators = ['=', '!*', '=*'];
        this.booleanOperators = ['=', '!*', '=*'];

        this.operatorDefinitionMap$ = this.operatorDefinitions$.pipe(
            map(operatorDefinitions => {
                const operatorDefinitionMap = new Map<CriterionValueType, ReadonlyArray<Operator>>();
                operatorDefinitionMap.set('string', operatorDefinitions.filter(op => this.stringOperators.includes(op.value)));
                operatorDefinitionMap.set('number', operatorDefinitions.filter(op => this.numberOperators.includes(op.value)));
                operatorDefinitionMap.set('boolean', operatorDefinitions.filter(op => this.booleanOperators.includes(op.value)));
                operatorDefinitionMap.set('lookup', operatorDefinitions.filter(op => this.lookUpOperators.includes(op.value)));
                return operatorDefinitionMap;
            }),
            shareReplayLast()
        );

        this.criterionDefinitions$ = this.tubesService.tubeTypes$.pipe(
            combineLatestWith(this.tubesService.tubeBases$, this.tubesService.tubePinouts$, authService.isAdmin$, translationService.translations$),
            map(([types, bases, pinouts, isAdmin]) => {
                const definitions = [
                    Criterion.textField('name', translationService.translate('Name'), 'label'),
                    Criterion.textField('notes', translationService.translate('Notes'), 'notes'),
                    Criterion.lookupField('type', translationService.translate('Type'), 'typeId', types),
                    Criterion.lookupField('base', translationService.translate('Base'), 'baseId', bases),
                    Criterion.lookupField('pinout', translationService.translate('Pinout'), 'pinoutId', pinouts),
                    Criterion.numericField('vh1', translationService.translate('Vh1'), 'V', 'vh1'),
                    Criterion.numericField('ih1', translationService.translate('Ih1'), 'A', 'ih1'),
                    Criterion.numericField('vamax', translationService.translate('Va max'), 'V', 'units/vamax'),
                    Criterion.numericField('vamaxp', translationService.translate('Va max peak'), 'V', 'units/vmaxp'),
                    Criterion.numericField('pamax', translationService.translate('Pa max'), 'W', 'units/pamax'),
                    Criterion.numericField('vg2max', translationService.translate('Vg2 max'), 'V', 'units/vg2ma'),
                    Criterion.numericField('vg2maxp', translationService.translate('Vg2 max peak'), 'V', 'units/vgmaxp'),
                    Criterion.numericField('ig2max', translationService.translate('Ig2 max'), 'mA', 'units/ig2max'),
                    Criterion.numericField('ig2maxp', translationService.translate('Ig2 max peak'), 'mA', 'units/igmaxp'),
                    Criterion.numericField('pg2max', translationService.translate('Pg2 max'), 'W', 'units/pg2max'),
                    Criterion.numericField('vhknmax', translationService.translate('Vhkn max'), 'V', 'units/vhknma'),
                    Criterion.numericField('vhkpmax', translationService.translate('Vhk max peak'), 'V', 'units/vhkpmx'),
                    Criterion.numericField('ikmax', translationService.translate('Ik max'), 'mA', 'units/ikmax'),
                    Criterion.numericField('ikmaxp', translationService.translate('Ik max peak'), 'mA', 'units/imaxp'),
                    Criterion.numericField('vg1nmax', translationService.translate('Vg1n max'), 'V', 'units/vg1nmax'),
                    Criterion.numericField('vg1nmaxp', translationService.translate('Vg1n max peak'), 'V', 'units/vg1maxp'),
                    Criterion.numericField('ig1max', translationService.translate('Ig1 max'), 'mA', 'units/ig1max'),
                    Criterion.numericField('ig1maxp', translationService.translate('Ig1 max peak'), 'mA', 'units/igmaxp'),
                    Criterion.numericField('cgk', translationService.translate('Cgk'), 'pF', 'units/cgk'),
                    Criterion.numericField('cak', translationService.translate('Cak'), 'pF', 'units/cak'),
                    Criterion.numericField('cga', translationService.translate('Cga'), 'pF', 'units/cga'),
                    Criterion.numericField('days', translationService.translate('Modified'), translationService.translate('days'), 'date')
                ] as Array<Criterion>;

                if (isAdmin) {
                    definitions.push(...[
                        Criterion.numericField('id', translationService.translate('id'), undefined, 'id'),
                        Criterion.textField('userId', translationService.translate('user'), 'userId'),
                        Criterion.booleanField('seen', translationService.translate('seen'), 'seen'),
                        Criterion.booleanField('merged', translationService.translate('merged'), 'merged')
                    ]);
                }

                return definitions;
            }),
            shareReplayLast()
        );

        this.criterionDefinitionMap$ = this.criterionDefinitions$.pipe(
            map(criteria => criteria.reduce((dic, criterion) => dic.set(criterion.id, criterion), new Map<string, Criterion>())),
            shareReplayLast()
        );
    }

    public stringify(criteria: ReadonlyArray<Criterion>): string {
        const query = [] as string[];
        criteria.forEach(criterion => {
            if (isUndefined(criterion.value)) {
                query.push(`${criterion.id}${criterion.operator}undefined`);
            } else if (criterion.type === 'lookup') {
                query.push(`${criterion.id}${criterion.operator}${String(criterion.value)}`);
            } else {
                let value = criterion.value.toString();
                criterion.value = value = value.replace(/[=&><~!*]/g, '');
                query.push(`${criterion.id}${criterion.operator}${value}`);
            }
        });

        return query.join('&');
    }

    public isAdvancedSearch(query: string): boolean {
        return !!query.match(/(=|>=|<=|>|<|~|!|\*)/);
    }

    public parse$(query: string): Observable<ReadonlyArray<Criterion>> {
        return this.criterionDefinitionMap$.pipe(
            take(1),
            switchMap(dic => {
                const criteria = [] as Criterion[];
                if (!query || !query.trim()) {
                    return of(criteria);
                }

                const criteriaMap = new Map<string, Criterion>();
                return from(query.split('&')).pipe(
                    mergeMap(param => {
                        const match = param.trim().split(/(=\*|!\*|!=|!~|<=|>=|=|~|<|>)/);
                        let criterion: Criterion;
                        if (match.length === 1) {
                            criterion = Criterion.fromCriterion(dic.get('name')).setValue(query).setOperator('~');
                        } else {
                            // Add Criterion
                            const id = match[0].trim();
                            const operator = match[1].trim() as CriterionOperator;

                            criterion = dic.has(id) && Criterion.fromCriterion(dic.get(id));
                            if (!criterion) {
                                // Ignore param
                                return throwError(() => ({ invalidId: true } as ValidationErrors));
                            }

                            const value = match[2]?.trim() || '';
                            if (criteriaMap.has(`${id}#${operator}#${value}`)) {
                                // Ignore param
                                return throwError(() => ({ duplicate: true } as ValidationErrors));
                            }

                            if (value === '' && operator !== '!*' && operator !== '=*') {
                                // No value
                                return throwError(() => ({ requiredValue: true } as ValidationErrors));
                            }

                            criterion.operator = operator;
                            if (operator === '=*' || operator === '!*') {
                                criterion.value = undefined;
                            } else if (criterion.type === 'number' && this.numberOperators.includes(operator)) {
                                if (value) {
                                    const val = +value;
                                    if (isNaN(val)) {
                                        return throwError(() => ({ invalidValue: true } as ValidationErrors));
                                    }
                                }
                                criterion.value = value;
                            } else if (criterion.type === 'string' && this.stringOperators.includes(operator)) {
                                criterion.value = value !== 'undefined' ? value : undefined;
                            } else if (criterion.type === 'boolean' && this.booleanOperators.includes(operator)) {
                                criterion.value = value === 'true';
                            } else if (criterion.type === 'lookup' && this.lookUpOperators.includes(operator)) {
                                // Find seleted value on lookup
                                if (criterion.lookup) {
                                    const lookUpValue = criterion.lookup.find(v => String(v.id).toLowerCase() === value.toString().toLowerCase());
                                    if (!lookUpValue) {
                                        // Invalid lookup value
                                        return throwError(() => ({ invalidLookup: true } as ValidationErrors));
                                    }
                                    criterion.value = String(lookUpValue.id);
                                } else {
                                    criterion.value = undefined;
                                }
                            } else {
                                return throwError(() => ({ invalidOperator: true } as ValidationErrors));
                            }
                        }

                        criteriaMap.set(`${criterion.id}#${criterion.operator}#${String(criterion.value)}`, criterion);
                        return of(criterion);
                    }),
                    toArray()
                );
            })
        );
    }
}
