import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IFormArray, IFormBuilder, IFormGroup } from '@rxweb/types';
import { catchError, combineLatestWith, debounceTime, delay, distinctUntilChanged, filter, map, mergeWith, Observable, of, startWith, Subject, switchMap, take, takeUntil, throttleTime, withLatestFrom } from 'rxjs';

import { shareReplayLast } from 'src/app/common/custom-operators';
import { loading$, LoadingContext } from 'src/app/common/rx-if/loading-context';

import { asyncMap } from '../common/custom-operators';
import { DestroyDirective } from '../common/destroy/destroy.directive';
import { InputAutoSizeService } from '../common/input-autosize/input-autosize.service';
import { StatusData } from '../common/status/status.model';
import { StatusService } from '../common/status/status.service';
import { Criterion, CriterionOperator, CriterionValue } from '../models/criterion.model';
import { Printable } from '../models/printable.model';
import { Tube } from '../models/tube.model';
import { AuthService } from '../services/auth/auth.service';
import { isDefined } from '../services/constants';
import { OpenService } from '../services/open.service';
import { TubesService } from '../services/tubes.service';
import { CriteriaService } from './services/criterion.service';
import { SearchService } from './services/search.service';

interface SimpleForm {
    search: string;
}

interface CriterionForm {
    id: string;
    operator: CriterionOperator;
    value: CriterionValue;
    unit: string;
}

interface AdvancedForm {
    criteria: Array<CriterionForm>;
}

interface QueryParams {
    search?: string;
    advancedSearch?: string;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends DestroyDirective {
    public tubesLoader$: Observable<LoadingContext<ReadonlyArray<Tube>>>;
    public reloadSearch$ = new Subject<void>();
    public lastSearch$ = new Subject<string>();

    public addCriterion$ = new Subject<void>();
    public removeCriterion$ = new Subject<number>();
    public addTube$ = new Subject<MouseEvent>();

    public simpleForm$: Observable<IFormGroup<SimpleForm>>;
    public advancedForm$: Observable<IFormGroup<AdvancedForm>>;
    public criterionFormArray$: Observable<IFormArray<CriterionForm>>;

    public constructor(
        public searchService: SearchService,
        public criteriaService: CriteriaService,
        public authService: AuthService,
        public tubeService: TubesService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        formBuilder: FormBuilder,
        statusService: StatusService,
        openService: OpenService,
        inputAutoSizeService: InputAutoSizeService
    ) {
        super();

        const fb = formBuilder as IFormBuilder;

        const queryParams$ = activatedRoute.queryParams as Observable<QueryParams>;

        const searchQuery$ = queryParams$.pipe(
            map(queryParams => queryParams.search),
            distinctUntilChanged(),
            shareReplayLast()
        );

        const isAvancedSearchQuery$ = queryParams$.pipe(
            map(queryParams => queryParams.advancedSearch === 'true'),
            startWith(false),
            distinctUntilChanged(),
            shareReplayLast()
        );

        // !!! Ignore the rebuild of the form after a search to prevent the focus issue in form array !!!
        const ignoreChange$ = this.lastSearch$.pipe(
            delay(300),
            map(() => false),
            mergeWith(this.lastSearch$.pipe(map(() => true))),
            startWith(false),
            shareReplayLast()
        );

        this.simpleForm$ = isAvancedSearchQuery$.pipe(
            combineLatestWith(searchQuery$),
            withLatestFrom(ignoreChange$),
            filter(([_, ignoreChange]) => !ignoreChange),
            map(([[advancedSearch, search]]) => advancedSearch ? undefined as IFormGroup<SimpleForm> : fb.group<SimpleForm>({
                search
            })),
            shareReplayLast()
        );

        const simpleFormChange$ = this.simpleForm$.pipe(
            filter(Boolean),
            switchMap(form => form.valueChanges.pipe(
                debounceTime(75),
                map(values => values.search)
            ))
        );

        const criterionAdded$ = this.addCriterion$.pipe(
            switchMap(() => this.advancedForm$.pipe(take(1))),
            map(form => [...form.value.criteria, { id: 'name', operator: '~', value: null, type: 'string', unit: undefined } as CriterionForm]),
            shareReplayLast()
        );

        const criterionRemoved$ = this.removeCriterion$.pipe(
            switchMap(indexToRemove => this.advancedForm$.pipe(
                take(1),
                map(form => form.value.criteria.filter((_criterion, index) => indexToRemove !== index))
            )),
            shareReplayLast()
        );

        const criterionValidator = (control: AbstractControl): ValidationErrors | null => {
            const criterionControl = control as IFormGroup<CriterionForm>;
            const criterion = criterionControl.getRawValue();
            const applyOptions = { emitEvent: false, onlySelf: true };

            if (criterion.operator === '!*' || criterion.operator === '=*') {
                criterionControl.controls.value.setErrors(null);
                criterionControl.controls.value.disable(applyOptions);
                if (isDefined(criterion.value)) {
                    criterionControl.controls.value.setValue(undefined, applyOptions);
                }
            } else {
                criterionControl.controls.value.enable(applyOptions);
                if (criterion.value === undefined && criterion.value !== null && (criterion.id === 'seen' || criterion.id === 'merged')) {
                    criterionControl.controls.value.setValue(false, applyOptions);
                    criterionControl.controls.value.setErrors(null);
                } else if (isDefined(criterion.value)) {
                    criterionControl.controls.value.setErrors(null);
                } else {
                    criterionControl.controls.value.setErrors({ required: true } as ValidationErrors);
                }
            }
            return null;
        };

        this.advancedForm$ = isAvancedSearchQuery$.pipe(
            combineLatestWith(searchQuery$),
            withLatestFrom(ignoreChange$),
            filter(([_, ignoreChange]) => !ignoreChange),
            switchMap(([[advancedSearch, search]]) => {
                if (!advancedSearch) {
                    return of(undefined as ReadonlyArray<Criterion>);
                }

                return criteriaService.parse$(search).pipe(
                    catchError(() => statusService.showStatus$({
                        message: 'Invalid search params',
                        type: 'warning',
                        title: 'Warning',
                        duration: 2000
                    } as StatusData).pipe(
                        map(() => [{ id: 'text', operator: '~', value: null } as CriterionForm])
                    ))
                );
            }),
            mergeWith(criterionAdded$, criterionRemoved$),
            map(criteria => {
                if (!criteria) {
                    return undefined as IFormGroup<AdvancedForm>;
                }

                const criteriaFormArray = criteria.map(criterion => fb.group<CriterionForm>({
                    id: [criterion.id, Validators.required],
                    operator: [criterion.operator, Validators.required],
                    value: [criterion.value, inputAutoSizeService.getValidator('value')],
                    unit: criterion.unit
                }));

                criteriaFormArray.forEach(formGroup => formGroup.setValidators(criterionValidator));

                return fb.group<AdvancedForm>({
                    criteria: fb.array(criteriaFormArray)
                });
            }),
            shareReplayLast()
        );

        this.criterionFormArray$ = this.advancedForm$.pipe(
            filter(Boolean),
            map(form => form.controls.criteria as IFormArray<CriterionForm>),
            shareReplayLast()
        );

        const advancedFormChange$ = this.advancedForm$.pipe(
            filter(Boolean),
            switchMap(form => form.controls.criteria.valueChanges.pipe(
                filter(() => form.valid),
                debounceTime(75),
                mergeWith(criterionAdded$, criterionRemoved$),
                map(() => {
                    const formCriteria = form.getRawValue().criteria;
                    const criteria = formCriteria.map(formCriterion => new Criterion(formCriterion.id, formCriterion.operator, formCriterion.value, formCriterion.unit));
                    try {
                        return criteriaService.stringify(criteria);
                    } catch {
                        return undefined;
                    }
                })
            )),
            shareReplayLast()
        );

        const reloadSimpleSearch$ = this.reloadSearch$.pipe(
            withLatestFrom(this.simpleForm$.pipe(
                filter(Boolean)
            )),
            map(([_, form]) => form.value.search)
        );

        const reloadAdvancedSearch$ = this.reloadSearch$.pipe(
            withLatestFrom(this.advancedForm$.pipe(
                filter(Boolean)
            )),
            map(([_, form]) => {
                const formCriteria = form.getRawValue().criteria;
                const criteria = formCriteria.map(formCriterion => new Criterion(formCriterion.id, formCriterion.operator, formCriterion.value, formCriterion.unit));
                return criteriaService.stringify(criteria);
            })
        );

        // Validate the simple form
        const simpleFormValidate$ = (errors: ValidationErrors): Observable<ReadonlyArray<Tube>> => this.simpleForm$.pipe(
            take(1),
            filter(Boolean),
            map(form => {
                form.controls.search.setErrors(errors);
                form.updateValueAndValidity({ onlySelf: true, emitEvent: false });
                form.markAllAsTouched();
                return undefined as ReadonlyArray<Tube>;
            })
        );

        // Validate the advanced form
        const advancedFormValidate$ = (errors: ValidationErrors): Observable<ReadonlyArray<Tube>> => this.advancedForm$.pipe(
            take(1),
            filter(Boolean),
            map(form => {
                form.controls.criteria.setErrors(errors);
                form.updateValueAndValidity({ emitEvent: false });
                form.markAllAsTouched();
                return undefined as ReadonlyArray<Tube>;
            })
        );

        // Retrigger the search with the queryparams changes
        const isAdvancedSearchChange$ = searchQuery$.pipe(
            combineLatestWith(isAvancedSearchQuery$),
            map(([search]) => search)
        );

        const tubesLoader$ = loading$(tubeService.tubes$).pipe(
            shareReplayLast(),
            takeUntil(this.destroyed$) // This observable don't finalze, I don't now why and continue to publish after form disposing. This line solve the issue
        );

        this.addTube$.pipe(
            throttleTime(1000),
            map(event => openService.navigateTo(['edit'], event.ctrlKey)),
            takeUntil(this.destroyed$)
        ).subscribe();

        this.tubesLoader$ = simpleFormChange$.pipe(
            mergeWith(advancedFormChange$, isAdvancedSearchChange$, reloadSimpleSearch$, reloadAdvancedSearch$),
            distinctUntilChanged(),
            combineLatestWith(tubesLoader$),
            debounceTime(150),
            switchMap(([search, tubesLoader]) => {
                console.log('Search for tubes with parameters', search);
                if (tubesLoader.data) {
                    return searchService.filter$(tubesLoader.data, search).pipe(
                        catchError((errors: unknown) => {
                            console.log('Search validation errors', errors);
                            return simpleFormValidate$(errors).pipe(
                                mergeWith(advancedFormValidate$(errors))
                            );
                        }),
                        map(tubes => {
                            if (tubes) {
                                // Update url if search criteria are valid without reloading, because we don't want the form loose focus
                                this.lastSearch$.next(search);
                                const queryParams = { search } as QueryParams;
                                const urlTree = this.router.createUrlTree([], { relativeTo: activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
                                void this.router.navigateByUrl(urlTree);
                            }
                            return new LoadingContext(false, tubes);
                        })
                    );
                }
                return of(tubesLoader);
            }),
            shareReplayLast()
        );
    }

    public clearSearch$(): Observable<unknown> {
        return asyncMap(() => {
            const queryParams = { search: undefined } as QueryParams;
            return this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
        });
    }

    public advancedSearch$(value: boolean): Observable<unknown> {
        return asyncMap(() => {
            const advancedSearch = value ? 'true' : undefined as string;
            const queryParams = { advancedSearch } as QueryParams;
            return this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: queryParams, queryParamsHandling: 'merge' });
        });
    }

    public filterOptions<T extends Printable>(list: ReadonlyArray<T>, text: string): ReadonlyArray<T> {
        if (!text?.trim()) {
            return list;
        }

        const ltext = text.trim().toLowerCase();
        return list.filter(item => item.label.toLowerCase().includes(ltext));
    }
}
