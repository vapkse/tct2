import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule, Routes } from '@angular/router';

import { InputAutosizeModule } from 'src/app/common/input-autosize/input-autosize.module';
import { NumericStepperModule } from 'src/app/common/numeric-stepper/numeric-stepper.module';
import { RxIfModule } from 'src/app/common/rx-if/rx-if.module';
import { ThrottleEventModule } from 'src/app/common/throttle-event/throttle-event.module';
import { ViewPortModule } from 'src/app/common/viewport/viewport.module';

import { MainToolbarModule } from '../components/main-toolbar/main-toolbar.module';
import { TranslationModule } from '../translation/translation.module';
import { HomeComponent } from './home.component';
import { TubeCardModule } from './tube-card/tube-card.module';

const routes: Routes = [
    { path: '', component: HomeComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        InputAutosizeModule,
        MainToolbarModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        NumericStepperModule,
        ReactiveFormsModule,
        RxIfModule,
        ThrottleEventModule,
        TranslationModule,
        TubeCardModule,
        ViewPortModule
    ],
    exports: [
        HomeComponent
    ],
    declarations: [
        HomeComponent
    ]
})
export class HomeModule {
}
