const pinouts = require('../tubes-pinouts.json');
const fs = require('fs');

console.log('starting assets generator');
pinouts.forEach(pinout => {
    console.log('creating file', pinout.label);
    fs.copyFileSync('src/assets/images/not-available.svg', `src/assets/generator/${pinout.label}.svg`);
});
