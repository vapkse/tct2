declare module '@pdftron/pdfjs-express-viewer';

/**
* Creates a WebViewer instance and embeds it on the HTML page.
* @name WebViewer
* @param {WVOptions} options A set of options required for the contstructor to create an instance properly
* @param {string} [options.annotationUser=Guest] Name of the user for annotations
* @param {string} [options.config] URL path to a custom JavaScript for customizations
* @param {string} [options.css] URL path to a custom CSS file for customizations
* @param {Array.<string>} [options.disabledElements] List of data-elements to be disabled in UI
* @param {boolean} [options.enableAnnotations=true] Enable annotations feature
* @param {boolean} [options.enableAzureWorkaround=false] Enable workaround of the issue in Azure related to range requests
* @param {boolean} [options.enableFilePicker=false] Enable file picker feature
* @param {boolean} [options.enableMeasurement=false] Enable measurement tools
* @param {boolean} [options.enableRedaction=false] Enable redaction tool
* @param {string} [options.extension] Extension of the document to be loaded
* @param {boolean} [options.forceClientSideInit=false] If set to true then when loading a document using WebViewer Server the document will always switch to client only rendering allowing page manipulation and the full API to be used.
* @param {boolean} [options.fullAPI=false] Enable PDFNet.js library functions
* @param {string} [options.initialDoc] URL path to a document to load on startup
* @param {boolean} [options.isAdminUser=false] Set user permission to admin
* @param {boolean} [options.isReadOnly=false] Set user permission to read-only
* @param {string} options.licenseKey License key for viewing documents
* @param {boolean} [options.mobileRedirect=true] Whether the mobile viewer should redirect to a new window or not
* @param {boolean} [options.path='WebViewer/lib']  Path to the WebViewer lib folder
* @param {string} [options.preloadWorker] Type of workers to be preloaded. Accepts `pdf`|`office`|`all`.
* @param {string} [options.ui=default] Type of UI to be used
* @param {object} [options.workerTransportPromise]
* @param {function} [options.workerTransportPromise.pdf] Promise that resolves to a PDF worker
* @param {function} [options.workerTransportPromise.office]  Promise that resolves to an office worker
* @param {HTMLElement} viewerElement A DOMElement that is needed to root the iframe of the WebViewer onto the  HTML page
* @return {Promise<WebViewerInstance>} returns a promise that resolves to a webviewer instance.
* @example // 5.1 and after
WebViewer({
licenseKey: 'YOUR_LICENSE_KEY'
}, document.getElementById('viewer'))
.then(function(instance) {
    var docViewer = instance.docViewer;
    var annotManager = instance.annotManager;
    // call methods from instance, docViewer and annotManager as needed
    // you can also access major namespaces from the instancs as follows:
    // var Tools = instance.Tools;
    // var Annotations = instance.Annotations;
});
*/
export function WebViewer(options: WVOptions, viewerElement: HTMLElement): Promise<WebViewerInstance>;

/**
* @interface WVOptions
* An object from which you can choose options for the webviewer
* instance to be produced using the constructor.
* See our guides and API for WebViewer at https://www.pdftron.com/documentation/web
* @example WebViewer({
* path: 'path/to/WebViewer/lib',  // This contains a default value of `WebViewer/lib`
* initialDoc: 'path/to/local/file OR URL', // (see more options in guides)
* enableAnnotations: true, // Allows you to toggle the use of annotations on the document
* // and many other options to explore
* } document.getElementById('viewer')).then(function(instance) {...})
*/
export interface WVOptions {
    initialDoc: string;
    annotationUser?: string;
    config?: string;
    css?: string;
    disabledElements?: Array<any>;
    documentId?: string;
    enableAnnotations?: boolean;
    enableAzureWorkaround?: boolean;
    enableFilePicker?: boolean;
    enableOfflineMode?: boolean;
    enableRedaction?: boolean;
    enableMeasurement?: boolean;
    extension?: any;
    forceClientSideInit?: boolean;
    fullAPI?: boolean;
    isAdminUser?: boolean;
    isReadOnly?: boolean;
    l?: string;
    licenseKey?: string;
    mobileRedirect?: boolean;
    pdfBackend?: string;
    path?: string;
    preloadPDFWorker?: boolean;
    useDownloader?: boolean;
    ui?: string;
    workerTransportPromise?: {
        pdf?: (...params: Array<any>) => any;
        office?: (...params: Array<any>) => any;
    };
}

export class DocumentViewer extends utils.eventHandler {
    /**
     * Retrieve the number of the current page that is searched in.
     * If the returned value is -1, it indicates the search process has not been initialized
     * (e.g., Begin() is not called yet); if the returned value is 0, it indicates the search
     * process has finished, and if the returned value is positive, it is a valid page number.
     * @method PDFNet.TextSearch#getCurrentPage
     * @return {number} A promise that resolves to the current page number.
     */
    getCurrentPage(): number;
    /**
     * Returns the number of pages in a document.
     * @method CoreControls.Document#getPageCount
     * @returns {number} The number of the pages in the current document.
     */
    getPageCount(): number;
    /**
     * Sets the current page. Updates the current page and jumps to it.
     * @method CoreControls.DocumentViewer#setCurrentPage
     * @param {int} pageNumber The page number to jump to.
     */
    setCurrentPage(pageNumber: number): void;
    /**
     * Returns the current page number.
     * @method CoreControls.DocumentViewer#getCurrentPage
     * @returns {number} The current 1-indexed page number.
     */
    getCurrentPage(): number;

    setWatermark(options: any): void;
}

export class Core {
    documentViewer: DocumentViewer;
}

export class Header {
    /**
     * Select a button from header to edit.
     * @method WebViewer.Header#get
     * @param {string} dataElement data-element of the button.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#insertBefore insertBefore}, {@link CoreControls.ReaderControl.Header#insertAfter insertAfter} and {@link CoreControls.ReaderControl.Header#delete delete} to perform an operation on the button.
     */
    get(dataElement: string): any;
    /**
     * Get all list of header items from a group selected from {@link CoreControls.ReaderControl.Header#getHeader getHeader}. By default, it returns the items from 'default' group.
     * @method WebViewer.Header#getItems
     * @returns {Array.<object>} List of header item objects. You can edit it using normal array operations and update the whole header by passing it to {@link CoreControls.ReaderControl.Header#update update}.
     */
    getItems(): object[];
    /**
     * Select a header group to edit.
     * @method WebViewer.Header#getHeader
     * @param {string} headerGroup Name of the header group. By default, 'default' and 'tools' are accepted.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    getHeader(headerGroup: string): any;
    /**
     * Insert a button before the selected button from {@link CoreControls.ReaderControl.Header#get get}.
     * @method WebViewer.Header#insertBefore
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    insertBefore(): any;
    /**
     * Insert a button after the selected button from {@link CoreControls.ReaderControl.Header#get get}.
     * @method WebViewer.Header#insertAfter
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    insertAfter(): any;
    /**
     * Delete a button.
     * @method WebViewer.Header#delete
     * @param {(number|string)} [id] You can either pass an index or `data-element` of the button to delete. If you already selected a button from {@link CoreControls.ReaderControl.Header#get get}, passing null would delete the selected button.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    delete(id?: number | string): any;
    /**
     * Removes the first button in the header.
     * @method WebViewer.Header#shift
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    shift(): any;
    /**
     * Adds a button (or buttons) to the beginning of the header.
     * @method WebViewer.Header#unshift
     * @param {object|Array.<object>} obj Either one or array of header objects. See <a href='https://www.pdftron.com/documentation/web/guides/customizing-header#header-items' target='_blank'>Header items</a> for details.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    unshift(obj: any | object[]): any;
    /**
     * Adds a button (or buttons) to the end of the header.
     * @method WebViewer.Header#push
     * @param {object|Array.<object>} obj Either one or array of header objects. See <a href='https://www.pdftron.com/documentation/web/guides/customizing-header#header-items' target='_blank'>Header items</a> for details.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    push(obj: any | object[]): any;
    /**
     * Removes the last button in the header.
     * @method WebViewer.Header#pop
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    pop(): any;
    /**
     * Updates the header with new list of header items.
     * @method WebViewer.Header#update
     * @param {Array.<object>} headerObjects List of header objects to replace the exising header. You can use {@link CoreControls.ReaderControl.Header#getItems getItems} to refer to existing header objects.
     * @returns {CoreControls.ReaderControl.Header} Header object for chaining. You can call {@link CoreControls.ReaderControl.Header#get get}, {@link CoreControls.ReaderControl.Header#getItems getItems}, {@link CoreControls.ReaderControl.Header#shift shift}, {@link CoreControls.ReaderControl.Header#unshift unshift}, {@link CoreControls.ReaderControl.Header#push push}, {@link CoreControls.ReaderControl.Header#pop pop} and {@link CoreControls.ReaderControl.Header#update update}.
     */
    update(headerObjects: object[]): any;
}

export type headerCallback = (header: Header) => void;

export class UI {
    setHeaderItems(headerCallback: headerCallback): void;

    /* remove left panel and left panel button from the DOM
      instance.disableElements([ 'leftPanel', 'leftPanelButton' ]);
    });
     */
    disableElements(dataElements: string[]): void;

    setLanguage(language: string): void;

    getCurrentPageNumber(): number;

    iframeWindow: Window;
}

/**
* Creates a WebViewer instance and embeds it on the HTML page.
* @name WebViewer
* @class Main class.
* @param {object} options
* @param {HTMLElement} viewerElement DOM element that will contain WebViewer
*/
export class WebViewerInstance {
    constructor(options: {
        initialDoc: string;
    }, viewerElement: HTMLElement);

    Core: Core;
    UI: UI;

    setTheme(theme?: string, themeOptions?: {
        primary?: string;
        secondary?: string;
        border?: string;
        buttonHover?: string;
        buttonActive?: string;
        text?: string;
        icon?: string;
        iconActive?: string;
    }): void;
}

export namespace utils {
    /**
     * @class
     * @name eventHandler
     * @memberOf utils
     */
    class eventHandler {
        /**
         * Add an event handler
         * @method utils.eventHandler#on
         * @param {string} eventName The name of the event to listen to
         * @param {function} handler The function to be called when the event is triggered
         * @return {object} Returns the object that 'on' is being called on.
         */
        on(eventName: string, handler: (...params: Array<any>) => any): any;
        addEventListener(eventName: string, handler: (...params: Array<any>) => any): any;
        /**
         * Remove an event handler
         * @method utils.eventHandler#off
         * @param {string} eventName The name of the event to remove the handler for
         * @param {function} [handler] The handler associated with this event to be removed
         * @return {object} Returns the object that 'off' is being called on.
         */
        off(eventName: string, handler?: (...params: Array<any>) => any): any;
        removeEventListener(eventName: string, handler?: (...params: Array<any>) => any): any;
        /**
         * Trigger an event
         * @method utils.eventHandler#trigger
         * @param {string} eventName The name of the event to trigger
         * @param {array} eventParams Parameters associated with the event
         * @return {object} Returns the object that 'trigger' is being called on.
         * @example
             annotManager.trigger('annotationChanged', [[annotation], 'modify']);
         */
        trigger(eventName: string, eventParams: Array<any>): any;
        /**
         * Add an event handler that will be removed automatically after being handled the first time
         * @method utils.eventHandler#one
         * @param {string} eventName The name of the event to listen to
         * @param {function} handler The function to be called when the event is triggered
         * @return {object} Returns the object that 'one' is being called on.
         */
        one(eventName: string, handler: (...params: Array<any>) => any): any;
    }
    /**
     * This function will split the specified inklist by the specified point. The specified point will remain on the old inklist and the new inklist will be added to the end of the inklist array.
     * @param inklist - the array of inklist arrays.
     * @param splitpoint - the point within the array to split the list by
     * @param inklistpos - the position within the `inklist` array.
     * @return the newly created array
     */
    function splitInkListBetween(inklist: any, splitpoint: any, inklistpos: any): any;
    /**
     * This function will return true if the specified point `testpt` is in the quad
     * @param qp - the quadpoint to check within.
     * @param testpt - Point to check for
     * @return - A bool indicating if the point is in the quad
     */
    function isPointInQuad(qp: any, testpt: any): any;
    /**
     * This function constructs a rectangle around point P1P2 with width
     * the rectangle will be tilted the same angle as the P1P2
     * B________A
     * |   P1   |
     * |   |    |
     * |   |    |
     * |   |    |
     * |___P2___|
     * C        D
     * @param pt1, pt2 - the endpoints of the quad
     * @param eraserRadius - The radius to expand the quad with
     * @return the QuadPoint
     */
    function getQuadFromPoints(pt1: any, eraserRadius: any): any;
    /**
     * This function will return the specific points that intersect the given quad and the line segment.
     * Note that if there are two points of intersections, the closest point to `pt1` will be the first point in the vector.
     * @param qp - the quadpoint to check within.
     * @param pt1, pt2 - Points that define the line segment to check against the quad
     * @return - A vector that contains at most two points of intersection
     */
    function returnPointThatIntersectsQuad(qp: any, pt1: any): any;
}

export default WebViewer;

